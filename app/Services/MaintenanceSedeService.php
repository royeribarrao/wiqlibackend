<?php namespace App\Services;

use App\Services\Contracts\MaintenanceSedeInterface as MaintenanceSedeInterface;
use App\Models\MaintenanceSede as MaintenanceSedeModel;

class MaintenanceSedeService implements MaintenanceSedeInterface {
  
  protected $maintenanceSede;
  
  public function __construct(MaintenanceSedeModel $maintenanceSedeModel)
  {
    $this->maintenanceSede = $maintenanceSedeModel;
  }
    
  public function all($columns = array('*'), $relation = [])
  {
    return $this->maintenanceSede->get(['id', 'name']);
  }
  
  public function paginate($perPage = 15, $columns = array('*'), $order_type = 'desc')
  {
    return $this->maintenanceSede->orderBy('id', $order_type)->paginate($perPage, $columns);
  }
  
  public function create(array $data)
  {
    $newMaintenanceSede = $this->maintenanceSede->create($data);
    return $newMaintenanceSede;
  }
  
  public function update(array $data, $id)
  {
    $maintenanceSedeGot = $this->maintenanceSede->find($id);
    $maintenanceSedeGot->update($data);
    return $maintenanceSedeGot;
  }
  
  public function delete($id)
  {
    return $this->maintenanceSede->destroy($id);
  }
  
  public function find($id, $columns = array('*'))
  {
    return $this->maintenanceSede->find($id, $columns);
  }
  
  public function findBy($field, $value, $columns = array('*'))
  {
    return $this->maintenanceSede->where($field, '=', $value)->first($columns);
  }
  
  public function search($searching, $columns = array('*'))
  {
    // TODO: Implement search() method.
  }
  
  public function updateState($id, $state)
  {
    $maintenanceServiceGot = $this->maintenanceSede->find($id);
    $maintenanceServiceGot->state = !$state;
    $maintenanceServiceGot->save();
    return $maintenanceServiceGot;
  }
}