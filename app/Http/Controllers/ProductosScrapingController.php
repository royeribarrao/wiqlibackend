<?php

namespace App\Http\Controllers;

use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliProducto;
use Illuminate\Http\Request;
use App\Models\ProductoExterno;
use App\Models\PrecioProductoExterno;

class ProductosScrapingController extends Controller
{
    public function todosProductos()
    {
        $productos = ProductoExterno::with('productoWiqli')
                ->orderBy('nombre')
                ->paginate(10);
        return $productos;
    }

    public function show($id)
    {
        $producto = ProductoExterno::find($id);
        return $producto;
    }

    public function crear(Request $request)
    {
        $producto = ProductoExterno::create([
            'nombre' => $request->nombre,
            'wiqli_producto_id' => $request->wiqli_producto_id,
            'url_vea' => $request->url_vea,
            'multiplicador_vea' => $request->multiplicador_vea,
            'url_tottus' => $request->url_tottus,
            'multiplicador_tottus' => $request->multiplicador_tottus,
            'url_wong' => $request->url_wong,
            'multiplicador_wong' => $request->multiplicador_wong
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto creado correctamente.'
        ]);
    }

    public function update(Request $request, $id)
    {
        $producto = ProductoExterno::find($id);
        $producto->update([
            'nombre' => $request->nombre,
            'url_vea' => $request->url_vea,
            'multiplicador_vea' => $request->multiplicador_vea,
            'url_tottus' => $request->url_tottus,
            'multiplicador_tottus' => $request->multiplicador_tottus,
            'url_wong' => $request->url_wong,
            'multiplicador_wong' => $request->multiplicador_wong
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto actualizado correctamente.'
        ]);
    }

    public function getPrecios($id)
    {
        $precios = PrecioProductoExterno::select(
                'precios_productos_externos.precio_unitario_online as precio_externo',
                'precios_productos_externos.precio_unitario_wiqli as precio_wiqli',
                'precios_productos_externos.id as id',
                'precios_productos_externos.tienda_id as tienda',
                'precios_productos_externos.created_at as created_at',
                'precios_productos_externos.multiplicador',
                'productos_externos.id as producto_externo_id',
                'productos.nombre',
                'productos.id as producto_id',
                'productos.precio_unitario',
            )
            ->leftJoin('productos_externos', 'productos_externos.id', 'precios_productos_externos.producto_externo_id')
            ->leftJoin('productos', 'productos.id', 'productos_externos.wiqli_producto_id')
            ->where('precios_productos_externos.producto_externo_id', $id)
            ->orderBy('precios_productos_externos.id', 'desc')
            ->paginate(10);
        return $precios;
    }

    public function getPreciosScraping($id)
    {
        $preciosVea = PrecioProductoExterno::where('tienda_id', 1)
                        ->where('producto_externo_id', $id)
                        ->orderBy('created_at', 'asc')
                        ->get();
        $preciosTottus = PrecioProductoExterno::where('tienda_id', 2)
                        ->where('producto_externo_id', $id)
                        ->orderBy('created_at', 'asc')
                        ->get();
        $preciosWong = PrecioProductoExterno::where('tienda_id', 3)
                        ->where('producto_externo_id', $id)
                        ->orderBy('created_at', 'asc')
                        ->get();

        $arrayPrecios = [];
        foreach ($preciosVea as $key => $precio) {
            array_push($arrayPrecios, $precio->precio_unitario_online);
        }

        return response()->json([
            'state'=> 1,
            'vea' => $preciosVea,
            'tottus' => $preciosTottus,
            'wong' => $preciosWong
        ]);
    }
}
