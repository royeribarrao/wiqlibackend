<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Distrito;

class DistritoController extends Controller
{
    public function getDistritos()
    {
        $distritos = Distrito::orderBy('nombre', 'asc')->get();
        return $distritos;
    }
}
