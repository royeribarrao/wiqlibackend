<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\ProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Configuracion;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\ExcelPedidosExport;

use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\AvisoUsuario;
use Mail;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Http\Controllers\PayUController;

class WiqliPedidoController extends Controller
{
    protected $payUService;
    protected $correosAdmin;

    public function __construct(PayUController $payU)
    {
        $this->payUService = $payU;
        $this->correosAdmin = config('constants.correosAdmin');
    }

    public function all(Request $request)
    {
        $fechas = [];
        if(isset($request->fecha)){
            $fechas[0] = substr($request->fecha[0], 1, 10);
            $fechas[1] = substr($request->fecha[1], 1, 10);
        }
        
        $pedidos = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])
                    ->where(function ($q) use ($request){
                        if (isset($request->fechaInicial) && isset($request->fechaFinal)) {
                            $q->whereBetween('fecha_entrega', [$request->fechaInicial, $request->fechaFinal]);
                        }
                    })
                    ->where('status', 1)
                    ->paginate(15);
        return $pedidos;
    }

    public function crearPedido(Request $request)
    {
        
        $cliente = $request->cliente;

        if(isset($cliente['tipoPago']) && $cliente['tipoPago'] == 2){
            $pagar = $this->payUService->pagarPedido($request->total, $request->datosTarjeta, $cliente);
            if($pagar->getData()->state == false){
                return response()->json([
                    'state'=> false,
                    'message' => $pagar->getData()->message
                ]);
            }
        }
        
        $newCliente = $this->guardarCliente($cliente);
        $newPedido = $this->guardarPedido($newCliente, $cliente, $request->costoDelivery);
        $totalProductos = $this->guardarDetallePedido($request->productos, $newPedido->id);
        $totalProductosExtra = $this->guardarProductosExtra($request->productosExtra, $newPedido->id);

        $newPedido->update([
            'total' => $newPedido->total + $totalProductos,
            'totalProductos' => $totalProductos
        ]);

        $referidoUsaDescuentoReferente = null;
        $cuponRecurrencia = null;
        if($request->cupon)
        {
            $idCupon = CuponDescuento::where('codigo', $request->codigoCupon)->first();
            if($idCupon->proveedor == 1)
            {
                if($newCliente->correo != $idCupon->correo_creador)
                {
                    $monto = $idCupon->tipo == 1 ? ($idCupon->monto * $totalProductos)/100 : $idCupon->monto;
                    $newClienteReferido = ClienteReferidoCupon::create([
                        'cupon_descuento_id' => $idCupon->id,
                        'correo_referido' => $cliente['correo'],
                        'monto' => $monto,
                        'correo_referente' => $idCupon->correo_creador
                    ]);
    
                    $newPedido->update([
                        'total' => $newPedido->total - $monto,
                        'descuento' => $monto,
                        'cupon_id' => $idCupon->id
                    ]);
                    $referidoUsaDescuentoReferente = $monto;
                }
            }
            if($idCupon->proveedor == 2)
            {
                $newClienteReferido = ClienteReferidoCupon::create([
                    'cupon_descuento_id' => $idCupon->id,
                    'correo_referido' => $cliente['correo'],
                    'monto' => $request->descuento,
                    'correo_referente' => $idCupon->correo_creador
                ]);

                $newPedido->update([
                    'total' => $newPedido->total - $request->descuento,
                    'descuento' => $request->descuento,
                    'cupon_id' => $idCupon->id
                ]);
                $referidoUsaDescuentoReferente = $request->descuento;
            }
            if($idCupon->proveedor == 4)
            {
                $cuponRecurrencia = $idCupon;
                $newClienteReferido = ClienteReferidoCupon::create([
                    'cupon_descuento_id' => $idCupon->id,
                    'correo_referido' => $cliente['correo'],
                    'monto' => $request->descuento,
                    'correo_referente' => $idCupon->correo_creador
                ]);

                $newPedido->update([
                    'total' => $newPedido->total - $request->descuento,
                    'descuento' => $request->descuento,
                    'cupon_id' => $idCupon->id
                ]);
            }
        }
        $totalReferidos = ClienteReferidoCupon::select(
                                'clientes_referido_cupon.id',
                                'clientes_referido_cupon.cupon_descuento_id',
                                'clientes_referido_cupon.monto',
                                'clientes_referido_cupon.correo_referido',
                                'clientes_referido_cupon.activo',
                                'cupones_descuento.correo_creador'
                            )
                            ->join('cupones_descuento', 'cupones_descuento.id', 'clientes_referido_cupon.cupon_descuento_id')
                            ->where('cupones_descuento.correo_creador', $newCliente->correo)
                            ->where('clientes_referido_cupon.activo', 1)
                            ->get();
        
        $descuentoReferidos = 0;
        foreach ($totalReferidos as $key => $referido) {
            $descuentoReferidos += $referido->monto;
        }
        $newPedido->update([
            'total' => $newPedido->total - $descuentoReferidos,
            'descuento' => $request->descuento + $descuentoReferidos
        ]);
        
        $cupon = null;

        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $newPedido->id)
                    ->get();
        $productosAdicionales = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '=', 999)
                    ->where('pedido_id', $newPedido->id)
                    ->get();

        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }
        $productosParaDescuento = ProductoExterno::whereIn('wiqli_producto_id', $idsProductosInternos)->get();
        $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $newPedido->id));

        Mail::to($newCliente->correo)->send(new AvisoUsuario(
            $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
            $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
        ));

        foreach ($this->correosAdmin as $key => $value) {
            Mail::to($value)->send(new AvisoDashboard(
                $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
                $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
            ));
        }

        foreach ($totalReferidos as $key => $referido) {
            $referidoUpdate = ClienteReferidoCupon::find($referido->id);
            $referidoUpdate->update([
                'activo' => false,
                'pedido_referente_cobrador_id' => $newPedido->id
            ]);
        }

        return response()->json([
            'state'=> 1,
            'message' => 'Pedido creado correctamente.'
        ]);
    }

    public function guardarDetallePedido($productos = [], $pedido_id)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedido_id,
                'producto_id' => $producto['id'],
                'cantidad' => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] : $producto['cantidad'],
                'cantidad_minima' => $producto['cantidad_minima'],
                'precio_unitario' => $producto['precio_unitario'],
                'unidad_medida' => $producto['unidad']['nombre'],
                'total'  => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario']
            ]);
            $total += $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario'];
        }
        return $total;
    }

    public function guardarProductosExtra($productos = [], $pedidoId)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedidoId,
                'categoriaId' => $producto['categoriaId'],
                'producto_id' => 999,
                'nombre_desc' => $producto['nombre_desc'],
                'cantidad_desc' => $producto['cantidad_desc']
            ]);
        }
        return $total;
    }


    public function calcularTotalDescuento($idsProductosInternos, $pedidoId)
    {   
        $totalProductosInternos = 0;
        $totalProductosExternos = 0;
        foreach ($idsProductosInternos as $key => $id) {
            $detallePedido = WiqliDetallePedido::where('pedido_id', $pedidoId)->where('producto_id', $id)->first();
            $subTotal = 0;
            $precio1 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 1)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio2 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 2)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio3 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 3)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $totalNumeroPrecios = 0;
            $totalPrecioExterno = 0;
            if($precio1){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio1->precio_unitario_online * $precio1->multiplicador;
            }
            if($precio2){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio2->precio_unitario_online * $precio2->multiplicador;
            }
            if($precio3){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio3->precio_unitario_online * $precio3->multiplicador;
            }

            if($precio1 || $precio2 || $precio3)
            {
                $totalProductosInternos += $detallePedido->cantidad * $detallePedido->precio_unitario;
            }

            if($totalNumeroPrecios > 0 && $totalPrecioExterno>0){
                if($detallePedido){
                    $subTotal = $detallePedido->cantidad * $totalPrecioExterno/$totalNumeroPrecios;
                }
            }

            $totalProductosExternos += $subTotal;
        }
        if($totalProductosInternos > 0 && $totalProductosExternos>0){
            return ($totalProductosInternos - $totalProductosExternos);
        }
        return 0;
    }

    public function guardarPedido($new_cliente, $cliente, $costoDelivery)
    {
        $pedido = WiqliPedido::create([
            'total' => $costoDelivery,
            'cliente_id' => $new_cliente->id,
            'costo_delivery' => $costoDelivery,
            'fecha_entrega' => substr($cliente['fecha_recojo'], 6, 4) . '-' . substr($cliente['fecha_recojo'], 3, 2) . '-' . substr($cliente['fecha_recojo'], 0, 2),
            'observacion' => isset($cliente['observacion']) ? $cliente['observacion'] : '',
            'tipoPago' => $cliente['tipoPago'],
            'pagado' => $cliente['tipoPago'] == 2 ? 1 : 0
        ]);
        return $pedido;
    }

    public function guardarCliente($cliente)
    {
        $new_cliente = WiqliCliente::create([
            'nombres' => $cliente['nombres'],
            'apellidos' => $cliente['apellidos'],
            'direccion' => $cliente['direccion'],
            'referencia' => $cliente['referencia'],
            'distritoId' => $cliente['distrito'],
            'provincia' => 'Lima',
            'departamento' => 'Lima',
            'telefono' => $cliente['telefono'],
            'correo' => $cliente['correo']
        ]);
        return $new_cliente;
    }

    public function exportExcel(Request $request)
    {
        return Excel::download(new PedidosExport, 'reporte.xlsx');
    }

    public function exportExcelPorCliente($id)
    {
        return Excel::download(new PedidosExport, 'reporte.xlsx');
    }

    public function verPdf($id)
    {
        $pedido = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])->where('id', $id)->get();
        $clientesReferidos = ClienteReferidoCupon::where('correo_referente', $pedido[0]->cliente->correo)
                        ->where('pedido_referente_cobrador_id', $id)
                        ->get();
        $cupon = null;
        if($pedido[0]['cupon_id'])
        {
            $cupon = CuponDescuento::find($pedido[0]['cupon_id']);
        }

        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $id)
                    ->where('status', 1)
                    ->get();

        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }
        $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $id));


        $data = [
            'productos' => $pedido[0]['detalle'],
            'pedido' => $pedido[0],
            'referidos' => $clientesReferidos,
            'cupon' => $cupon,
            'ahorroExterno' => $totalAhorro
        ];
        $pdf = Pdf::loadView('exports.pedido', $data);
        return $pdf->download('pedido_'.$data['pedido']['cliente']['nombres'].'.pdf');
    }

    public function prueba()
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])->get();

        $productos = WiqliProducto::with('unidad')->get()->toArray();
        
        //totales de los productos pedidos
        $totalesCantidad = [];
        $prods = WiqliDetallePedido::all()
            ->where('producto_id', '!=', 999)
            ->groupBy('producto_id');
        
        foreach ($prods as $key => $producto) {
            $subtotal = 0;
            foreach ($producto as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totalesCantidad, $subtotal);
        }
        //calculando los productos aptos con sus cantidades totales
        $detalle_pedidos = WiqliDetallePedido::select('producto_id')
            ->where('producto_id', '!=', 999)
            ->orderBy('producto_id')
            ->get()
            ->toArray();
        $detalle = [];
        
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });

        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }

        foreach ($new_prods as $key => $value) {
            $new_prods[$key]['cantidad'] = $totalesCantidad[$key];
            $new_prods[$key]['total'] = $value['precio_unitario'] * $totalesCantidad[$key];
        }

        foreach ($new_prods as $key1 => $producto) {
            foreach ($pedidos as $key2 => $pedido) {
                $new_prods[$key1]['pedidos'][$key2]['id'] = $pedido['id'];
            }
        }
        
        $detallePedidos = WiqliDetallePedido::where('producto_id', '!=', 999)->get();
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($detallePedidos as $key2 => $detalle) {
                foreach($producto['pedidos'] as $key3 => $pedido){
                    if($producto['id'] == $detalle['producto_id'] && $pedido['id'] == $detalle['pedido_id'])
                    {
                        $new_prods[$key1]['pedidos'][$key3]['cantidad'] = $detalle['cantidad'];
                        $new_prods[$key1]['pedidos'][$key3]['precio'] = $detalle['precio_unitario'];
                        $new_prods[$key1]['pedidos'][$key3]['total'] = $detalle['total'];
                    }
                }
            }
        }
        return $new_prods;
    }

    public function exportarExcelTodos()
    {
        return Excel::download(new PedidosExport, 'reporte.xlsx');
    }

    public function exportarExcel(Request $request, $fechaInicial, $fechaFinal)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
                        $query->where('producto_id', '!=', 999);
                    }])
                    ->where('status', 1)
                    ->whereBetween('fecha_entrega', [$fechaInicial, $fechaFinal])
                    ->get();
        $productosAptos = $this->obtenerProductosAptos($fechaInicial, $fechaFinal);
        $productosAdicionales = $this->obtenerProductosAdicionales($fechaInicial, $fechaFinal);
        
        $totalPedido = $this->obtenerTotalPedido($fechaInicial, $fechaFinal);
        $total = $this->obtenerTotalVenta($fechaInicial, $fechaFinal);
        
        return (new ExcelPedidosExport)
                ->pedidos($pedidos)
                ->productosAptos($productosAptos)
                ->productosAdicionales($productosAdicionales)
                ->totalPedido($totalPedido)
                ->total($total)
                ->download('Report pedido: ' . $fechaInicial . ' - ' . $fechaFinal . '.xlsx');
    }

    public function obtenerProductosAptos($fechaInicial, $fechaFinal)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->whereBetween('fecha_entrega', [$fechaInicial, $fechaFinal])
        ->get();

        $productos = WiqliProducto::with('unidad')->orderBy('nombre', 'asc')->get()->toArray();
        
        //totales de los productos pedidos
        $totalesCantidad = [];
        $prods = DB::table('detalles_pedido')
                    ->where('detalles_pedido.producto_id', '!=', 999)
                    ->where('pedidos.status', 1)
                    ->whereBetween('pedidos.fecha_entrega', [$fechaInicial, $fechaFinal])
                    ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                    ->get()
                    ->groupBy('producto_id')
                    ->toArray();
        
        foreach ($prods as $key => $producto) {
            $subtotal = 0;
            foreach ($producto as $key1 => $value) {
                $subtotal += $value->cantidad;
            }
            $dato = ['id' => null, 'total' => null];
            // $totalesCantidad[$key]['id'] = $key;
            // $totalesCantidad[$key]['total'] = $subtotal;
            $dato['id'] = $key;
            $dato['total'] = $subtotal;
            array_push($totalesCantidad, $dato);
        }
        //calculando los productos aptos con sus cantidades totales
        $detalle_pedidos = DB::table('detalles_pedido')
                ->select('producto_id', 'productos.nombre')
                ->where('detalles_pedido.producto_id', '!=', 999)
                ->where('pedidos.status', 1)
                ->orderBy('productos.nombre', 'asc')
                ->whereBetween('pedidos.fecha_entrega', [$fechaInicial, $fechaFinal])
                ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                ->leftJoin('productos', 'detalles_pedido.producto_id', 'productos.id')
                ->get()
                ->toArray();
        $detalle = [];
        
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido->producto_id);
        }
        
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        
        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }
        foreach ($new_prods as $key => $value) {
            foreach ($totalesCantidad as $key1 => $value1) {
                if($value['id'] == $value1['id'])
                {
                    $new_prods[$key]['cantidad'] = $value1['total'];
                    $new_prods[$key]['total'] = $value['precio_unitario'] * $value1['total'];
                }
            }
        }
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($pedidos as $key2 => $pedido) {
                $new_prods[$key1]['pedidos'][$key2]['id'] = $pedido['id'];
            }
        }
        $detallePedidos = WiqliDetallePedido::where('producto_id', '!=', 999)->get();
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($detallePedidos as $key2 => $detalle) {
                foreach($producto['pedidos'] as $key3 => $pedido){
                    if($producto['id'] == $detalle['producto_id'] && $pedido['id'] == $detalle['pedido_id'])
                    {
                        $new_prods[$key1]['pedidos'][$key3]['cantidad'] = $detalle['cantidad'];
                        $new_prods[$key1]['pedidos'][$key3]['precio'] = $detalle['precio_unitario'];
                        $new_prods[$key1]['pedidos'][$key3]['total'] = $detalle['total'];
                    }
                }
            }
        }
        return $new_prods;
    }

    public function obtenerProductosAdicionales($fechaInicial, $fechaFinal)
    {
        $productos = WiqliDetallePedido::select(
                            'detalles_pedido.id',
                            'detalles_pedido.nombre_desc',
                            'detalles_pedido.cantidad_desc',
                            'detalles_pedido.total',
                            'detalles_pedido.precio_unitario',
                            'clientes.nombres',
                            'clientes.apellidos'
                        )
                        ->where('pedidos.status', 1)
                        ->where('detalles_pedido.producto_id', '=', 999)
                        ->where('detalles_pedido.status', 1)
                        ->whereBetween('pedidos.fecha_entrega', [$fechaInicial, $fechaFinal])
                        ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                        ->join('clientes', 'pedidos.cliente_id', 'clientes.id')
                        ->get();
        return $productos;
    }

    public function obtenerTotalPedido($fechaInicial, $fechaFinal)
    {
        $totales_cantidad = [];
        $prods = DB::table('detalles_pedido')
                ->where('detalles_pedido.producto_id', '!=', 999)
                ->where('pedidos.status', 1)
                ->whereBetween('pedidos.fecha_entrega', [$fechaInicial, $fechaFinal])
                ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                ->get()
                ->groupBy('producto_id')
                ->toArray();
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value->cantidad;
            }
            array_push($totales_cantidad, $subtotal);
        }
        return $totales_cantidad;
    }

    public function obtenerTotalVenta($fechaInicial, $fechaFinal)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->whereBetween('fecha_entrega', [$fechaInicial, $fechaFinal])
        ->get();
        $total = 0;
        foreach ($pedidos as $key => $pedido) {
            $total += $pedido->total;
        }
        return $total;
    }
}