<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Models\Wiqli\WiqliCliente;
use App\Http\Controllers\Controller;

class WiqliClienteController extends Controller
{
    public function all()
    {
        $clientes = WiqliCliente::all();
        return $clientes;
    }
}
