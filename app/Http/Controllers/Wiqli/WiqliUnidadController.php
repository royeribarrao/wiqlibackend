<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Models\Wiqli\WiqliUnidadMedida;
use App\Http\Controllers\Controller;

class WiqliUnidadController extends Controller
{
    public function all()
    {
        $unidades = WiqliUnidadMedida::all();
        return $unidades;
    }
}
