<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Configuracion;

class WiqliConfiguracionController extends Controller
{
    public function getConfiguracion()
    {
        $configuracion = Configuracion::first();
        return $configuracion;
    }

    public function guardarConfiguracion(Request $request)
    {
        $configuracion = Configuracion::first();
        $configuracion->update([
            'monto_descuento' => $request->monto_descuento,
            'monto_minimo_compra_referido' => $request->monto_minimo_compra_referido,
            'monto_minimo_envio_codigo' => $request->monto_minimo_envio_codigo,
            'tipo_descuento' => $request->tipo_descuento
        ]);
        return response()->json([
            'state' => true,
            'message' => 'Configuración actualizada correctamente.'
        ]);
    }
}
