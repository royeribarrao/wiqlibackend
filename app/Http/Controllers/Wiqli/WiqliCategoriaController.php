<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Models\Wiqli\WiqliCategoriaProducto;
use App\Http\Controllers\Controller;

class WiqliCategoriaController extends Controller
{
    public function all()
    {
        $categorias = WiqliCategoriaProducto::all();
        return $categorias;
    }
}
