<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Models\Wiqli\WiqliProducto;
use App\Http\Controllers\Controller;
use App\Services\SaveImageService;

class WiqliProductoController extends Controller
{
    protected $rutaArchivos = 'productos/';
    protected $storageDisk = 'wiqli';

    public function __construct(
        SaveImageService $imagenService
    )
    {
        $this->imagenService = $imagenService;
    }

    public function all(Request $request)
    {
        $productos = WiqliProducto::with(['unidad', 'categoria'])
                            ->where('activo', 1)
                            ->orderBy('nombre', 'asc')
                            ->get();
        return $productos;
    }

    public function allAdmin(Request $request)
    {
        
        $productos = WiqliProducto::with(['unidad'])->select(
                    'productos.id as producto_id',
                    'productos.activo as producto_estado',
                    'productos.nombre as producto_nombre',
                    'productos.cantidad_minima as producto_cantidad_minima',
                    'productos.imagen as producto_imagen',
                    'productos.precio_unitario as producto_precio_unitario',
                    'productos.stock as producto_stock',
                    'categorias_producto.nombre as categoria_nombre',
                    'categorias_producto.id as categoria_id',
                    'unidades_medida.nombre as unidad_medida_nombre',
                    'unidades_medida.id as unidad_medida_id',

                )
                ->where(function ($q) use ($request){
                    if (isset($request->nombre) && $request->nombre !== '') {
                        $q->where('productos.nombre','like', '%'.$request->nombre.'%');
                    }
                })
                ->where(function ($q) use ($request){
                    if (isset($request->categoria_id) && $request->categoria_id !== '') {
                        $q->where('categorias_producto.id', $request->categoria_id);
                    }
                })
                ->leftJoin('unidades_medida', 'productos.unidad_id', 'unidades_medida.id')
                ->leftJoin('categorias_producto', 'productos.categoria_id', 'categorias_producto.id')
                ->orderBy('productos.categoria_id', 'asc')
                ->orderBy('productos.nombre', 'asc')
                ->paginate(10);
        return $productos;
    }

    public function show($id)
    {
        $producto = WiqliProducto::with(['unidad', 'categoria'])->find($id);
        return $producto;
    }

    public function crear(Request $request)
    {
        $data = $request->all();
        if(isset($data['files'])){
            $imageProducto = $this->imagenService->guardarImagen($this->storageDisk, $request->file('files'), $this->rutaArchivos);
        }
        $producto = WiqliProducto::create([
            'cantidad_minima' => $request->cantidad_minima,
            'categoria_id' => $request->categoria_id,
            'imagen' => isset($imageProducto) ? $imageProducto[0]['full_path'] : $request->imagen,
            'nombre' => $request->nombre,
            'precio_unitario' => $request->precio_unitario,
            'unidad_id' => $request->unidad_id,
        ]);
        return response()->json([
            'state'=> true,
            'message' => 'Producto creado correctamente.'
        ]);
    }

    public function actualizar(Request $request, $id)
    {
        $data = $request->all();
        if(isset($data['files'])){
            $imageProducto = $this->imagenService->guardarImagen($this->storageDisk, $request->file('files'), $this->rutaArchivos);
        }
        $producto = WiqliProducto::find($id);
        $producto->update([
            'cantidad_minima' => $request->cantidad_minima,
            'categoria_id' => $request->categoria_id,
            'imagen' => isset($imageProducto) ? $imageProducto[0]['full_path'] : $producto->imagen,
            'nombre' => $request->nombre,
            'precio_unitario' => $request->precio_unitario,
            'disponibilidad_limitada' => $request->disponibilidad_limitada,
            'unidad_id' => $request->unidad_id,
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Producto actualizado correctamente.'
        ]);
    }

    public function eliminar(Request $request, $id)
    {
        $producto = WiqliProducto::find($id);
        $producto->update([
            'activo' => false
        ]);
        $producto->delete();
        return response()->json([
            'state'=> 1,
            'message' => 'Producto eliminado correctamente.'
        ]);
    }

    public function obtenerProductoByNombre(Request $request)
    {
        $productos = WiqliProducto::where('nombre','like', '%'.$request->nombre.'%')->orderBy('nombre', 'asc')->get();
        if(!count($productos)){
            $producto = WiqliProducto::where('nombre', 'Otro producto')->orderBy('nombre', 'asc')->get();
            return $producto;
        }
        return $productos;
    }

    public function actualizarEstadoProducto($id)
    {
        $producto = WiqliProducto::find($id);
        $producto->update([
            'activo' => !$producto->activo
        ]);
        return response()->json([
            'state'=> 1,
            'message' => 'Estado del producto actualizado correctamente.'
        ]);
    }
}
