<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Wiqli\WiqliCliente;

class WiqliCuponDescuentoController extends Controller
{
    public function verificarCupon($codigo, $correo)
    {
        $cupon = CuponDescuento::where('codigo', $codigo)->first();
        $cliente = WiqliCliente::where('correo', $correo)->first();
        // $existeReferente = CuponDescuento::where('correo_creador', $cupon->correo_creador)
        //                 ->where('proveedor', 1)
        //                 ->where('activo', 1)
        //                 ->first();
        if($cupon)
        {
            //$cupon->publico == 1  ==> apto para todos
            //$cupon->publico == 2  ==> apto para solo nuevos
            //$cupon->publico == 3  ==> apto para solo existentes

            
            if($cupon->proveedor == 1)
            {
                $referidoExiste = ClienteReferidoCupon::where('correo_referido', $correo)->first();
                
                if($referidoExiste)
                {
                    return response()->json([
                        'state' => false,
                        'message' => 'Ya accediste a un descuento con anterioridad.'
                    ]);
                }

                if($cupon->correo_creador == $correo){
                    return response()->json([
                        'state' => false,
                        'message' => 'No puedes aplicar a tu mismo cupón.'
                    ]);
                }

            }
                

            if($cupon->proveedor == 2)
            {
                if($cupon->publico == 1)
                {
                    if($cliente)
                    {
                        return response()->json([
                            'state' => false,
                            'message' => 'Solo puede acceder al cupón una vez.'
                        ]);
                    }
                }

                if($cupon->publico == 2)
                {
                    if($cliente)
                    {
                        return response()->json([
                            'state' => false,
                            'message' => 'Cupón válido solo para nuevos clientes.'
                        ]);
                    }
                }

            }

            return response()->json([
                'state' => true,
                'monto' => $cupon->monto,
                'tipo' => $cupon->tipo,
                'message' => "Data servida"
            ]);
        }
        return response()->json([
            'state' => false,
            'message' => 'El cupon no existe.'
        ]);
    }
}