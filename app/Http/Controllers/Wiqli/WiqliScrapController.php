<?php

namespace App\Http\Controllers\Wiqli;

use Illuminate\Http\Request;
use App\Models\ProductoExterno;
use App\Models\PrecioProductoExterno;
use App\Models\Wiqli\WiqliProducto;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class WiqliScrapController extends Controller
{
    public function almacenarEnCacheVea(Request $request)
    {
        $data = $request->all();
        Cache::put('dataScrapVea', $data, now()->addMinutes(300));
        return response()->json([
            'state'=> 1,
            'message' => 'Data vea guardada en caché creada correctamente.'
        ]);
    }

    public function almacenarEnCacheJuntoz(Request $request)
    {
        $data = $request->all();
        Cache::put('dataScrapJuntoz', $data, now()->addMinutes(300));
        return response()->json([
            'state'=> 1,
            'message' => 'Data juntoz guardada en caché creada correctamente.'
        ]);
    }

    public function almacenarEnCacheTottus(Request $request)
    {
        $data = $request->all();
        Cache::put('dataScrapTottus', $data, now()->addMinutes(300));
        return response()->json([
            'state'=> 1,
            'message' => 'Data tottus guardada en caché creada correctamente.'
        ]);
    }

    public function almacenarEnCacheWong(Request $request)
    {
        $data = $request->all();
        Cache::put('dataScrapWong', $data, now()->addMinutes(300));
        return response()->json([
            'state'=> 1,
            'message' => 'Data wong guardada en caché creada correctamente.'
        ]);
    }

    public function crearPreciosVea()
    {
        $data = Cache::get('dataScrapVea');
        foreach ($data as $key => $producto) {
            try {
                $productowiqli = ProductoExterno::find($producto['id']);
                $producto2 = WiqliProducto::find($productowiqli->wiqli_producto_id);
                PrecioProductoExterno::create([
                    'tienda_id' => 1,
                    'producto_externo_id' => $producto['id'],
                    'precio_unitario_online' => $producto['precioOnline'],
                    'precio_unitario_wiqli' => $producto2->precio_unitario,
                    'multiplicador' => $productowiqli->multiplicador_vea,
                    'precio_unitario_regular' => $producto['precioRegular'],
                    'fecha_actualizacion' => Carbon::now()->toTimeString()
                ]);

                $productowiqli->update([
                    'precio_vea' => $productowiqli->multiplicador_vea * $producto['precioOnline']
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
            
        }
        return response()->json([
            'state'=> 1,
            'message' => 'Precios vea creados correctamente.'
        ]);
    }

    public function crearPreciosTottus()
    {               
        $data = Cache::get('dataScrapTottus');
        foreach ($data as $key => $producto) {
            try {
                $productowiqli = ProductoExterno::find($producto['id']);
                $producto2 = WiqliProducto::find($productowiqli->wiqli_producto_id);
                PrecioProductoExterno::create([
                    'tienda_id' => 2,
                    'producto_externo_id' => $producto['id'],
                    'precio_unitario_online' => $producto['precioOnline'],
                    'precio_unitario_wiqli' => $producto2->precio_unitario,
                    'multiplicador' => $productowiqli->multiplicador_tottus,
                    'fecha_actualizacion' => Carbon::now()->toTimeString()
                ]);
                $productowiqli->update([
                    'precio_tottus' => $productowiqli->multiplicador_tottus * $producto['precioOnline'],
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        return response()->json([
            'state'=> 1,
            'message' => 'Precios tottus creados correctamente.'
        ]);
    }

    public function crearPreciosWong()
    {
        $data = Cache::get('dataScrapWong');
        foreach ($data as $key => $producto) {
            try {
                $productowiqli = ProductoExterno::find($producto['id']);
                $producto2 = WiqliProducto::find($productowiqli->wiqli_producto_id);
                PrecioProductoExterno::create([
                    'tienda_id' => 3,
                    'producto_externo_id' => $producto['id'],
                    'precio_unitario_online' => $producto['precioOnline'],
                    'precio_unitario_wiqli' => $producto2->precio_unitario,
                    'multiplicador' => $productowiqli->multiplicador_wong,
                    'fecha_actualizacion' => Carbon::now()->toTimeString()
                ]);
                $productowiqli->update([
                    'precio_wong' => $productowiqli->multiplicador_wong * $producto['precioOnline'],
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        return response()->json([
            'state'=> 1,
            'message' => 'Precios wong creados correctamente.'
        ]);
    }

    public function verDataCacheVea()
    {
        $data = Cache::get('dataScrapVea');
        return $data;
    }

    public function verDataCacheJuntoz()
    {
        $data = Cache::get('dataScrapJuntoz');
        return $data;
    }

    public function verDataCacheTottus()
    {
        // $hola = "s/ 15.9<span class=\"priceUnit\"> UN</span>";
        // $posicion = strpos($hola, "<");
        // return $posicion;
        $data = Cache::get('dataScrapTottus');
        return $data;
        // $new_data = [];
        // foreach ($data as $key => $value) {
        //     $posicionBorrar = strpos($value, "<");
        //     $newStr = substr($value, 3, $posicionBorrar - 3);
        //     array_push($new_data, $newStr);
        // }
        // return $new_data;
    }

    public function verDataCacheWong()
    {
        $data = Cache::get('dataScrapWong');
        return $data;
    }

    public function obtenerProductosScraping()
    {
        $productos = ProductoExterno::all();
        return $productos;
    }
}
