<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Suscripcion;
use App\Mail\Cliente\RecordatorioPedido;
use Mail;

class ExampleController extends Controller
{
    public function recordatorioSuscripcion()
    {
        $today = date('d-m-Y');

        $suscripciones = Suscripcion::with(['usuario.distrito', 'productos'])
            ->where('isActive', 1)
            ->where('fechaCobro', $today)
            ->get();
        
        $respuestas = [];
        foreach ($suscripciones as $key => $suscripcion) {
            try {
                Mail::to('royer@repo.com.pe')->send(new RecordatorioPedido(
                    $suscripcion, $suscripcion->usuario, $suscripcion->productos
                ));

            } catch (\Throwable $th) {
                
            }
        }

        return "tarea completada";
    }

    public function actualizarProductosExternosWong()
    {
        $productos = ProductoExterno::all();
        foreach ($productos as $key => $producto) {
            $ultimoPrecio = PrecioProductoExterno::where('producto_externo_id', $producto->id)
                            ->where('tienda_id', 3)
                            ->latest()
                            ->first();
            if($ultimoPrecio){
                if($ultimoPrecio->precio_unitario_online && $ultimoPrecio->multiplicador)
                {
                    $producto->update([
                        'precio_wong' => $ultimoPrecio->precio_unitario_online * $ultimoPrecio->multiplicador
                    ]);
                }
            }
        }
        return "Tarea terminada";
    }

    public function actualizar()
    {
        $precios = PrecioProductoExterno::all();
        foreach ($precios as $key => $precio) {
            $productoExterno = ProductoExterno::find($precio->producto_externo_id);
            $productoWiqli = WiqliProducto::find($productoExterno->wiqli_producto_id);
            
            $precio->update([
                'precio_unitario_wiqli' => $productoWiqli->precio_unitario
            ]);
        }
        return "Tarea terminada";
    }

    public function actualizarVictor()
    {
        $precios = PrecioProductoExterno::all();
        foreach ($precios as $key => $precio) {
            $productoExterno = ProductoExterno::find($precio->producto_externo_id);
            if($precio->tienda_id == 1)
            {
                $precio->update([
                    'multiplicador' => $productoExterno->multiplicador_vea
                ]);
            }
            if($precio->tienda_id == 2)
            {
                $precio->update([
                    'multiplicador' => $productoExterno->multiplicador_tottus
                ]);
            }
            if($precio->tienda_id == 3)
            {
                $precio->update([
                    'multiplicador' => $productoExterno->multiplicador_wong
                ]);
            }
        }
        return "Tarea terminada";
    }

    public function crearPedido(Request $request)
    {
        $id = 39;
        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $id)
                    ->get();
        
        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }
        $productosParaDescuento = ProductoExterno::whereIn('wiqli_producto_id', $idsProductosInternos)->get();
        
        $totalAhorro = $this->calcularTotalDescuento($idsProductosInternos, $id);
        return $totalAhorro;

    }

    public function verCorreos()
    {
        $cliente = WiqliCliente::distinct()->get();
        $clientes = $cliente->unique('correo');
        return $clientes;
    }

    public function verPdf($id)
    {
        //return Excel::download(new PedidoExport, 'reporte.xlsx');
        $pedido = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])->where('id', $id)->get();
        $clientesReferidos = ClienteReferidoCupon::where('correo_referente', $pedido[0]->cliente->correo)
                        ->where('pedido_referente_cobrador_id', $id)
                        ->get();
        $cupon = null;
        if($pedido[0]['cupon_id'])
        {
            $cupon = CuponDescuento::find($pedido[0]['cupon_id']);
        }


        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $id)
                    ->where('status', 1)
                    ->get();

        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }
        return $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $id));
        //$totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $id));


        $data = [
            'productos' => $pedido[0]['detalle'],
            'pedido' => $pedido[0],
            'referidos' => $clientesReferidos,
            'cupon' => $cupon,
            'ahorroExterno' => $totalAhorro
        ];
        $pdf = Pdf::loadView('exports.pedido', $data);
        return $pdf->download('pedido_'.$data['pedido']['cliente']['nombres'].'.pdf');
    }

    public function calcularTotalDescuento($idsProductosInternos, $pedidoId)
    {   
        //$totalProductosInternos = WiqliPedido::find($pedidoId)->totalProductos;
        
        $totalProductosInternos = 0;
        $totalProductosExternos = 0;
        $detalles = [];
        $precios1 = [];
        $precios2 = [];
        $precios3 = [];
        foreach ($idsProductosInternos as $key => $id) {
            $detallePedido = WiqliDetallePedido::where('pedido_id', $pedidoId)->where('producto_id', $id)->first();
            array_push($detalles, $detallePedido);
            $subTotal = 0;
            $precio1 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 1)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio2 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 2)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio3 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 3)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $totalNumeroPrecios = 0;
            $totalPrecioExterno = 0;
            if($precio1){
                array_push($precios1, $precio1);
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio1->precio_unitario_online * $precio1->multiplicador;
            }
            if($precio2){
                array_push($precios2, $precio2);
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio2->precio_unitario_online * $precio2->multiplicador;
            }
            if($precio3){
                array_push($precios3, $precio3);
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio3->precio_unitario_online * $precio3->multiplicador;
            }

            if($precio1 || $precio2 || $precio3)
            {
                $totalProductosInternos += $detallePedido->total;
            }

            if($totalNumeroPrecios > 0 && $totalPrecioExterno>0){
                if($detallePedido){
                    $subTotal = $detallePedido->cantidad * $totalPrecioExterno/$totalNumeroPrecios;
                }
            }
            $totalProductosExternos += $subTotal;
        }
        if($totalProductosInternos > 0 && $totalProductosExternos>0){
            return ($totalProductosInternos - $totalProductosExternos);
        }
        return 0;
    }

    public function recordarCompra()
    {
        $clientes = [];
        $cliente = WiqliCliente::find(1);
        Mail::to('royer@repo.com.pe')->send(new RecordatorioCompra($cliente));
        return response()->json([
            'status' => true,
            'message' => "Mensaje enviado"
        ]);
    }

    public function verCorreoVerificacion()
    {
        $user = User::find(16);
        $codigoConfirmacion = \Str::random(25);
        Mail::to('royeribarrao@gmail.com')->send(new VerificacionCuenta($user, $codigoConfirmacion));
        return "tarea realizada";
    }
    
    public function probandoController()
    {
        $user = $this->userService->getRoyer();
        return $user;
    }

}
