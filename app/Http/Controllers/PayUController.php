<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SdkPayU\lib\PayU;
use App\SdkPayU\lib\PayU\PayUPayments;
use App\SdkPayU\lib\PayU\PayUTokens;
use App\SdkPayU\lib\PayU\util\PayUParameters;
use App\SdkPayU\lib\PayU\api\PayUCountries;
use App\SdkPayU\lib\PayU\api\Environment;
use App\SdkPayU\lib\PayU\PayUReports;
use App\Models\TuRepo\RepoDatosDelivery;
use App\Models\TuRepo\RepoServicio;
use App\Models\PayUTransaccion;
use App\Models\DatosDelivery;
use App\Models\Gestion;
use App\Models\User;
use App\Models\Producto;
use App\Models\Tienda;
use App\Models\NuevoProductoServicioCambio;
use App\Models\ProductoGestion;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Mail\TuRepo\ConfirmacionProceso;
use App\Mail\Repo\AvisoPedidoRepo;
use App\Mail\ConfirmacionPedido;
use App\Mail\Tienda\NuevoProceso;
use App\Models\TuRepo\TuRepoCuponDescuento;
use Mail;

class PayUController extends Controller
{
    public function pagarPedido($total, $datosTarjeta, $cliente)
    {
        $searchString = " ";
        $replaceString = "";
        $numero_tarjeta = str_replace($searchString, $replaceString, $datosTarjeta['numeroTarjeta']);
        $fecha_vencimiento = substr($datosTarjeta['fechaVencimiento'], 0, 4).'/'.substr($datosTarjeta['fechaVencimiento'], 5, 2);
        $reference = "Wiqli".\Str::random(7).random_int(9999, 1000000);
       
        $ultimoCliente = WiqliCliente::latest()->first();
        $clienteId = $ultimoCliente->id + 1;
        $fullName = $cliente['nombres'] . ' ' . $cliente['apellidos'];

        $parameters = array(
            PayUParameters::ACCOUNT_ID => "960756",
            PayUParameters::REFERENCE_CODE => "$reference",
            PayUParameters::DESCRIPTION => "Wiqli",
            PayUParameters::VALUE => "$total",
            PayUParameters::CURRENCY => "PEN",
            PayUParameters::BUYER_ID => "$clienteId",
            PayUParameters::BUYER_NAME => "$fullName",
            PayUParameters::BUYER_EMAIL => $cliente['correo'],
            PayUParameters::BUYER_CONTACT_PHONE => $cliente['telefono'],
            PayUParameters::BUYER_DNI => "700100100",
            PayUParameters::BUYER_STREET => $cliente['direccion'],
            PayUParameters::BUYER_STREET_2 => $cliente['referencia'],
            PayUParameters::BUYER_CITY => "Lima",
            PayUParameters::BUYER_STATE => "Lima",
            PayUParameters::BUYER_COUNTRY => "PE",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => $cliente['telefono'],
            PayUParameters::PAYER_ID => "1",
            PayUParameters::PAYER_NAME => $datosTarjeta['nombreTarjeta'],
            PayUParameters::PAYER_EMAIL => $cliente['correo'],
            PayUParameters::PAYER_CONTACT_PHONE => $cliente['telefono'],
            PayUParameters::PAYER_DNI => "700100100",
            PayUParameters::BUYER_DNI => "DNI",
            PayUParameters::PAYER_STREET => $cliente['direccion'],
            PayUParameters::PAYER_STREET_2 => $cliente['referencia'],
            PayUParameters::PAYER_CITY => "Lima",
            PayUParameters::PAYER_STATE => "Lima",
            PayUParameters::PAYER_COUNTRY => "PE",
            PayUParameters::PAYER_POSTAL_CODE => "000000",
            PayUParameters::PAYER_PHONE => $cliente['telefono'],
            PayUParameters::CREDIT_CARD_NUMBER => $numero_tarjeta,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $fecha_vencimiento,
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> $datosTarjeta['cvv'],
            PayUParameters::PAYMENT_METHOD => $datosTarjeta['tipoBanco'],
            PayUParameters::INSTALLMENTS_NUMBER => "1",
            PayUParameters::COUNTRY => PayUCountries::PE,
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
            PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );
        $response = PayUPayments::doAuthorizationAndCapture($parameters);
        
        if(!($response->code)){
            return response()->json([
                'state'=> false,
                'message' => $response->error
            ]);
        }

        $ultimoPedido = WiqliPedido::latest()->first();
        $pedidoId = $ultimoPedido->id + 1;
        
        if($response){
            if($response->transactionResponse->responseCode == "APPROVED"){
                $transaction = PayUTransaccion::create([
                    'tipo' => 1,
                    'state' => $response->transactionResponse->state,
                    'orderId' => $response->transactionResponse->orderId,
                    'servicio_id' => $pedidoId,
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => $response->transactionResponse->transactionId,
                    'authorizationCode' => $response->transactionResponse->authorizationCode
                ]);

                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => 'Pago Satisfactorio',
                    'orderId' => $response->transactionResponse->orderId,
                    'transactionId' => $response->transactionResponse->transactionId
                ]);
            }elseif(
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_CONFIRMATION" ||
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_TRANSMISSION"
                ){
                $transaction = PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'servicio_id' => $pedidoId,
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }else{
                PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'servicio_id' => $pedidoId,
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'state'=> false,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }
        }

        return response()->json([
            'state' => false,
            'message' => "Algo salió mal, no existe response"
        ]);
    }

    public function crearToken($datos, $usuarioId)
    {
        $parameters = array(
            // Ingresa aquí el nombre del pagador.
            PayUParameters::PAYER_NAME => $datos['nombreTarjeta'],
            PayUParameters::PAYER_ID => $usuarioId,
            PayUParameters::PAYER_DNI => $datos['dni'],
            PayUParameters::CREDIT_CARD_NUMBER => str_replace(" ", "", $datos['numeroTarjeta']),
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => substr($datos['fechaVencimiento'], 0, 4).'/'.substr($datos['fechaVencimiento'], 5, 2),
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> $datos['cvv'],
            // Ingresa aquí el nombre de la tarjeta de crédito
            PayUParameters::PAYMENT_METHOD => $datos['tipoBanco']
        );
        
        $response = PayUTokens::create($parameters);
        //return $response->creditCardToken;
        if($response){
            //Puedes obtener el token de la tarjeta de crédito 
            //$response->creditCardToken->creditCardTokenId;
            return $response;
        }
        return null;
    }

    public function cobrarSuscripcion($datosSuscripcion, $datosTarjeta)
    {
            //         Nota
            // Para procesar sin CVV es necesario enviar el parámetro PROCESS_WITHOUT_CVV2 omo true en la petición del pago y quitar el parámetro CREDIT_CARD_SECURITY_CODE.
            // Por defecto, no está activo el procesamiento de tarjetas de crédito sin código de seguridad. Si quieres activar esta funcionalidad, contacta a tu representante de ventas.
        $reference = $datosSuscripcion->codigoSuscripcion.\Str::random(7).random_int(9999, 1000000);
        $parameters = array(
            //Ingresa aquí el identificador de la cuenta
            PayUParameters::ACCOUNT_ID => "960756",
            // Ingresa aquí la referencia de pago.
            PayUParameters::REFERENCE_CODE => $reference,
            // Ingresa aquí la descripción del pago.
            PayUParameters::DESCRIPTION => "Wiqli Suscripción",
        
            // -- Valores --
                //Ingresa aquí el valor.
            PayUParameters::VALUE => $datosSuscripcion->montoCobro,
            // Ingresa aquí la moneda.
            PayUParameters::CURRENCY => "PEN",
        
            // -- Comprador --
          // Ingresa aquí la información del comprador.
          //PayUParameters::[...] => [...],
        
        
          // -- Pagador --
          // Ingresa aquí la información del pagador.
          //PayUParameters::[...] => [...],
        
            // -- Datos de la tarjeta de crédito --
          // Ingresa aquí el token de la tarjeta de crédito
            PayUParameters::TOKEN_ID => $datosTarjeta->tokenId,
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => $datosTarjeta->fechaVencimiento,
            // Ingresa aquí el código de seguridad de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> $datosTarjeta->cvv,
            // Ingresa aquí el nombre de la tarjeta de crédito
            PayUParameters::PAYMENT_METHOD => $datosTarjeta->tipoBanco,
        
            // Ingresa aquí el número de cuotas.
            PayUParameters::INSTALLMENTS_NUMBER => "1",
            // Ingresa aquí el nombre del país.
            PayUParameters::COUNTRY => PayUCountries::PE,
        
            // Device Session ID
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            // IP del pagador
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            // Cookie de la sesión actual
            PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
            // User agent de la sesión actual
            PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
            );
        
        // Petición de "Autorización y captura"
        
        $response = PayUPayments::doAuthorizationAndCapture($parameters);
        if(!($response->code)){
            
        }
        
        if($response){
            if($response->transactionResponse->responseCode == "APPROVED"){
                $transaction = PayUTransaccion::create([
                    'tipo' => 1,
                    'state' => $response->transactionResponse->state,
                    'orderId' => $response->transactionResponse->orderId,
                    'suscripcionId' => $datosSuscripcion->id,
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => $response->transactionResponse->transactionId,
                    'authorizationCode' => $response->transactionResponse->authorizationCode
                ]);

                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => 'Pago Satisfactorio',
                    'orderId' => $response->transactionResponse->orderId,
                    'transactionId' => $response->transactionResponse->transactionId
                ]);
            }elseif(
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_CONFIRMATION" ||
                    $response->transactionResponse->responseCode == "PENDING_TRANSACTION_TRANSMISSION"
                ){
                $transaction = PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'suscripcionId' => $datosSuscripcion->id,
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'id' => $transaction->id,
                    'state'=> true,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }else{
                PayUTransaccion::create([
                    'tipo' => 2,
                    'state' => $response->transactionResponse->state,
                    'orderId' => isset($response->transactionResponse->orderId) ?  $response->transactionResponse->orderId : '',
                    'suscripcionId' => $datosSuscripcion->id,
                    'responseCode' => $response->transactionResponse->responseCode,
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : '',
                    'paymentNetworkResponseErrorMessage' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : ''
                ]);
                return response()->json([
                    'state'=> false,
                    'message' => isset($response->transactionResponse->paymentNetworkResponseErrorMessage) ? $response->transactionResponse->paymentNetworkResponseErrorMessage : '',
                    'orderId' => isset($response->transactionResponse->orderId) ? $response->transactionResponse->orderId : '',
                    'transactionId' => isset($response->transactionResponse->transactionId) ? $response->transactionResponse->transactionId : ''
                ]);
            }
        }

        return response()->json([
            'state' => false,
            'message' => "Algo salió mal, no existe response"
        ]);
    }

    public function ping()
    {
        $response = PayUPayments::doPing();
        return $response;
    }

    public function getEnvironment()
    {
        $environment = Environment::getPaymentsUrl();
        return $environment;
    }

    public function getMetodosDePago()
    {
        $array=PayUPayments::getPaymentMethods();
        $payment_methods=$array->paymentMethods;
        $royer = [];
        foreach ($payment_methods as $key => $payment_method){
            $payment_method->country;
            $payment_method->description;
            $payment_method->id;
            $royer[$key] = $payment_method;
        }
        return $royer;
    }

    public function consultaPorIdentificadorDelaOrden()
    {
        // Incluye aquí la referencia de la orden.
        $parameters = array(PayUParameters::ORDER_ID => "44469220");

        $order = PayUReports::getOrderDetail($parameters);

        if ($order) {
            $order->accountId;
            $order->status;
            $order->referenceCode;
            $order->additionalValues->TX_VALUE->value;
            $order->additionalValues->TX_TAX->value;
            if ($order->buyer) {
                $order->buyer->emailAddress;
                $order->buyer->fullName;
            }
            $transactions=$order->transactions;
            foreach ($transactions as $transaction) {
                $transaction->type;
                $transaction->transactionResponse->state;
                $transaction->transactionResponse->paymentNetworkResponseCode;
                $transaction->transactionResponse->trazabilityCode;
                $transaction->transactionResponse->responseCode;
                if ($transaction->payer) {
                    $transaction->payer->fullName;
                    $transaction->payer->emailAddress;
                }
            }
        }

    }

    public function consultaPorIdentificadorDelaTransaccion($transactionId)
    {
        $parameters = array(PayUParameters::TRANSACTION_ID => $transactionId);

        $response = PayUReports::getTransactionResponse($parameters);

        // if ($response) {
        //     $response->state;
        //     $response->trazabilityCode;
        //     $response->authorizationCode;
        //     $response->responseCode;
        //     $response->operationDate;
        // }
        return $response;
    }

    public function consultaPorIdentificadorDelaReferencia()
    {
        // Incluye aquí la referencia de la orden.
        $parameters = array(PayUParameters::REFERENCE_CODE => "payment_test_1625093692957");

        $response = PayUReports::getOrderDetailByReferenceCode($parameters);

        foreach ($response as $order) {
            $order->accountId;
            $order->status;
            $order->referenceCode;
            $order->additionalValues->TX_VALUE->value;
            $order->additionalValues->TX_TAX->value;

            if ($order->buyer) {
                $order->buyer->emailAddress;
                $order->buyer->fullName;
            }

            $transactions=$order->transactions;
            foreach ($transactions as $transaction) {
                $transaction->type;
                $transaction->transactionResponse->state;
                $transaction->transactionResponse->paymentNetworkResponseCode;
                $transaction->transactionResponse->trazabilityCode;
                $transaction->transactionResponse->responseCode;
                if ($transaction->payer) {
                    $transaction->payer->fullName;
                    $transaction->payer->emailAddress;
                }
            }
        }

    }

    public function eliminarTarjeta($usuarioId, $tokenId)
    {
        $parameters = array(
            PayUParameters::PAYER_ID => $usuarioId,
            PayUParameters::TOKEN_ID => $tokenId       
        );
        
        $response=PayUTokens::remove($parameters);
        
        if($response){
            return response()->json([
                'state' => true,
                'message' => "Tarjeta eliminada correctamente."
            ]);
        }
        return response()->json([
            'state' => false,
            'message' => "Hubo problemas en eliminar la tarjeta."
        ]);
    }

    public function getListaTokensPayU()
    {
        // -- Parámetros opcionales --
        $parameters = array(
            // Ingresa aquí el identificador del pagador.
            PayUParameters::PAYER_ID => "",
            // Ingresa aquí identificador el token.
            PayUParameters::TOKEN_ID => "",
            // Ingresa la fecha de inicio y fin para filtrar por rango de fechas. Optional.
            PayUParameters::START_DATE=> "2023-04-01T12:00:00",
            PayUParameters::END_DATE=> "2023-05-01T12:00:00"
        );
        
        $response=PayUTokens::find($parameters);
        if($response->code) {
            $credit_cards = $response->creditCardTokenList;
            // foreach ($credit_cards as $credit_card) { 
            //     $credit_card->creationDate;
            //     $credit_card->creditCardTokenId;
            //     $credit_card->identificationNumber;
            //     $credit_card->maskedNumber;
            //     $credit_card->name;
            //     $credit_card->payerId;
            //     $credit_card->paymentMethod;
            // }   
            return $response->creditCardTokenList;
        }

        return [];
        return response()->json([
            'state' => false,
            'message' => "Hubo problemas en el proveedor."
        ]);
    }
}