<?php

namespace App\Http\Controllers;

use App\Services\Contracts\UserInterface as UserService;
use App\Services\Contracts\MaintenanceSedeInterface as SedeService;
use Illuminate\Http\Request;
use App\Models\Gestion;
use App\Models\Rol;
use App\Models\User;
use App\Models\CuponDescuento;
use App\Models\Suscripcion;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliBilletera;

class UserController extends Controller
{
    protected $userService;
    protected $sedeService;

    public function __construct(
        UserService $user,
        SedeService $sede

    )
    {
        $this->sedeService = $sede;
        $this->userService = $user;
    }

    public function get(Request $request) {
        
        $req = $request->all();
        $users = $this->userService->paginate(15, $req);    
        return response()->json($users);
    }

    public function getAuthenticated() 
    {
        $usuario = \Auth::user();
        $usuario->informacion = \Auth::user()->informacion;
        $ultimaCompra = WiqliPedido::where('usuario_id', \Auth::user()->id)
                        ->latest()
                        ->first();
        $productos = [];
        if($ultimaCompra){
            $detallePedido = WiqliDetallePedido::where('pedido_id', $ultimaCompra->id)->get();
            foreach ($detallePedido as $key => $detalle) {
                $producto = WiqliProducto::find($detalle->producto_id);
                $producto->cantidad = 1;
                array_push($productos, $producto);
            }
        }
        $usuario->productos = $productos;
        return $usuario;
    }

    public function getUltimaCompra()
    {
        $usuario = \Auth::user();
        $ultimaCompra = WiqliPedido::where('usuario_id', \Auth::user()->id)
                        ->latest()
                        ->first();
        $productos = [];
        if($ultimaCompra){
            $detallePedido = WiqliDetallePedido::where('pedido_id', $ultimaCompra->id)->get();
            foreach ($detallePedido as $key => $detalle) {
                $producto = WiqliProducto::with(['unidad', 'categoria'])->find($detalle->producto_id);
                $producto->cantidad = 1;
                array_push($productos, $producto);
            }
        }
        return $productos;
    }

    public function getTotalDescuentoReferidos()
    {
        $usuario = \Auth::user();
        $pedidos = WiqliPedido::where('usuario_id', \Auth::user()->id)->get();
        $totalDescuento = 0;

        foreach ($pedidos as $key => $pedido) {
            $totalDescuento += $pedido->descuento;
        };

        return $totalDescuento;
    }

    public function getCuponDescuento()
    {
        $usuario = \Auth::user();
        $cupon = CuponDescuento::where('correo_creador', \Auth::user()->email)->latest()->first();
        
        if($cupon){
            return response()->json([
                'state' => true,
                'cupon' => $cupon->codigo,
                'message' => "Cupon servidor"
            ]);
        }

        return response()->json([
            'state' => false,
            'cupon' => false,
            'message' => "Aún no ha creado su cupón"
        ]);
    }

    public function getInformacion()
    {
        $usuario = \Auth::user();
        $billetera = WiqliBilletera::where('usuarioId', $usuario->id)->first();
        if(!$billetera){
            $billetera =  WiqliBilletera::create([
                'usuarioId' => $usuario->id
            ]);
        }
        $user = User::with(['billetera', 'distrito'])->find($usuario->id);
        return $user;
    }

    public function getProductosSuscripcion()
    {
        $usuario = \Auth::user();
        $suscripcion = Suscripcion::with('productos.producto')
            ->where('usuarioId', $usuario->id)
            ->where('isActive', 1)
            ->first();
        return $suscripcion;
    }

    public function store(Request $request) {

        $this->userService->create($request->all());           
        return response()->json([
            'state'=> 1,
            'message' => 'Usuario creado correctamente.'
        ]);
    }
    
    public function loginCliente(Request $request){
        return response()->json([
            'state'=> 200,
            'message' => 'Login correcto',
            'data' => $request->all()
        ]);
    }

    public function show($id) {
        $user = $this->userService->find($id);
        return response()->json($user);
    }

    public function update($id, Request $request) {
        $this->userService->update($request->all(), $id);
        return response()->json([
            'state'=> 1,
            'message' => 'Usuario actualizado correctamente.'
        ]);
    }

    public function getProfiles(){
        $roles = Rol::all();
        $profiles = $this->userService->getProfiles();
        return $roles;
    }

    public function logout() {
        $user = auth()->user();
        $user->tokens->each(function($token, $key) {
            $token->delete();
        });
    }

    public function cambiarEstado($id)
    {
        $user = User::find($id);
        $user->update([
            'state' => !$user->state
        ]);
        return response()->json([
            'state'=> true,
            'message' => "Usuario actualizado"
        ]);
    }
}
