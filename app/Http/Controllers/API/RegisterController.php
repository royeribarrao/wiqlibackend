<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CuponDescuento;
use Illuminate\Support\Facades\Validator;
use App\Mail\VerificacionCuenta;
use Mail;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $codigoConfirmacion = \Str::random(25);

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        if($validator->fails()){
            return response()->json([
                'state'=> false,
                'message' => 'Asegurese de no estar registrado.'
            ]);
        }else{
            $user = User::create([
                'email' => $request->email,
                'password' => $request->password,
                'confirmation_code' => $codigoConfirmacion,
                'name' => $request->nombres,
                'father_lastname' => $request->apellidos,
                'phone' => $request->telefono,
                'rol_id' => 3
            ]);

            $cupon = CuponDescuento::create([
                'codigo' => \Str::random(7),
                'monto' => 5.00,
                'correo_creador' => $request->email,
                'tipo' => 2,
                'proveedor' => 1,
                'activo' => 1
            ]);
            Mail::to($request['email'])->send(new VerificacionCuenta($user, $codigoConfirmacion));
        }
    
        return response()->json([
            'state' => true,
            'message' => 'Bienvenido a Wiqli, le enviaremos un correo de confirmación.'
        ]);
    }

    public function confirmacion($codigo)
    {
        $user = User::where('confirmation_code', $codigo)->first();
        if(!$user)
        {
            return response()->json([
                'state' => false,
                'message' => 'Petición no permitida.'
            ]);
        }
        if($user->confirmed){
            return response()->json([
                'state' => 204,
                'message' => 'Este correo ya ha sido verificado.'
            ]);
        }

        $user->update([
            'confirmed' => true
        ]);
        $user->update([
            'email_verified_at' => $user->updated_at
        ]);
        return response()->json([
            'state' => 200,
            'message' => 'Verificación correcta. ya puede usar su cuenta Wiqli.'
        ]);
    }
}