<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Contracts\PatientInterface as PatientService;
use Codedge\Fpdf\Fpdf\Fpdf;

class PdfController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $patientService;

    public function __construct(
        PatientService $patientService
    )
    {
        $this->patientService = $patientService;
    }

    public function studyOrders(Request $request) {
        $data = $request->all();
        $newData = json_decode($data['data'], true);
        $studies = $newData['studies'];
        $studyCause = $newData['presumptive_diagnosis'] ? $newData['presumptive_diagnosis'] : '' ;
        $pacienteData = $newData['pacienteDataExport'];
        $pacienteFechaNacim = $pacienteData['birthdate'];
     
            if (isset($newData["radiologiaOtros"])){
                $radioOtros = $newData["radiologiaOtros"];
            }
            if (isset($newData["ecografiaOtros"])){
                $ecoOtros = $newData["ecografiaOtros"];
            }
            if (isset($newData["resoOtros"])){
                $resoOtros = $newData["resoOtros"];
            }
            if (isset($newData["tomoOtros"])){
                $tomoOtros = $newData["tomoOtros"];
            }
            function getEdad($fecha_nacimiento) { 
                $tiempo = strtotime($fecha_nacimiento); 
                $ahora = time(); 
                $edad = ($ahora-$tiempo)/(60*60*24*365.25); 
                $edad = floor($edad); 
                return $edad; 
            } 
            $hoy = date("d") . " - " . date("m") . " - " . date("Y");

        $pdf = new Fpdf('P', 'mm', array(80,297));
        $pdf->AddPage('P',array(207,290.8));

        //cabecera izquierda 1
        $pdf->Cell(96.5, 15, '',0,1);
        $pdf->Image(base_path('public').'/images/marca-agua-sinFondo.png',30, 90, 150);
        $pdf->Image(base_path('public').'/images/membrete-header.jpg',10, 13, 192);

       
        //Información del paciente
        $pdf->SetXY(10, 36);
        $pdf->SetFont('Arial','B',8.4);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(38, 2.8, 'APELLIDOS Y NOMBRES:');

        $pdf->SetFont('Arial','',8.4);
        $nombreCorto = substr( strtoupper($pacienteData['fullname']) , 0, 33) . '.';
        $pdf->Cell(70, 2.8, strtoupper($nombreCorto));

        $pdf->SetFont('Arial','B',8.4);
        $pdf->Cell(1, 2.8, 'EDAD:');
        

        $pdf->SetFont('Arial','',8.4);
        if (getEdad($pacienteFechaNacim) <= 9) {
            $pdf->Cell(22.5, 2.8,"0". getEdad($pacienteFechaNacim) . utf8_decode(" años"),0, 0, 'R');
            
        }else{
            $pdf->Cell(22.5, 2.8, getEdad($pacienteFechaNacim) . utf8_decode(" años"),0, 0, 'R');
        }

        $pdf->Cell(26.5, 2.8, '');
        $pdf->SetFont('Arial','B',8.4);

        $pdf->Cell(23.5, 2.8, 'DNI:');
        $pdf->SetFont('Arial','',8.4);
        $pdf->Cell(10, 2.8, $pacienteData['dni'], 0, 0, 'R');

        $pdf->SetXY(10, 41);
        $pdf->SetFont('Arial','B',8.4);

        $pdf->Cell(10, 2.8, 'SEDE:');
        $pdf->SetFont('Arial','',8.4);
        $pdf->Cell(148, 2.8, strtoupper($pacienteData['sede']));
        $pdf->SetFont('Arial','B',8.4);
        $pdf->Cell(12, 2.8, 'FECHA:');
        $pdf->SetFont('Arial','',8.4);
        $pdf->Cell(22, 2.8, $hoy,0,0, 'R');

        //primeras pruebas
        $pdf->SetXY(10, 48);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFillColor(0, 74, 197);
        $pdf->SetFont('Arial','B',14);
        $pdf->Cell(191, 9.3, utf8_decode('RADIOLOGÍA DIGITAL'),0,1,'C','B,T,R,L');
        $pdf->SetX(10);
        $pdf->SetFillColor(243,25,25);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(191, 5.2, utf8_decode('TRAER PLACAS IMPRESAS OBLIGATORIAMENTE (NO SE ACEPTAN CDs)'),0,1,'C','B,T,R,L');
        $pdf->SetX(10);
        $pdf->SetFillColor(169, 217, 255 );
        $pdf->SetFont('Arial','B',8);
        $pdf->SetTextColor(0,0,0);
        $pdf->Cell(191, 5.1, utf8_decode('LOS ESTUDIOS SOLICITADOS NO REQUIEREN PREPARACIÓN DEL PACIENTE'),0,1,'C','T,R,L,B');

        //marcacion de las primeras pruebas
        //primera linea
        $pdf->SetX(10);
        $pdf->SetFillColor(243,25,25);
        $pdf->Cell(200, 4.7, utf8_decode(''),0,1,'C');


        $pdf->SetX(10);

        if (array_search(1, $studies) !== false) {
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Cervical F y L'),0,'C');


        if(array_search(7, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Pelvis Frontal'),0,'C');


        if(array_search(11, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(56.7, 6, utf8_decode('Hombros comparativos F y L'),0,1,'L');
            //segunda linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);

        $pdf->SetX(10);
        if(array_search(2, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Cervical Funcional'),0,'C');

        if(array_search(8, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Rodillas comparativas'),0,'C');

        if(array_search(12, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(56.7, 6, utf8_decode('Codos comparativo F y O'),0,1,'L');
            //tercera linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if(array_search(3, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(65.5, 6, utf8_decode('Columna Dorsal F y L'),0,'C');

        $pdf->Cell(60, 2, utf8_decode('FRONTAL Y DE PIE'),0,'C');

        if(array_search(13, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(56.7,6, utf8_decode('Mano y muñeca comparativo F y O'),0,1,'L');
            //cuarta linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if(array_search(4, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Dorsal Funcional'),0,'C');

        if(array_search(9, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Rodillas Comparativas'),0,'C');

        if(array_search(14, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(56.7,6, utf8_decode('Cadera Niños'),0,1,'L');

    

            //quinta linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if(array_search(5, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(65.5, 6, utf8_decode('Columna Lumbar F y L'),0,'C');

        $pdf->Cell(60, 2, utf8_decode('LATERAL y DECÚBITO'),0,0,'L');

        if(array_search(15, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(56.7,6, utf8_decode('Pie y Tobillo Comparativo F y L'),0,1,'L');



        //sexta linea
        $pdf->SetX(19);
        $pdf->Cell(200, 2, '',0,1);
        $pdf->SetX(10);
        
        if(array_search(6, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Lumbar Funcional'),0,'C');

        if(array_search(10, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Parrilla Costal'),0,0,'L');
        
        if( isset($radioOtros['study'])){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(5.5, 6, utf8_decode('Otros: '. $radioOtros['study']),0,1,'L');
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(5.5, 6.5, utf8_decode('Otros: '),0,1,'L');
        }
      
        //BLOQUE 2
        $pdf->SetX(10);
        $pdf->Cell(200, 5, '',0,1);
        $pdf->SetX(10);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFillColor(0, 74, 197);
        $pdf->SetFont('Arial','B',14);
        $pdf->Cell(191, 10, utf8_decode('ECOGRAFÍA ESPECIALIZADA'),0, 1,'C','B,T,R,L');
        $pdf->SetX(3.6);
        $pdf->Cell(200, 4.7, '',0,1);
            //primera linea
        $pdf->SetX(10);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial','B',8);
        if(array_search(17, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Cervical (Tiroides,ganglios, Paratiroides)'),0,'C');
        if(array_search(21, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Urológica (Próstata, Vejiga, Riñones)'),0,'C');
        if(array_search(25, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(56.7, 6, utf8_decode('Caderas Lactantes'),0,1,'L');
            //segunda linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if(array_search(18, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Mamas'),0,'C');
        if(array_search(22, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Testicular'),0,'C');
        if(array_search(201, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Hombro Derecho'),0,1,'L');

        }
        else if(array_search(202, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Hombro izquierdo'),0,1,'L');

        }
        else if(array_search(203, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Hombro ambos'),0,1,'L');

        }
        else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Hombro:'),0,1,'L');
        }


            //tercera linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if(array_search(19, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Renal y vías urinarias'),0,'C');
        if(array_search(23, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Abdominal'),0,'C');

        if(array_search(204, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Rodilla derecha'),0,1,'L');
        }
        else if(array_search(205, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Rodilla izquierda'),0,1,'L');
        }
        else if(array_search(206, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Rodilla ambos'),0,1,'L');
        }
        else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Rodilla:'),0,1,'L');
        }

        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if(array_search(20, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Doppler Venoso MM. ll Derecho'),0,'C');
        if(array_search(24, $studies) !== false){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Doppler Venoso MM. ll Izquierdo'),0,'C');


        if( isset($ecoOtros['study'])){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(5.5, 6, utf8_decode('Otros: '. $ecoOtros['study']),0,1,'L');
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(5.5, 6.5, utf8_decode('Otros: '),0,1,'L');
        }

        // TERCER BLOQUE
        $pdf->SetX(10);
        $pdf->Cell(200, 5, '',0,1);
        $pdf->SetX(10);
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFillColor(0, 74, 197 );
        $pdf->SetFont('Arial','B',14);

        
        $pdf->Cell(191, 10,
            $newData['type_study'] === 1 ?
            utf8_decode('RESONANCIA MAGNÉTICA NUCLEAR - 3.0 TESLAS'):
            utf8_decode('TOMOGRAFÍA ESPECIALIZADA'),
            0, 1,'C','B,T,R,L'
        );
        $pdf->SetX(10);
        $pdf->SetFillColor(243,25,25);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(191, 5.2, utf8_decode('INDISPENSABLE TRAER PLACAS IMPRESAS'),0,1,'C','B,T,R,L');

            //primera linea
        $pdf->SetX(10);
        $pdf->Cell(200, 4.7, '',0,1);
        $pdf->SetX(10);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial','B',8);


        if((array_search(32, $studies) !== false) || (array_search(36, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        } else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Cervical SIN Contraste'),0,'C');


        if((array_search(34, $studies) !== false) || (array_search(38, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        } else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Lumbar SIN Contraste'),0,'C');

       


     


        // LOGICA DE RODILLA
        // if((array_search(36, $studies) !== false) || (array_search(38, $studies) !== false)){
        //     $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        // } else{
        //     $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        // }
        // $pdf->Cell(60, 6, utf8_decode('Rodilla:'),0,'C');
        

        if((array_search(307, $studies) !== false) || (array_search(407, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera derecha CON contraste'),0,1,'L');
        } 
        
        else if( (array_search(308, $studies) !== false) || (array_search(408, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera derecha SIN contraste'),0,1,'L');
        } 
        else if ((array_search(309, $studies) !== false) || (array_search(409, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera izquierda CON contraste'),0,1,'L');
        }
        else if ((array_search(310, $studies) !== false) || (array_search(410, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera izquierda SIN contraste'),0,1,'L');
        }
        else if ((array_search(311, $studies) !== false) || (array_search(411, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera ambas CON contraste'),0,1,'L');
        } 
        else if ((array_search(312, $studies) !== false) || (array_search(412, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera ambas SIN contraste'),0,1,'L');
        } else {
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(56.7, 6, utf8_decode('Cadera:'),0,1,'L');
        }
        

            //segunda linea
        $pdf->SetX(10);
        $pdf->Cell(200, 1.4, '',0,1);
        $pdf->SetX(10);
        if((array_search(33, $studies) !== false) || (array_search(37, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
        }
        $pdf->Cell(60, 6, utf8_decode('Columna Dorsal SIN Contraste'), 0, 'C');
        
        
        if((array_search(301, $studies) !== false) || (array_search(401, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla Derecha CON contraste'),0,0,'L');
        } 
        else if ((array_search(302, $studies) !== false) || (array_search(402, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla Derecha SIN contraste'),0,0,'L');
        } 
        else if ((array_search(303, $studies) !== false) || (array_search(403, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla Izquierda CON contraste'),0,0,'L');
        } 
        else if ((array_search(304, $studies) !== false) || (array_search(404, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla Izquierda SIN contraste'),0,0,'L');
        } 
        else if ((array_search(305, $studies) !== false) || (array_search(405, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla Ambos CON contraste'),0,0,'L');
        }
        else if ((array_search(306, $studies) !== false) || (array_search(406, $studies) !== false)){
            $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla Ambos SIN contraste'),0,0,'L');
        }else {
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(60, 6, utf8_decode('Rodilla:'),0,0,'L');
        }
        
        if(count($resoOtros) !== 0 || count($tomoOtros) !== 0){
            if ( count($resoOtros) > 0 ) {
                if( isset($resoOtros['study'])){
                    $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
                    $pdf->Cell(5.5, 6, utf8_decode('Otros: '. $resoOtros['study']),0,1,'L');
                }
            }
            if ( count($tomoOtros) > 0 ) {
                if( isset($tomoOtros['study'])){
                    $pdf->Cell(5.5, 4.5, 'X',1, 0 ,"C");
                    $pdf->Cell(5.5, 6, utf8_decode('Otros: '. $tomoOtros['study']),0,1,'L');
                }
            }
            
        }else{
            $pdf->Cell(5.5, 4.5, '',1, 0 ,"C");
            $pdf->Cell(5.5, 6.5, utf8_decode('Otros: '),0,1,'L');
            
        }

        //Frase
        $pdf->Cell(200, 4, '',0,1);
        $pdf->SetX(9);
        $pdf->SetFont('Arial','B',8);
        $pdf->SetTextColor(48, 135, 207);
        $pdf->Cell(56.7, 6, (('MOTIVO DE ESTUDIO: ' . utf8_decode($studyCause))),0,1,'L');
        //firma
        $pdf->SetFont('Arial','B',40);
        $pdf->SetX(0);
        $pdf->Image(base_path('public').'/images/FirmaDr.jpg',25 , 215, 160);
        
        
        $pdf->Cell(202.4, 18, '',0,1, 'C');
        $pdf->SetX(2);
        $pdf->Image(base_path('public').'/images/membrete-footer.jpg',3.6 , 255, 200);
        //funcion comentada para futuros cambios
        // $pdf->Output(base_path('public').'/pdfs/image.pdf', 'F');
        $pdf->Output();
        exit;
        
    }

}
