<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\Wiqli\WiqliBilletera;
use App\Models\Wiqli\WiqliMovimientoBilletera;
use App\Models\ProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\PedidoExport;
use App\Exports\ExcelPedidosExport;

use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\AvisoUsuario;
use Mail;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Http\Controllers\PayUController;

class ClientePedidoController extends Controller
{
    protected $payUService;
    protected $correosAdmin;

    public function __construct(PayUController $payU)
    {
        $this->payUService = $payU;
        $this->correosAdmin = config('constants.correosAdmin');
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function crearPedido(Request $request)
    {
        
        $cliente = $request->cliente;
        $ultimoPedido = WiqliPedido::latest('id')->first();
        $consumoBilletera = $this->usarBilletera($request->total, $ultimoPedido->id + 1);

        if(isset($cliente['tipoPago']) && $cliente['tipoPago'] == 2){
            $pagar = $this->payUService->pagarPedido($request->total - $consumoBilletera, $request->datosTarjeta, $cliente);
            if($pagar->getData()->state == false){
                return response()->json([
                    'state'=> false,
                    'message' => $pagar->getData()->message
                ]);
            }
        }
        
        $newCliente = $this->guardarCliente($cliente, $request->costoDelivery);
        $newPedido = $this->guardarPedido($newCliente, $cliente, $request->costoDelivery);
        $totalProductos = $this->guardarDetallePedido($request->productos, $newPedido->id);
        $totalProductosExtra = $this->guardarProductosExtra($request->productosExtra, $newPedido->id);

        $newPedido->update([
            'total' => $newPedido->total + $totalProductos,
            'totalProductos' => $totalProductos
        ]);

        $referidoUsaDescuentoReferente = null;
        $cuponRecurrencia = null;
        if($request->cupon)
        {
            $idCupon = CuponDescuento::where('codigo', $request->codigoCupon)->first();
            if($idCupon->proveedor == 1)
            {
                if($newCliente->correo != $idCupon->correo_creador)
                {
                    $monto = $idCupon->tipo == 1 ? ($idCupon->monto * $totalProductos)/100 : $idCupon->monto;
                    $newClienteReferido = ClienteReferidoCupon::create([
                        'cupon_descuento_id' => $idCupon->id,
                        'correo_referido' => $cliente['correo'],
                        'monto' => $monto,
                        'correo_referente' => $idCupon->correo_creador
                    ]);
    
                    $newPedido->update([
                        'total' => $newPedido->total - $monto,
                        'descuento' => $monto,
                        'cupon_id' => $idCupon->id
                    ]);
                    $referidoUsaDescuentoReferente = $monto;
                }
            }
            if($idCupon->proveedor == 2)
            {
                $newClienteReferido = ClienteReferidoCupon::create([
                    'cupon_descuento_id' => $idCupon->id,
                    'correo_referido' => $cliente['correo'],
                    'monto' => $request->descuento,
                    'correo_referente' => $idCupon->correo_creador
                ]);

                $newPedido->update([
                    'total' => $newPedido->total - $request->descuento,
                    'descuento' => $request->descuento,
                    'cupon_id' => $idCupon->id
                ]);
                $referidoUsaDescuentoReferente = $request->descuento;
            }
            if($idCupon->proveedor == 4)
            {
                $cuponRecurrencia = $idCupon;
                $newClienteReferido = ClienteReferidoCupon::create([
                    'cupon_descuento_id' => $idCupon->id,
                    'correo_referido' => $cliente['correo'],
                    'monto' => $request->descuento,
                    'correo_referente' => $idCupon->correo_creador
                ]);

                $newPedido->update([
                    'total' => $newPedido->total - $request->descuento,
                    'descuento' => $request->descuento,
                    'cupon_id' => $idCupon->id
                ]);
            }
        }
        $totalReferidos = ClienteReferidoCupon::select(
                                'clientes_referido_cupon.id',
                                'clientes_referido_cupon.cupon_descuento_id',
                                'clientes_referido_cupon.monto',
                                'clientes_referido_cupon.correo_referido',
                                'clientes_referido_cupon.activo',
                                'cupones_descuento.correo_creador'
                            )
                            ->join('cupones_descuento', 'cupones_descuento.id', 'clientes_referido_cupon.cupon_descuento_id')
                            ->where('cupones_descuento.correo_creador', $newCliente->correo)
                            ->where('clientes_referido_cupon.activo', 1)
                            ->get();
        
        $descuentoReferidos = 0;
        foreach ($totalReferidos as $key => $referido) {
            $descuentoReferidos += $referido->monto;
        }
        $newPedido->update([
            'total' => $newPedido->total - $descuentoReferidos,
            'descuento' => $request->descuento + $descuentoReferidos
        ]);
        
        $cupon = null;

        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $newPedido->id)
                    ->get();
        $productosAdicionales = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '=', 999)
                    ->where('pedido_id', $newPedido->id)
                    ->get();

        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }
        $productosParaDescuento = ProductoExterno::whereIn('wiqli_producto_id', $idsProductosInternos)->get();
        $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $newPedido->id));

        Mail::to($newCliente->correo)->send(new AvisoUsuario(
            $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
            $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
        ));

        foreach ($this->correosAdmin as $key => $value) {
            Mail::to($value)->send(new AvisoDashboard(
                $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
                $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
            ));
        }

        foreach ($totalReferidos as $key => $referido) {
            $referidoUpdate = ClienteReferidoCupon::find($referido->id);
            $referidoUpdate->update([
                'activo' => false,
                'pedido_referente_cobrador_id' => $newPedido->id
            ]);
        }

        return response()->json([
            'state'=> 1,
            'message' => 'Pedido creado correctamente.'
        ]);
    }

    public function guardarDetallePedido($productos = [], $pedido_id)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedido_id,
                'producto_id' => $producto['id'],
                'cantidad' => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] : $producto['cantidad'],
                'cantidad_minima' => $producto['cantidad_minima'],
                'precio_unitario' => $producto['precio_unitario'],
                'unidad_medida' => $producto['unidad']['nombre'],
                'total'  => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario']
            ]);
            $total += $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario'];
        }
        return $total;
    }

    public function guardarProductosExtra($productos = [], $pedidoId)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedidoId,
                'categoriaId' => $producto['categoriaId'],
                'producto_id' => 999,
                'nombre_desc' => $producto['nombre_desc'],
                'cantidad_desc' => $producto['cantidad_desc']
            ]);
        }
        return $total;
    }

    public function calcularTotalDescuento($idsProductosInternos, $pedidoId)
    {   
        $totalProductosInternos = 0;
        $totalProductosExternos = 0;
        foreach ($idsProductosInternos as $key => $id) {
            $detallePedido = WiqliDetallePedido::where('pedido_id', $pedidoId)->where('producto_id', $id)->first();
            $subTotal = 0;
            $precio1 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 1)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio2 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 2)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio3 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 3)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $totalNumeroPrecios = 0;
            $totalPrecioExterno = 0;
            if($precio1){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio1->precio_unitario_online * $precio1->multiplicador;
            }
            if($precio2){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio2->precio_unitario_online * $precio2->multiplicador;
            }
            if($precio3){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio3->precio_unitario_online * $precio3->multiplicador;
            }

            if($precio1 || $precio2 || $precio3)
            {
                $totalProductosInternos += $detallePedido->cantidad * $detallePedido->precio_unitario;
            }

            if($totalNumeroPrecios > 0 && $totalPrecioExterno>0){
                if($detallePedido){
                    $subTotal = $detallePedido->cantidad * $totalPrecioExterno/$totalNumeroPrecios;
                }
            }

            $totalProductosExternos += $subTotal;
        }
        if($totalProductosInternos > 0 && $totalProductosExternos>0){
            return ($totalProductosInternos - $totalProductosExternos);
        }
        return 0;
    }

    public function guardarPedido($new_cliente, $cliente, $costoDelivery)
    {
        $user = $this->getAuthenticated();
        $pedido = WiqliPedido::create([
            'total' => $costoDelivery,
            'usuario_id' => $user ? $user->id : '',
            'cliente_id' => $new_cliente->id,
            'costo_delivery' => $costoDelivery,
            'fecha_entrega' => substr($cliente['fecha_recojo'], 6, 4) . '-' . substr($cliente['fecha_recojo'], 3, 2) . '-' . substr($cliente['fecha_recojo'], 0, 2),
            'observacion' => isset($cliente['observacion']) ? $cliente['observacion'] : '',
            'tipoPago' => $cliente['tipoPago'],
            'pagado' => $cliente['tipoPago'] == 2 ? 1 : 0
        ]);
        return $pedido;
    }

    public function guardarCliente($cliente, $costoDelivery)
    {
        $user = $this->getAuthenticated();
        $new_cliente = WiqliCliente::create([
            'nombres' => $cliente['nombres'],
            'apellidos' => $cliente['apellidos'],
            'direccion' => $cliente['direccion'],
            'referencia' => $cliente['referencia'],
            'distritoId' => $cliente['distrito'],
            'provincia' => 'Lima',
            'departamento' => 'Lima',
            'telefono' => $cliente['telefono'],
            'correo' => $cliente['correo']
        ]);

        $usuario = User::find($user->id);
        $usuario->update([
            'name' => $cliente['nombres'],
            'father_lastname' => $cliente['apellidos'],
            'address' => $cliente['direccion'],
            'referencia' => $cliente['referencia'],
            'phone' => $cliente['telefono'],
            'distritoId' => $cliente['distrito']
        ]);

        return $new_cliente;
    }

    public function usarBilletera($total, $pedidoId)
    {
        $user = $this->getAuthenticated();
        $billetera = WiqliBilletera::where('usuarioId', $user->id)->first();
        $consumoBilletera = $billetera->saldo;
        if($billetera->saldo > $total)
        {
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $user->id,
                'pedidoId' => $pedidoId,
                'monto' => -($billetera->saldo - $total)
            ]);
            $billetera->update([
                'saldo' => $billetera->saldo - $total
            ]);
            $consumoBilletera = $total;
        }else{
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $user->id,
                'pedidoId' => $pedidoId,
                'monto' => -($billetera->saldo)
            ]);
            $billetera->update([
                'saldo' => 0.00
            ]);
        }
        
        return $consumoBilletera;
    }

    public function exportExcel(Request $request)
    {
        return Excel::download(new PedidosExport, 'reporte.xlsx');
    }

    public function exportExcelPorCliente($id)
    {
        return Excel::download(new PedidosExport, 'reporte.xlsx');
    }

    public function verPdf($id)
    {
        //return Excel::download(new PedidoExport, 'reporte.xlsx');
        $pedido = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])->where('id', $id)->get();
        $clientesReferidos = ClienteReferidoCupon::where('correo_referente', $pedido[0]->cliente->correo)
                        ->where('pedido_referente_cobrador_id', $id)
                        ->get();
        $cupon = null;
        if($pedido[0]['cupon_id'])
        {
            $cupon = CuponDescuento::find($pedido[0]['cupon_id']);
        }

        $productosPedido = WiqliDetallePedido::with('producto')
                    ->where('producto_id', '!=', 999)
                    ->where('pedido_id', $id)
                    ->where('status', 1)
                    ->get();

        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }
        $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $id));


        $data = [
            'productos' => $pedido[0]['detalle'],
            'pedido' => $pedido[0],
            'referidos' => $clientesReferidos,
            'cupon' => $cupon,
            'ahorroExterno' => $totalAhorro
        ];
        $pdf = Pdf::loadView('exports.pedido', $data);
        return $pdf->download('pedido_'.$data['pedido']['cliente']['nombres'].'.pdf');
    }

    public function obtenerTotalVenta($fechaInicial, $fechaFinal)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->whereBetween('fecha_entrega', [$fechaInicial, $fechaFinal])
        ->get();
        $total = 0;
        foreach ($pedidos as $key => $pedido) {
            $total += $pedido->total;
        }
        return $total;
    }
}