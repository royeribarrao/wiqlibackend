<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Suscripcion;
use App\Models\TarjetaCliente;
use App\Models\User;
use App\Models\ProductoSuscripcion;
use Mail;
use App\Mail\Cliente\CreacionSuscripcion;
use App\Mail\Admin\NuevaSuscripcion;
use App\Http\Controllers\PayUController;
use Carbon\Carbon;

class ClienteSuscripcionController extends Controller
{
    protected $correoRenzo;
    protected $correoRicardo;
    protected $correoMapi;
    protected $correoRoyer;
    protected $payUService;
    protected $correosAdmin;

    public function __construct(PayUController $payU)
    {
        $this->correoRenzo = config('constants.correo_renzo');
        $this->correoRicardo = config('constants.correo_ricardo');
        $this->correoMapi = config('constants.correo_mapi');
        $this->correoRoyer = config('constants.correo_royer');
        $this->payUService = $payU;
        $this->correosAdmin = config('constants.correosAdmin');
    }

    public function getAuthenticated() 
    {
        return \Auth::user();
    }

    public function crearSuscripcion(Request $request)
    {
        $usuario = $this->getAuthenticated();
        try {
            $pagar = $this->payUService->crearToken($request->datosTarjeta, $usuario->id);
            if(!$pagar->code)
            {
                return response()->json([
                    'state'=> false,
                    'message' => $pagar->error
                ]);
            }
            
        } catch (\Throwable $th) {
            return response()->json([
                'state'=> false,
                'message' => "Hubo un error en la petición. Inténtelo más tarde."
            ]);
        }
        
        $suscripcion = Suscripcion::create([
            'usuarioId' => $usuario->id,
            'fechaSuscripcion' => date('d-m-Y'),
            'codigoSuscripcion' => \Str::random(7),
            'periodo' => $request->periodo,
            'diaEntrega' => $request->diaRecojo,
            'fechaCobro' => $this->establecerFechaCobro()
        ]);

        $tarjetaCliente = TarjetaCliente::create([
            'usuarioId' => $usuario->id,
            'suscripcionId' => $suscripcion->id,
            'numeroTarjeta' => str_replace(" ", "", $request->numeroTarjeta),
            'nombreTarjeta' => $request->nombreTarjeta,
            'dni' => $request->dni,
            'fechaVencimiento' => substr($request->fechaVencimiento, 0, 4).'/'.substr($request->fechaVencimiento, 5, 2),
            'cvv' => $request->cvv,
            'tipoBanco' => $request->tipoBanco,
            'tokenId' => $pagar->creditCardToken->creditCardTokenId
        ]);

        $totalProductos = $this->guardarProductosSuscripcion($request->productos, $suscripcion->id);
        $totalProductosExtra = $this->guardarProductosExtraSuscripcion($request->productosExtra, $suscripcion->id);
        
        $userUpdate = User::find($usuario->id)->update(['isSuscrito' => 1]);
        $suscripcion->update([
            'montoCobro' => $totalProductos + 10.00
        ]);

        Mail::to($usuario->email)->send(new CreacionSuscripcion($usuario, $suscripcion));

        Mail::to($this->correoRenzo)->send(new NuevaSuscripcion($usuario, $suscripcion));

        Mail::to($this->correoRicardo)->send(new NuevaSuscripcion($usuario, $suscripcion));

        Mail::to($this->correoMapi)->send(new NuevaSuscripcion($usuario, $suscripcion));

        Mail::to($this->correoRoyer)->send(new NuevaSuscripcion($usuario, $suscripcion));

        return response()->json([
            'state' => true,
            'message' => "Suscripción creada correctamente.",
            'codigo' => $suscripcion->codigoSuscripcion
        ]);
    }

    public function guardarProductosSuscripcion($productos = [], $suscripcionId)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            ProductoSuscripcion::create([
                'suscripcionId' => $suscripcionId,
                'productoId' => $producto['id'],
                'cantidad' => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] : $producto['cantidad'],
                'cantidad_minima' => $producto['cantidad_minima'],
                'precio_unitario' => $producto['precio_unitario'],
                'unidad_medida' => $producto['unidad']['nombre'],
                'total'  => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario']
            ]);
            $total += $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario'];
        }
        return $total;
    }

    public function guardarProductosExtraSuscripcion($productos = [], $suscripcionId)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            ProductoSuscripcion::create([
                'suscripcionId' => $suscripcionId,
                'productoId' => 999,
                'categoriaId' => $producto['categoriaId'],
                'cantidad_desc' => $producto['cantidad_desc'],
                'nombre_desc' => $producto['nombre_desc'],
            ]);
        }
        return $total;
    }

    public function establecerFechaCobro()
    {
        $nroDia = date('N');
        //$proximo_lunes = time() + ( (7-($nroDia-1)) * 24 * 60 * 60 );
        //$proximo_martes = time() + ( (7-($nroDia-2)) * 24 * 60 * 60 );
        //$siguienteSemana = date("d-m-Y",strtotime($proximo_lunes_fecha."+ 1 week"));
        //$proximo_lunes_fecha = date('d-m-Y', $proximo_lunes);
        //$proximo_martes_fecha = date('d-m-Y', $proximo_martes);

        $proximo_fecha_recojo = time() + ( (7-($nroDia-1)) * 24 * 60 * 60 );
        $proximo_fecha = date('d-m-Y', $proximo_fecha_recojo);

        return $proximo_fecha;
    }

    public function editarProductosSuscripcion(Request $request)
    {
        $usuario = $this->getAuthenticated();
        $productos = $request->productos;
        $productosExtra = $request->productosExtra;
        
        
        $productosDelete = ProductoSuscripcion::where('suscripcionId', $request->suscripcionId)->delete();
        $totalProductos = 0;
        foreach ($productos as $key => $producto) {
            ProductoSuscripcion::create([
                'suscripcionId' => $request->suscripcionId,
                'productoId' => $producto['productoId'],
                'cantidad' => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] : $producto['cantidad'],
                'cantidad_minima' => $producto['cantidad_minima'],
                'precio_unitario' => $producto['precio_unitario'],
                'unidad_medida' => isset($producto['unidad']) ? $producto['unidad']['nombre'] : $producto['unidad_medida'],
                'total'  => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario']
            ]);
            $totalProductos += $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario'];
        }

        foreach ($productosExtra as $key => $producto) {
            ProductoSuscripcion::create([
                'suscripcionId' => $request->suscripcionId,
                'productoId' => 999,
                'categoriaId' => $producto['categoriaId'],
                'cantidad_desc' => $producto['cantidad_desc'],
                'nombre_desc' => $producto['nombre_desc']
            ]);
        }

        $suscripcion = Suscripcion::find($request->suscripcionId)->update(['montoCobro' => $totalProductos + 10]);

        return response()->json([
            'state' => true,
            'message' => "Lista de productos actualizados correctamente."
        ]);
    }

    public function editarPeriodoSuscripcion(Request $request)
    {
        $suscripcion = Suscripcion::find($request->suscripcionId);
        $suscripcion->update([
            'periodo' => $request->periodo ? $request->periodo : $suscripcion->periodo,
            'diaEntrega' => $request->diaEntrega ? $request->diaEntrega : $suscripcion->diaEntrega
        ]);

        return response()->json([
            'state' => true,
            'message' => "Periodo actualizado correctamente."
        ]);
    }

    public function editarDatosTarjetaSuscripcion(Request $request)
    {
        $usuario = $this->getAuthenticated();
        try {
            $pagar = $this->payUService->crearToken($request->datosTarjeta, $usuario->id);
            if(!$pagar->code)
            {
                return response()->json([
                    'state'=> false,
                    'message' => $pagar->error
                ]);
            }
            
        } catch (\Throwable $th) {
            return response()->json([
                'state'=> false,
                'message' => "Hubo un error en la petición. Inténtelo más tarde."
            ]);
        }

        $tarjetaToDelete = TarjetaCliente::where('usuarioId', $usuario->id)->first();
        $eliminarTarjetaPayu = $this->payUService->eliminarTarjeta($usuario->id, $tarjetaToDelete->tokenId);
        $tarjetaToDelete->delete();
        

        $tarjetaCliente = TarjetaCliente::create([
            'usuarioId' => $usuario->id,
            'suscripcionId' => $request->suscripcionId,
            'numeroTarjeta' => str_replace(" ", "", $request->numeroTarjeta),
            'nombreTarjeta' => $request->nombreTarjeta,
            'dni' => $usuario->dni,
            'fechaVencimiento' => substr($request->fechaVencimiento, 0, 4).'/'.substr($request->fechaVencimiento, 5, 2),
            'cvv' => $request->cvv,
            'tipoBanco' => $request->metodoPago,
            'tokenId' => $pagar->creditCardToken->creditCardTokenId
        ]);

        return response()->json([
            'state'=> true,
            'message' => "Datos de tarjeta actualizada correctamente."
        ]);
    }

    public function cancelarSuscripcion()
    {
        $usuario = $this->getAuthenticated();

        $tarjetaToDelete = TarjetaCliente::where('usuarioId', $usuario->id)->first();
        $eliminarTarjetaPayu = $this->payUService->eliminarTarjeta($usuario->id, $tarjetaToDelete->tokenId);
        $tarjetaToDelete->delete();

        $suscripcion = Suscripcion::where('usuarioId', $usuario->id)->delete();
        $user = User::find($usuario->id)->update(['isSuscrito' => false]);

        return response()->json([
            'state'=> true,
            'message' => "Su suscripción ha sido cancelada."
        ]);
    }

    public function verCorreo()
    {
        $usuario = User::find(16);
        $suscripcion = Suscripcion::find(5);
        Mail::to("royer@repo.com.pe")->send(new CreacionSuscripcion($usuario, $suscripcion));

        return response()->json([
            'state' => true,
            'message' => "Correo enviado."
        ]);
    }

    public function verCorreoSuscripcionAdmin()
    {
        $usuario = User::find(16);
        $suscripcion = Suscripcion::find(5);
        Mail::to("royer@repo.com.pe")->send(new NuevaSuscripcion($usuario, $suscripcion));
        return response()->json([
            'state' => true,
            'message' => "Correo enviado."
        ]);
    }

    public function verTarjetas()
    {
        $tarjetas = $this->payUService->getListaTokensPayU();
        return $tarjetas;
    }
}