<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\ProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Configuracion;
use App\Models\Temporal;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\PedidoExport;
use App\Exports\PruebaExport;

use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\AvisoUsuario;
use Mail;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Mail\Wiqli\RecordatorioCompra;
use App\Mail\Wiqli\RecordatorioCompraNavidad;

class AdminNotificacionController extends Controller
{
    public function verCorreos()
    {
        $cliente = WiqliCliente::distinct()->get();
        $clientes = $cliente->unique('correo');
        return $clientes;
    }

    public function recordarCompra()
    {
        $clientesTodos = $this->verCorreos();
        foreach ($clientesTodos as $key => $cliente) {
            try {
                Mail::to($cliente->correo)->send(new RecordatorioCompra($cliente));
                Temporal::create([
                    'id' => $cliente->id
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
            
        }
        return response()->json([
            'status' => true,
            'message' => "Mensaje enviado"
        ]);
    }

    public function recordarCompraNavidad()
    {
        $clientesTodos = $this->verCorreos();
        // $cliente = WiqliCliente::find(21);
        // Mail::to($cliente->correo)->send(new RecordatorioCompraNavidad($cliente));
        // return "trabajo echo";
        foreach ($clientesTodos as $key => $cliente) {
            try {
                Mail::to($cliente->correo)->send(new RecordatorioCompraNavidad($cliente));
                // Temporal::create([
                //     'id' => $cliente->id
                // ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
            
        }
        return response()->json([
            'status' => true,
            'clientes' => $clientesTodos,
            'message' => "Mensaje enviado"
        ]);
    }

    public function clientesNoCompraSemanaAtras()
    {
        $cliente = WiqliCliente::distinct()->get();
        $clientes = $cliente->unique('correo');
        $clientes2 = [];
        foreach ($clientes as $key => $item) {
            array_push($clientes2, $item);
        }

        $clientesCompraUltimaSemana = WiqliCliente::select(
                                'clientes.id',
                                'clientes.correo'
                            )
                                ->leftJoin('pedidos', 'pedidos.cliente_id', 'clientes.id')
                                ->where('pedidos.fecha_entrega', '>', '2022-11-18')
                                ->get();
        $clientesCompraUltimaSemana2 = [];
        foreach ($clientesCompraUltimaSemana as $key => $item) {
            array_push($clientesCompraUltimaSemana2, $item['correo']);
        }
        
        $clientesAptos = array_filter($clientes2, function ($item) use ($clientesCompraUltimaSemana2){
            if(!(in_array($item['correo'], $clientesCompraUltimaSemana2)))
            {
                return true;
            }
            
        });
        return $clientesAptos;
        
    }
}