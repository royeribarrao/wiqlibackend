<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\Wiqli\WiqliPagoPedido;
use App\Models\ProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Configuracion;
use App\Models\User;
use App\Models\Wiqli\WiqliBilletera;
use App\Models\Wiqli\WiqliMovimientoBilletera;

use App\Exports\ExcelPedidosExport;
use App\Mail\Cliente\BoletaPedido;

use DateTime;
use Mail;

class AdminPedidoController extends Controller
{
    public function all(Request $request)
    {
        $fechas = [];
        if(isset($request->fecha)){
            $fechas[0] = substr($request->fecha[0], 1, 10);
            $fechas[1] = substr($request->fecha[1], 1, 10);
        }
        
        $pedidos = WiqliPedido::with(['cliente', 'detalle.producto.unidad', 'cupon'])
                    ->where(function ($q) use ($request){
                        if (isset($request->fechaInicial) && isset($request->fechaFinal)) {
                            $q->whereBetween('fecha_entrega', [$request->fechaInicial, $request->fechaFinal]);
                        }
                    })
                    ->where('status', 1)
                    ->orderBy('fecha_entrega', 'desc')
                    ->paginate(10);
        return $pedidos;
    }

    public function allWrongs(Request $request)
    {
        $fechas = [];
        if(isset($request->fecha)){
            $fechas[0] = substr($request->fecha[0], 1, 10);
            $fechas[1] = substr($request->fecha[1], 1, 10);
        }
        
        $pedidos = WiqliPedido::with(['cliente', 'detalle.producto.unidad'])
                    ->where(function ($q) use ($request){
                        if (isset($request->fechaInicial) && isset($request->fechaFinal)) {
                            $q->whereBetween('fecha_entrega', [$request->fechaInicial, $request->fechaFinal]);
                        }
                    })
                    ->where('status', 0)
                    ->paginate(15);
        return $pedidos;
    }

    public function showDetalle(Request $request, $id)
    {
        $pedido = WiqliDetallePedido::with('producto')->where('pedido_id', $id)->get();
        return $pedido;
    }

    public function updatePedido(Request $request, $id)
    {
        $pedidoDetalle = WiqliDetallePedido::find($id);
        $pedido = WiqliPedido::find($pedidoDetalle->pedido_id);
        $totalAntiguoDetalle = $pedidoDetalle->total;
       
        $pedidoDetalle->update([
            'cantidad' => $pedidoDetalle->producto_id != 999 ? $request['cantidad'] : $pedidoDetalle->cantidad,
            'cantidad_desc' => $pedidoDetalle->producto_id == 999 ? $request['cantidad'] : $pedidoDetalle->cantidad_desc,
            'nombre_desc' => $pedidoDetalle->producto_id == 999 ? $request['producto'] : $pedidoDetalle->nombre_desc,
            'precio_unitario' => $request['precio_unitario'],
            'producto_id' => $pedidoDetalle->producto_id,
            'total' => $request['cantidad'] && $request['precio_unitario'] ? $request['cantidad'] * $request['precio_unitario'] : 0.00
        ]);

        $detalles = WiqliDetallePedido::where('pedido_id', $pedido->id)->get();
        $total = 0;
        foreach ($detalles as $key => $detalle) {
            $total += $detalle->total;
        }

        if($pedido->usuario_id)
        {
            $billetera = WiqliBilletera::where('usuarioId', $pedido->usuario_id)->first();
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $pedido->usuario_id,
                'pedidoId' => $pedido->id,
                'monto' => -$pedidoDetalle->total
            ]);
            $billetera->update([
                'saldo' => $totalAntiguoDetalle - $pedidoDetalle->total + $billetera->saldo
            ]);
        }

        $pedido->update([
            'total' => $total + $pedido->costo_delivery - $pedido->descuento,
            'totalProductos' => $total
        ]);

        return response()->json([
            'state'=> true,
            'message' => 'Detalle de pedido actualizado correctamente.'
        ]);
    }

    public function cambiarEstado($id)
    {
        $pedido = WiqliPedido::find($id);
        
        if($pedido->status > 0){
            $pedido->update([
                'status' => 0
            ]);
        }elseif($pedido->status == 0){
            $pedido->update([
                'status' => 1
            ]);
        }
        
        return response()->json([
            'state'=> true,
            'message' => "Pedido actualizado"
        ]);
    }

    public function updateEstadoDetalle($id)
    {
        $detallePedido = WiqliDetallePedido::find($id);
    
        if($detallePedido->status){
            $detallePedido->update([
                'status' => 0
            ]);
        }elseif($detallePedido->status == 0){
            $detallePedido->update([
                'status' => 1
            ]);
        }
        
        $pedido = WiqliPedido::find($detallePedido->pedido_id);
        $detalles = WiqliDetallePedido::where('pedido_id', $pedido->id)->where('status', true)->get();
        $total = 0;
        foreach ($detalles as $key => $detalle) {
            $total += $detalle->total;
        }

        if($pedido->usuario_id)
        {
            $billetera = WiqliBilletera::where('usuarioId', $pedido->usuario_id)->first();
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $pedido->usuario_id,
                'pedidoId' => $pedido->id,
                'monto' => -$detallePedido->total
            ]);
            $billetera->update([
                'saldo' =>  $detallePedido->status ?
                            -$detallePedido->total + $billetera->saldo : 
                            $detallePedido->total + $billetera->saldo
            ]);
        }

        $pedido->update([
            'total' => $total + $pedido->costo_delivery - $pedido->descuento,
            'totalProductos' => $total
        ]);
        
        return response()->json([
            'state'=> true,
            'message' => "Pedido actualizado"
        ]);
    }

    public function deleteDetalle($id)
    {
        $detallePedido = WiqliDetallePedido::find($id);
        $pedido = WiqliPedido::find($detallePedido->pedido_id);

        if($pedido->usuario_id)
        {
            $billetera = WiqliBilletera::where('usuarioId', $pedido->usuario_id)->first();
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $pedido->usuario_id,
                'pedidoId' => $pedido->id,
                'monto' => $detallePedido->total
            ]);
            $billetera->update([
                'saldo' => $detallePedido->total + $billetera->saldo
            ]);
        }

        $detallePedido->delete();

        $detalles = WiqliDetallePedido::where('pedido_id', $pedido->id)->where('status', true)->get();
        $total = 0;
        foreach ($detalles as $key => $detalle) {
            $total += $detalle->total;
        }
        
        $pedido->update([
            'total' => $total + $pedido->costo_delivery - $pedido->descuento,
            'totalProductos' => $total
        ]);

        return response()->json([
            'state'=> true,
            'message' => "Detalle eliminado y pedido actualizado"
        ]);
    }

    public function agregarProductoDetalleAPedido(Request $request, $pedidoId, $productoId)
    {
        $pedido = WiqliPedido::find($pedidoId);
        if($request->producto){
            $detalle = WiqliDetallePedido::create([
                'pedido_id' => $pedidoId,
                'nombre_desc' => $request->nombre_desc,
                'cantidad_desc' => $request->cantidad_desc,
                'precio_unitario' => $request->precio_unitario
            ]);
        }else{
            $producto = WiqliProducto::with('unidad')->find($productoId);
            $detalle = WiqliDetallePedido::create([
                'pedido_id' => $pedidoId,
                'producto_id' => $productoId,
                'unidad_medida' => $producto->unidad->nombre,
                'cantidad_minima' => $producto->cantidad_minima,
                'cantidad' => $request->cantidad,
                'precio_unitario' => $request->precio_unitario,
                'total' => $request->precio_unitario * $request->cantidad
            ]);

            $pedido->update([
                'totalProductos' => $pedido->totalProductos + $request->precio_unitario * $request->cantidad,
                'total' => $pedido->totalProductos + $request->precio_unitario * $request->cantidad + $pedido->costo_delivery - $pedido->descuento
            ]);

            if($pedido->usuario_id)
            {
                $billetera = WiqliBilletera::where('usuarioId', $pedido->usuario_id)->first();
                WiqliMovimientoBilletera::create([
                    'billeteraId' => $billetera->id,
                    'usuarioId' => $pedido->usuario_id,
                    'pedidoId' => $pedido->id,
                    'monto' => -$detalle->total
                ]);
                $billetera->update([
                    'saldo' => -$detalle->total + $billetera->saldo
                ]);
            }

        }
        return response()->json([
            'state'=> true,
            'message' => "Detalle agregado al pedido correctamente."
        ]);
    }

    public function obtenerInformacionPedido($pedidoId)
    {
        $pedido = WiqliPedido::with('cliente')->find($pedidoId);
        return $pedido;
    }

    public function registrarPagoPedido($pedidoId, Request $request)
    {
        $pedido = WiqliPedido::find($pedidoId);

        $pagoPedido = WiqliPagoPedido::create([
            'pedidoId' => $pedidoId,
            'monto' => $request->monto
        ]);
        
        $pagoPedido->update([
            'fechaPago' => $request->fechaPago
        ]);

        $pedido->update([
            'montoPagado' => $pedido->montoPagado + $request->monto
        ]);

        if($pedido->total == $request->monto)
        {
            $pedido->update([
                'pagado' => 1
            ]);
        }
        
        return response()->json([
            'state'=> true,
            'message' => "Registro de pago realizado."
        ]);
    }

    public function pagarTotalPedido($pedidoId)
    {
        $pedido = WiqliPedido::find($pedidoId);

        $pagoPedido = WiqliPagoPedido::create([
            'pedidoId' => $pedidoId,
            'monto' => $pedido->total - $pedido->montoPagado
        ]);
        $pagoPedido->update([
            'fechaPago' => substr($pagoPedido->created_at, 0, 10)
        ]);

        $pedido->update([
            'pagado' => !$pedido->pagado,
            'montoPagado' => $pedido->total
        ]);
        
        return response()->json([
            'state'=> true,
            'message' => "Pedido pagado."
        ]);
    }

    public function cancelarPagoPedido($pedidoId)
    {
        $pedido = WiqliPedido::find($pedidoId);
        $pagosPedido = WiqliPagoPedido::where('pedidoId', $pedidoId)->get();
        foreach ($pagosPedido as $key => $pago) {
            WiqliPagoPedido::find($pago->id)->delete();
        }

        $pedido->update([
            'pagado' => 0,
            'montoPagado' => 0
        ]);

        return response()->json([
            'state'=> true,
            'message' => "Pagos del pedido cancelados."
        ]);
    }

    public function obtenerPagos($pedidoId)
    {
        $pagos = WiqliPagoPedido::where('pedidoId', $pedidoId)->paginate(10);
        return $pagos;
    }

    public function enviarBoletaCliente(Request $request)
    {
        $pedidos = $request->all();
        foreach ($pedidos as $key => $pedido) 
        {
            $pedidoWiqli = WiqliPedido::find($pedido['id']);
            if(!$pedidoWiqli->boletaEnviada)
            {
                $clientesReferidos = ClienteReferidoCupon::where('correo_referente', $pedido['cliente']['correo'])
                    ->where('pedido_referente_cobrador_id', $pedido['id'])
                    ->get();
                $idsProductosInternos = [];
                foreach ($pedido['detalle'] as $key => $producto) {
                    array_push($idsProductosInternos, $producto['producto_id']);
                }
                $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $pedido['id']));
                $data["pedido"] = $pedido;
                $data["totalAhorro"] = $totalAhorro;
                $data["referidos"] = $clientesReferidos;
                $pdf = PDF::loadView('cliente.envioBoleta', $data);

                Mail::send('cliente.envioBoleta', $data, function ($message) use ($pedido, $pdf) {
                    $message->to($pedido['cliente']['correo'], $pedido['cliente']['correo'])
                        ->subject('Boleta de pedido de ' . $pedido['cliente']['nombres'])
                        ->attachData($pdf->output(), "boleta.pdf");
                });

                $pedidoWiqli->update([
                    'boletaEnviada' => 1
                ]);
            }
        }

        return response()->json([
            'state' => true,
            'message' => "Boletas enviadas correctamente."
        ]);
    }

    public function calcularTotalDescuento($idsProductosInternos, $pedidoId)
    {   
        $totalProductosInternos = 0;
        $totalProductosExternos = 0;
        foreach ($idsProductosInternos as $key => $id) {
            $detallePedido = WiqliDetallePedido::where('pedido_id', $pedidoId)->where('producto_id', $id)->first();
            $subTotal = 0;
            $precio1 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 1)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio2 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 2)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio3 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 3)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $totalNumeroPrecios = 0;
            $totalPrecioExterno = 0;
            if($precio1){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio1->precio_unitario_online * $precio1->multiplicador;
            }
            if($precio2){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio2->precio_unitario_online * $precio2->multiplicador;
            }
            if($precio3){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio3->precio_unitario_online * $precio3->multiplicador;
            }

            if($precio1 || $precio2 || $precio3)
            {
                $totalProductosInternos += $detallePedido->cantidad * $detallePedido->precio_unitario;
            }

            if($totalNumeroPrecios > 0 && $totalPrecioExterno>0){
                if($detallePedido){
                    $subTotal = $detallePedido->cantidad * $totalPrecioExterno/$totalNumeroPrecios;
                }
            }

            $totalProductosExternos += $subTotal;
        }
        if($totalProductosInternos > 0 && $totalProductosExternos>0){
            return ($totalProductosInternos - $totalProductosExternos);
        }
        return 0;
    }

    public function exportarExcelSeleccionados(Request $request)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
                        $query->where('producto_id', '!=', 999);
                    }])
                    ->where('status', 1)
                    ->whereIn('id', $request->pedidos)
                    ->get();
        $productosAptos = $this->obtenerProductosAptos($request->pedidos);
        $productosAdicionales = $this->obtenerProductosAdicionales($request->pedidos);
        
        $totalPedido = $this->obtenerTotalPedido($request->pedidos);
        $total = $this->obtenerTotalVenta($request->pedidos);
        
        return (new ExcelPedidosExport)
                ->pedidos($pedidos)
                ->productosAptos($productosAptos)
                ->productosAdicionales($productosAdicionales)
                ->totalPedido($totalPedido)
                ->total($total)
                ->download('Report pedido seleccionados: ' . '.xlsx');
    }

    public function obtenerProductosAptos($idPedidos)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->whereIn('id', $idPedidos)
        ->get();

        $productos = WiqliProducto::with('unidad')->orderBy('nombre', 'asc')->get()->toArray();
        
        //totales de los productos pedidos
        $totalesCantidad = [];
        $prods = DB::table('detalles_pedido')
                    ->where('detalles_pedido.producto_id', '!=', 999)
                    ->where('pedidos.status', 1)
                    ->whereIn('pedidos.id', $idPedidos)
                    ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                    ->get()
                    ->groupBy('producto_id')
                    ->toArray();
        
        foreach ($prods as $key => $producto) {
            $subtotal = 0;
            foreach ($producto as $key1 => $value) {
                $subtotal += $value->cantidad;
            }
            $dato = ['id' => null, 'total' => null];
            $dato['id'] = $key;
            $dato['total'] = $subtotal;
            array_push($totalesCantidad, $dato);
        }
        //calculando los productos aptos con sus cantidades totales
        $detalle_pedidos = DB::table('detalles_pedido')
                ->select('producto_id', 'productos.nombre')
                ->where('detalles_pedido.producto_id', '!=', 999)
                ->where('pedidos.status', 1)
                ->orderBy('productos.nombre', 'asc')
                ->whereIn('pedidos.id', $idPedidos)
                ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                ->leftJoin('productos', 'detalles_pedido.producto_id', 'productos.id')
                ->get()
                ->toArray();
        $detalle = [];
        
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido->producto_id);
        }
        
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        
        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }
        foreach ($new_prods as $key => $value) {
            foreach ($totalesCantidad as $key1 => $value1) {
                if($value['id'] == $value1['id'])
                {
                    $new_prods[$key]['cantidad'] = $value1['total'];
                    $new_prods[$key]['total'] = $value['precio_unitario'] * $value1['total'];
                }
            }
        }
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($pedidos as $key2 => $pedido) {
                $new_prods[$key1]['pedidos'][$key2]['id'] = $pedido['id'];
            }
        }
        $detallePedidos = WiqliDetallePedido::where('producto_id', '!=', 999)->get();
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($detallePedidos as $key2 => $detalle) {
                foreach($producto['pedidos'] as $key3 => $pedido){
                    if($producto['id'] == $detalle['producto_id'] && $pedido['id'] == $detalle['pedido_id'])
                    {
                        $new_prods[$key1]['pedidos'][$key3]['cantidad'] = $detalle['cantidad'];
                        $new_prods[$key1]['pedidos'][$key3]['precio'] = $detalle['precio_unitario'];
                        $new_prods[$key1]['pedidos'][$key3]['total'] = $detalle['total'];
                    }
                }
            }
        }
        return $new_prods;
    }

    public function obtenerProductosAdicionales($idPedidos)
    {
        $productos = WiqliDetallePedido::select(
                            'detalles_pedido.id',
                            'detalles_pedido.nombre_desc',
                            'detalles_pedido.cantidad_desc',
                            'detalles_pedido.total',
                            'detalles_pedido.precio_unitario',
                            'clientes.nombres',
                            'clientes.apellidos'
                        )
                        ->where('pedidos.status', 1)
                        ->where('detalles_pedido.producto_id', '=', 999)
                        ->where('detalles_pedido.status', 1)
                        ->whereIn('pedidos.id', $idPedidos)
                        ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                        ->join('clientes', 'pedidos.cliente_id', 'clientes.id')
                        ->get();
        return $productos;
    }

    public function obtenerTotalPedido($idPedidos)
    {
        $totales_cantidad = [];
        $prods = DB::table('detalles_pedido')
                ->where('detalles_pedido.producto_id', '!=', 999)
                ->where('pedidos.status', 1)
                ->whereIn('pedidos.id', $idPedidos)
                ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                ->get()
                ->groupBy('producto_id')
                ->toArray();
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value->cantidad;
            }
            array_push($totales_cantidad, $subtotal);
        }
        return $totales_cantidad;
    }

    public function obtenerTotalVenta($idPedidos)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->whereIn('id', $idPedidos)
        ->get();
        $total = 0;
        foreach ($pedidos as $key => $pedido) {
            $total += $pedido->total;
        }
        return $total;
    }
}