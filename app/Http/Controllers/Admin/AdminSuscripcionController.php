<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\PayUController;

class AdminSuscripcionController extends Controller
{
    protected $payUService;

    public function __construct(PayUController $payU)
    {
        $this->payUService = $payU;
    }

    public function comparacionPrecios(Request $request)
    {

    }

    public function verListaTokensPayU()
    {
        $tarjetas = $this->payUService->getListaTokensPayU();
        return $tarjetas;
    }
}