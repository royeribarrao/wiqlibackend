<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\ProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Configuracion;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\PedidoExport;
use App\Exports\PruebaExport;

use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\GenerarCuponReferente;
use Mail;
use Illuminate\Support\Facades\DB;
use DateTime;

class CorreoReferenteReferidoController extends Controller
{
    public function generarCodigo()
    {
        $codigo = \Str::random(7);
        return $codigo;
    }

    public function obtenerTodosCorreosUnicos()
    {
        $cliente = WiqliCliente::distinct()->get();
        $clientes = $cliente->unique('correo');
        return $clientes;
    }

    public function enviarCorreos()
    {
        $clientes = $this->obtenerTodosCorreosUnicos();
        $codigo = $this->generarCodigo();
        $configuracion = Configuracion::first();
        
        foreach ($clientes as $key => $cliente) {
            try {
                $cuponesCliente = CuponDescuento::where('correo_creador', $cliente->correo)
                                ->where('proveedor', 1)
                                ->where('activo', 1)
                                ->get();
                foreach ($cuponesCliente as $key => $cuponCliente) {
                    $cuponCliente->update([
                        'activo' => false
                    ]);
                }
                CuponDescuento::create([
                    'codigo' => $codigo,
                    'monto' => $configuracion->monto_descuento,
                    'correo_creador' => $cliente->correo,
                    'tipo' => $configuracion->tipo_descuento,
                    'proveedor' => 1,
                    'activo' => 1
                ]);
                Mail::to($cliente->correo)->send(new GenerarCuponReferente($cliente, $codigo));
            } catch (\Throwable $th) {
                return response()->json([
                    'state' => true,
                    'message' => 'Ocurrió un problema en el servidor.'
                ]);
            }
        }

        return response()->json([
            'state' => true,
            'message' => 'Cupones enviados correctamente a los usuarios de Wiqli.'
        ]);
    }

    public function enviarCorreoUnico($correo)
    {
        $clientes = $this->obtenerTodosCorreosUnicos();
        
        $configuracion = Configuracion::first();
        
        foreach ($clientes as $key => $cliente) {
            $codigo = $this->generarCodigo();
            try {
                $cuponesCliente = CuponDescuento::where('correo_creador', $cliente->correo)
                                ->where('proveedor', 1)
                                ->where('activo', 1)
                                ->get();
                foreach ($cuponesCliente as $key => $cuponCliente) {
                    $cuponCliente->update([
                        'activo' => false
                    ]);
                }
                CuponDescuento::create([
                    'codigo' => $codigo,
                    'monto' => $configuracion->monto_descuento,
                    'correo_creador' => $cliente->correo,
                    'tipo' => $configuracion->tipo_descuento,
                    'proveedor' => 1,
                    'activo' => 1
                ]);
                Mail::to($cliente->correo)->send(new GenerarCuponReferente($cliente, $codigo));
            } catch (\Throwable $th) {
                
            }
            
        }

        return response()->json([
            'state' => true,
            'message' => 'Cupones enviados correctamente a los usuarios de Wiqli.'
        ]);
    }

    public function enviarCuponDescuentoUnico()
    {
        $configuracion = Configuracion::first();
        $codigo = $this->generarCodigo();
        $cliente = WiqliCliente::find(51);
        $cuponesCliente = CuponDescuento::where('correo_creador', 'renzo@repo.com.pe')
                        ->where('proveedor', 1)
                        ->where('activo', 1)
                        ->get();
        foreach ($cuponesCliente as $key => $cuponCliente) {
            $cuponCliente->update([
                'activo' => false
            ]);
        }
        CuponDescuento::create([
            'codigo' => $codigo,
            'monto' => $configuracion->monto_descuento,
            'correo_creador' => 'renzo@repo.com.pe',
            'tipo' => $configuracion->tipo_descuento,
            'proveedor' => 1,
            'activo' => 1
        ]);
        Mail::to('renzo@repo.com.pe')->send(new GenerarCuponReferente($cliente, $codigo));
          
        return response()->json([
            'state' => true,
            'message' => 'Cupones enviados correctamente a los usuarios de Wiqli.'
        ]);
    }
}