<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\ProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Configuracion;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\PedidoExport;
use App\Exports\PruebaExport;

use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\AvisoUsuario;
use Mail;
use Illuminate\Support\Facades\DB;
use DateTime;

class CuponDescuentoController extends Controller
{
    public function all()
    {
        $cupones = CuponDescuento::orderBy('activo', 'desc')->paginate(10);
        return $cupones;
    }

    public function update($id)
    {
        $cupon = CuponDescuento::find($id);
        return $cupon;
    }

    public function updateStatus($id)
    {
        $cupon = CuponDescuento::find($id);
        $cupon->update([
            'activo' => $cupon->activo
        ]);
        return $cupon;
    }
}