<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\ProductoExterno;
use App\Models\PrecioProductoExterno;
use App\Models\CuponDescuento;
use App\Models\ClienteReferidoCupon;
use App\Models\Configuracion;
use App\Http\Controllers\Controller;
use App\Exports\PedidosExport;
use App\Exports\PedidoExport;
use App\Exports\PruebaExport;

use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade\Pdf;

use App\Mail\Wiqli\AvisoDashboard;
use App\Mail\Wiqli\AvisoUsuario;
use Mail;
use Illuminate\Support\Facades\DB;
use DateTime;

class AdminScrapController extends Controller
{
    public function comparacionPrecios(Request $request)
    {
        $parametros = $request->all();
        $precios = PrecioProductoExterno::select(
                'precios_productos_externos.precio_unitario_online as precio_externo',
                'precios_productos_externos.precio_unitario_wiqli as precio_wiqli',
                'precios_productos_externos.id as id',
                'precios_productos_externos.tienda_id as tienda',
                'precios_productos_externos.created_at as created_at',
                'precios_productos_externos.multiplicador',
                'productos_externos.id as producto_externo_id',
                'productos.nombre',
                'productos.id as productoId',
                'productos.precio_unitario',
                'productos.categoria_id',
            )
            ->where(function ($q) use ($parametros){
                if (isset($parametros['week']) && $parametros['week'] !== '') {
                    if(isset($parametros['week'][0]) && isset($parametros['week'][1])){
                        $q->whereBetween('precios_productos_externos.created_at', [$parametros['week'][1], $parametros['week'][0]]);
                    }
                }
            })
            ->leftJoin('productos_externos', 'productos_externos.id', 'precios_productos_externos.producto_externo_id')
            ->leftJoin('productos', 'productos.id', 'productos_externos.wiqli_producto_id')
            ->get();
        
        $precios2 = $precios->groupBy('categoria_id');
        $preciosFrutas = [];
        $preciosVerduras = [];
        $preciosCarnes = [];
        $preciosMenestras = [];

        $totalWiqliFrutas = 0;
        $totalOtrosFrutas = 0;

        $totalWiqliVerduras = 0;
        $totalOtrosVerduras = 0;

        $totalWiqliCarnes = 0;
        $totalOtrosCarnes = 0;

        $totalWiqliMenestras = 0;
        $totalOtrosMenestras = 0;

        foreach ($precios as $key => $value) {
            if($value->categoria_id == 1){
                $totalWiqliFrutas += $value->precio_wiqli;
                $totalOtrosFrutas += $value->multiplicador * $value->precio_externo;
                array_push($preciosFrutas, $value);
            }
            if($value->categoria_id == 2){
                $totalWiqliVerduras += $value->precio_wiqli;
                $totalOtrosVerduras += $value->multiplicador * $value->precio_externo;
                array_push($preciosVerduras, $value);
            }
            if($value->categoria_id == 3){
                $totalWiqliCarnes += $value->precio_wiqli;
                $totalOtrosCarnes += $value->multiplicador * $value->precio_externo;
                array_push($preciosCarnes, $value);
            }
            if($value->categoria_id == 4){
                $totalWiqliMenestras += $value->precio_wiqli;
                $totalOtrosMenestras += $value->multiplicador * $value->precio_externo;
                array_push($preciosMenestras, $value);
            }
        }

        return response()->json([
            'totalWiqliFrutas' => $totalWiqliFrutas,
            'totalWiqliVerduras' => $totalWiqliVerduras,
            'totalWiqliCarnes' => $totalWiqliCarnes,
            'totalWiqliMenestras' => $totalWiqliMenestras,
            'totalOtrosFrutas' => $totalOtrosFrutas,
            'totalOtrosVerduras' => $totalOtrosVerduras,
            'totalOtrosCarnes' => $totalOtrosCarnes,
            'totalOtrosMenestras' => $totalOtrosMenestras,
            'perFrutas' => $totalOtrosFrutas > 0 ? -($totalWiqliFrutas - $totalOtrosFrutas)/$totalOtrosFrutas*100 : 0,
            'perVerduras' => $totalOtrosVerduras > 0 ? -($totalWiqliVerduras - $totalOtrosVerduras)/$totalOtrosVerduras*100 : 0,
            'perCarnes' => $totalOtrosCarnes > 0 ? -($totalWiqliCarnes - $totalOtrosCarnes)/$totalOtrosCarnes*100 : 0,
            'perMenestras' => $totalOtrosMenestras > 0 ? -($totalWiqliMenestras - $totalOtrosMenestras)/$totalOtrosMenestras*100 : 0
        ]);
    }
}