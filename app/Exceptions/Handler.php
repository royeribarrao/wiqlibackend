<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Exception;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    // public function report(Exception $exception)
    // {
    //     parent::report($exception);
    // }

    // public function render($request, Exception $exception)
    // {

    //     // is this request asks for json?
    //     if( $request->header('Content-Type') == 'application/json'){
        
    //     // do we have exception triggered for this response?
    //         if ( !empty($exception) ) {

    //             // set default error message
    //             $response = [
    //                 'error' => 'Sorry, can not execute your request.'
    //             ];

    //             // If debug mode is enabled
    //             if (config('app.debug')) {
    //                 // Add the exception class name, message and stack trace to response
    //                 $response['exception'] = get_class($exception); // Reflection might be better here
    //                 $response['message'] = $exception->getMessage();
    //                 $response['trace'] = $exception->getTrace();
    //             }
    //             return response()->json($response);
                
    //         }

    //     }
    //     return parent::render($request, $exception);

    // }
}
