<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Configuracion extends Model
{
    protected $table = 'configuracion';
    protected $fillable = [
        'id',
        'monto_minimo_envio_codigo',
        'monto_minimo_compra_referido',
        'monto_descuento',
        'tipo_descuento'
    ];
}