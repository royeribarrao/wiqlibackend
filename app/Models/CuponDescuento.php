<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuponDescuento extends Model
{
    protected $table = 'cupones_descuento';
    protected $fillable = [
        'id',
        'codigo',
        'monto',
        'fecha_expiracion',
        'cantidad_expiracion',
        'correo_creador',
        'tipo',
        'proveedor',
        'publico',
        'activo'
    ];
}