<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductoSuscripcion extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'productos_suscripcion';
    protected $fillable = [
        'id',
        'suscripcionId',
        'productoId',
        'categoriaId',
        'nombre_desc',
        'cantidad',
        'cantidad_minima',
        'cantidad_desc',
        'precio_unitario',
        'unidad_medida',
        'total',
        'status'
    ];

    public function producto() {
        return $this->hasOne(\App\Models\Wiqli\WiqliProducto::class, 'id', 'productoId');
    }
}