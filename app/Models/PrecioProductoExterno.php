<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrecioProductoExterno extends Model
{
    protected $table = 'precios_productos_externos';
    protected $fillable = [
        'id',
        'tienda_id',
        'producto_externo_id',
        'multiplicador',
        'precio_unitario_online',
        'precio_unitario_wiqli',
        'precio_unitario_regular',
        'fecha_actualizacion'
    ];

    public function productoWiqli() {
        return $this->belongsTo(ProductoExterno::class, 'id', 'producto_externo_id');
    }
}