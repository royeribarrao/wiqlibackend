<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PrecioProducto extends Model
{
    protected $table = 'precios_producto';
    protected $fillable = [
        'id',
        'producto_id',
        'precio_unitario',
        'fecha_actualizacion'
    ];
}