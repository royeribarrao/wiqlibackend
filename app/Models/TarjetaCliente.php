<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TarjetaCliente extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'tarjetas_cliente';
    protected $fillable = [
        'id',
        'usuarioId',
        'suscripcionId',
        'numeroTarjeta',
        'nombreTarjeta',
        'fechaVencimiento',
        'cvv',
        'dni',
        'tipoBanco',
        'tokenId',
        'isActive'
    ];

    public function tarjeta() {
        return $this->belongsTo(TarjetaCliente::class, 'id', 'suscripcionId');
    }
}