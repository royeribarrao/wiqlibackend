<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClienteReferidoCupon extends Model
{
    protected $table = 'clientes_referido_cupon';
    protected $fillable = [
        'id',
        'pedido_referente_cobrador_id',
        'cupon_descuento_id',
        'correo_referido',
        'correo_referente',
        'monto',
        'activo'
    ];
}