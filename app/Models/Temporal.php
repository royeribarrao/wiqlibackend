<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Temporal extends Model
{
    protected $table = 'temporal';
    use HasFactory;
    protected $fillable = [
        'id',
        'name',
      ];
}
