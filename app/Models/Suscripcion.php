<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suscripcion extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'suscripciones';
    protected $fillable = [
        'id',
        'usuarioId',
        'montoCobro',
        'fechaCobro',
        'periodo',
        'diaEntrega',
        'fechaSuscripcion',
        'fechaCancelacion',
        'codigoSuscripcion',
        'isActive'
    ];

    public function tarjeta() {
        return $this->hasOne(TarjetaCliente::class, 'suscripcionId', 'id');
    }

    public function usuario() {
        return $this->hasOne(User::class, 'id', 'usuarioId');
    }

    public function productos() {
        return $this->hasMany(ProductoSuscripcion::class, 'suscripcionId', 'id');
    }
}