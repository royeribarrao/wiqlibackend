<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductoExterno extends Model
{
    protected $table = 'productos_externos';
    protected $fillable = [
        'id',
        'wiqli_producto_id',
        'nombre',
        'precio_vea',
        'url_vea',
        'multiplicador_vea',
        'url_wong',
        'precio_wong',
        'multiplicador_wong',
        'precio_tottus',
        'url_tottus',
        'multiplicador_tottus',
        'url_juntoz'
    ];

    public function productoWiqli() {
        return $this->hasOne(\App\Models\Wiqli\WiqliProducto::class, 'id', 'wiqli_producto_id');
    }
}