<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliPedido extends Model
{
    use HasFactory;
    protected $table = 'pedidos';
    protected $fillable = [
        'id',
        'usuario_id',
        'cliente_id',
        'cupon_id',
        'total',
        'totalProductos',
        'descuento',
        'costo_delivery',
        'fecha_pedido',
        'fecha_entrega',
        'fecha_pago',
        'observacion',
        'motivoDesactivo',
        'pagado',
        'montoPagado',
        'tipoPago',
        'boletaEnviada',
        'status'
    ];

    public function cliente() {
        return $this->hasOne(WiqliCliente::class, 'id', 'cliente_id');
    }

    public function detalle() {
        return $this->hasMany(WiqliDetallePedido::class, 'pedido_id', 'id');
    }

    public function cupon() {
        return $this->hasOne(\App\Models\CuponDescuento::class, 'id', 'cupon_id');
    }
}
