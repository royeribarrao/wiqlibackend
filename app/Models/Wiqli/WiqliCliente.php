<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliCliente extends Model
{
    use HasFactory;
    protected $table = 'clientes';
    protected $fillable = [
        'id',
        'nombres',
        'apellidos',
        'direccion',
        'referencia',
        'distritoId',
        'provincia',
        'departamento',
        'telefono',
        'correo',
        'latitud',
        'longitud'
    ];

    public function distrito() {
        return $this->hasOne(\App\Models\Distrito::class, 'id', 'distritoId');
    }
}
