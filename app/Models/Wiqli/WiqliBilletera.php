<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliBilletera extends Model
{
    use HasFactory;
    protected $table = 'billetera_usuario';
    protected $fillable = [
        'id',
        'usuarioId',
        'saldo',
        'saldoAFavor',
        'saldoEnContra'
    ];
}
