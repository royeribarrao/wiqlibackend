<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliMovimientoBilletera extends Model
{
    use HasFactory;
    protected $table = 'movimientos_billetera';
    protected $fillable = [
        'id',
        'billeteraId',
        'usuarioId',
        'pedidoId',
        'monto'
    ];
}
