<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliDetallePedido extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'detalles_pedido';
    protected $fillable = [
        'id',
        'pedido_id',
        'producto_id',
        'categoriaId',
        'nombre_desc',
        'cantidad_minima',
        'cantidad_desc',
        'cantidad',
        'precio_unitario',
        'unidad_medida',
        'total',
        'status'
    ];

    public function producto() {
        return $this->hasOne(WiqliProducto::class, 'id', 'producto_id');
    }
}
