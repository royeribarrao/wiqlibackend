<?php

namespace App\Models\Wiqli;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WiqliPagoPedido extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'pagos_pedido';
    protected $fillable = [
        'id',
        'pedidoId',
        'monto',
        'fechaPago'
    ];

    public function pedido() {
        return $this->hasOne(WiqliPedido::class, 'id', 'pedidoId');
    }
}
