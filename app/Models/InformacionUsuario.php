<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InformacionUsuario extends Model
{
    protected $table = 'informacion_usuario';
    protected $fillable = [
        'id',
        'nombres',
        'apellidoMaterno',
        'apellidoPaterno',
        'direccion',
        'referencia',
        'distrito',
        'provincia',
        'departamento',
        'telefono',
        'correo'
    ];
}