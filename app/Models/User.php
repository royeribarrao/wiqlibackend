<?php namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Traits\Fullname;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, Fullname;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'fullname',
        'father_lastname',
        'mother_lastname',
        'dni',
        'phone',
        'address',
        'latitud',
        'longitud',
        'referencia',
        'distritoId',
        'tarifaDistrito',
        'departamento',
        'state',
        'rol_id',
        'password',
        'confirmed',
        'confirmation_code',
        'email_verified_at',
        'isSuscrito',
        'updated_user',
        'created_user',
    ];

    protected $hidden = [
        'password',
    ];

    public function setPasswordAttribute($value){
        if ($value != ''){
            $this->attributes['password']= app('hash')->make($value);
        }
    }

    public function rol() {
        return $this->hasOne(Rol::class, 'id', 'rol_id');
    }

    public function informacion() {
        return $this->hasOne(InformacionUsuario::class, 'id', 'info_id');
    }

    public function billetera() {
        return $this->hasOne(\App\Models\Wiqli\WiqliBilletera::class, 'usuarioId', 'id');
    }

    public function distrito() {
        return $this->hasOne(Distrito::class, 'id', 'distritoId');
    }
}
