<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\PayUController;
use App\Http\Controllers\Cliente\ClienteSuscripcionController;
use App\Models\Suscripcion;
use App\Models\ClienteReferidoCupon;
use App\Models\ProductoExterno;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Models\Wiqli\WiqliBilletera;
use App\Models\Wiqli\WiqliMovimientoBilletera;
use App\Mail\Wiqli\AvisoDashboardSuscripcion;
use App\Mail\Wiqli\AvisoUsuarioSuscripcion;
use App\Mail\Example;
use Mail;

class CobrarSuscripcion extends Command
{
    protected $signature = 'command:cobrarSuscripcion';
    protected $description = 'Sirve para cobrar a las personas suscritas.';
    protected $payUService;
    protected $pedidoService;
    protected $correosAdmin;

    public function __construct(
        PayUController $payU,
        ClienteSuscripcionController $pedidoService
    )
    {
        parent::__construct();
        $this->payUService = $payU;
        $this->pedidoService = $pedidoService;
        $this->correosAdmin = config('constants.correosAdmin');
    }

    public function handle()
    {
        $today = date('d-m-Y');
        $suscripcionesPorCobrar = Suscripcion::with(['tarjeta', 'usuario', 'productos'])
            ->where('isActive', 1)
            ->where('fechaCobro', $today)
            ->get();
        $respuestas = [];
        foreach ($suscripcionesPorCobrar as $key => $suscripcion) {
            try {
                $cobroSuscripcion = $this->payUService->cobrarSuscripcion($suscripcion, $suscripcion->tarjeta);
                
                $productos = array_filter($suscripcion->productos->toArray(), function ($item) {
                    return $item['productoId'] != 999;
                });
                $productosExtra = array_filter($suscripcion->productos->toArray(), function ($item) {
                    return $item['productoId'] == 999;
                });
                $newPedido = $this->crearPedidoPorSuscripcion(
                    $suscripcion, $suscripcion->usuario, $productos, $productosExtra
                );

                $suscription = Suscripcion::find($suscripcion->id);
                $suscription->update([
                    'fechaCobro' => date("d-m-Y",strtotime(date($suscripcion->fechaCobro) . "+ " . ($suscripcion->periodo*7). " days"))
                ]);
                
                array_push($respuestas, gettype($newPedido));
            } catch (\Throwable $th) {
                //throw $th;
            }
            
        }
        echo json_encode($respuestas);
        //echo $today;
        //$tarjetas = $this->payUService->getListaTokensPayU();
    }

    public function crearPedidoPorSuscripcion($suscripcion, $cliente, $productos, $productosExtra)
    {
        $newCliente = $this->guardarCliente($cliente);
        $newPedido = $this->guardarPedido($suscripcion, $newCliente, $cliente);
        $totalProductos = $this->guardarDetallePedido($productos, $newPedido->id);
        $totalProductosExtra = $this->guardarProductosExtra($productosExtra, $newPedido->id);

        $newPedido->update([
            'total' => $newPedido->total + $totalProductos,
            'totalProductos' => $totalProductos
        ]);

        $totalReferidos = ClienteReferidoCupon::select(
            'clientes_referido_cupon.id',
            'clientes_referido_cupon.cupon_descuento_id',
            'clientes_referido_cupon.monto',
            'clientes_referido_cupon.correo_referido',
            'clientes_referido_cupon.activo',
            'cupones_descuento.correo_creador'
        )
                ->join('cupones_descuento', 'cupones_descuento.id', 'clientes_referido_cupon.cupon_descuento_id')
                ->where('cupones_descuento.correo_creador', $newCliente->correo)
                ->where('clientes_referido_cupon.activo', 1)
                ->get();

        $descuentoReferidos = 0;
        foreach ($totalReferidos as $key => $referido) {
            $descuentoReferidos += $referido->monto;
        }
        $newPedido->update([
            'total' => $newPedido->total - $descuentoReferidos,
            'descuento' => $descuentoReferidos
        ]);

        $referidoUsaDescuentoReferente = null;
        $cupon = null;
        $cuponRecurrencia = null;

        $productosPedido = WiqliDetallePedido::with('producto')
                            ->where('producto_id', '!=', 999)
                            ->where('pedido_id', $newPedido->id)
                            ->get();
        $productosAdicionales = WiqliDetallePedido::with('producto')
                            ->where('producto_id', '=', 999)
                            ->where('pedido_id', $newPedido->id)
                            ->get();

        $idsProductosInternos = [];
        foreach ($productosPedido as $key => $producto) {
            array_push($idsProductosInternos, $producto['producto_id']);
        }

        $totalAhorro = abs($this->calcularTotalDescuento($idsProductosInternos, $newPedido->id));
        
        // Mail::to("royer@repo.com.pe")->send(new AvisoUsuarioSuscripcion(
        //     $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
        //     $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
        // ));
        // Mail::to("royer@repo.com.pe")->send(new AvisoDashboardSuscripcion(
        //     $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
        //     $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
        // ));

        Mail::to($newCliente->correo)->send(new AvisoUsuarioSuscripcion(
            $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
            $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
        ));

        foreach ($this->correosAdmin as $key => $value) {
            Mail::to($value)->send(new AvisoDashboardSuscripcion(
                $newPedido, $newCliente, $productosPedido, $productosAdicionales, 
                $totalAhorro, $cupon, $totalReferidos, $referidoUsaDescuentoReferente, $cuponRecurrencia
            ));
        }

        foreach ($totalReferidos as $key => $referido) {
            $referidoUpdate = ClienteReferidoCupon::find($referido->id);
            $referidoUpdate->update([
                'activo' => false,
                'pedido_referente_cobrador_id' => $newPedido->id
            ]);
        }

        return response()->json([
            'state'=> 1,
            'message' => 'Pedido creado correctamente.'
        ]);
    }

    public function guardarCliente($cliente)
    {
        $new_cliente = WiqliCliente::create([
            'nombres' => $cliente->name,
            'apellidos' => $cliente->father_lastname,
            'direccion' => $cliente->address,
            'referencia' => $cliente->referencia,
            'provincia' => 'Lima',
            'departamento' => 'Lima',
            'telefono' => $cliente->phone,
            'correo' => $cliente->email
        ]);

        return $new_cliente;
    }

    public function guardarPedido($suscripcion, $new_cliente, $cliente)
    {
        $pedido = WiqliPedido::create([
            'total' => 10.00,
            'usuario_id' => $cliente->id,
            'cliente_id' => $new_cliente->id,
            'costo_delivery' => 10.00,
            'fecha_entrega' => $suscripcion->diaEntrega == 1 ? 
                                date("Y-m-d",strtotime(date($suscripcion->fechaEntrega)."+ 1 days")) : 
                                date("Y-m-d",strtotime(date($suscripcion->fechaEntrega)."+ 2 days")),
            'observacion' => '',
            'tipoPago' => 2,
            'pagado' => 1
        ]);
        return $pedido;
    }

    public function guardarDetallePedido($productos, $pedido_id)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedido_id,
                'producto_id' => $producto['productoId'],
                'cantidad' => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] : $producto['cantidad'],
                'cantidad_minima' => $producto['cantidad_minima'],
                'precio_unitario' => $producto['precio_unitario'],
                'unidad_medida' => $producto['unidad_medida'],
                'total'  => $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario']
            ]);
            $total += $producto['cantidad_minima'] != 1 ? $producto['cantidad_minima'] * $producto['cantidad'] * $producto['precio_unitario'] : $producto['cantidad'] * $producto['precio_unitario'];
        }
        return $total;
    }

    public function guardarProductosExtra($productos = [], $pedidoId)
    {
        $total = 0;
        foreach ($productos as $key => $producto) {
            WiqliDetallePedido::create([
                'pedido_id' => $pedidoId,
                'categoriaId' => $producto['categoriaId'],
                'producto_id' => 999,
                'nombre_desc' => $producto['nombre_desc'],
                'cantidad_desc' => $producto['cantidad_desc']
            ]);
        }
        return $total;
    }

    public function calcularTotalDescuento($idsProductosInternos, $pedidoId)
    {   
        $totalProductosInternos = 0;
        $totalProductosExternos = 0;
        foreach ($idsProductosInternos as $key => $id) {
            $detallePedido = WiqliDetallePedido::where('pedido_id', $pedidoId)->where('producto_id', $id)->first();
            $subTotal = 0;
            $precio1 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 1)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio2 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 2)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $precio3 = ProductoExterno::select(
                        'precio_unitario_online', 
                        'precios_productos_externos.created_at', 
                        'precios_productos_externos.id',
                        'productos_externos.wiqli_producto_id',
                        'precios_productos_externos.multiplicador'
                    )
                        ->where('productos_externos.wiqli_producto_id', $id)
                        ->where('precios_productos_externos.tienda_id', 3)
                        ->join('precios_productos_externos', 'precios_productos_externos.producto_externo_id', 'productos_externos.id')
                        ->latest()
                        ->first();
            $totalNumeroPrecios = 0;
            $totalPrecioExterno = 0;
            if($precio1){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio1->precio_unitario_online * $precio1->multiplicador;
            }
            if($precio2){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio2->precio_unitario_online * $precio2->multiplicador;
            }
            if($precio3){
                $totalNumeroPrecios += 1;
                $totalPrecioExterno += $precio3->precio_unitario_online * $precio3->multiplicador;
            }

            if($precio1 || $precio2 || $precio3)
            {
                $totalProductosInternos += $detallePedido->cantidad * $detallePedido->precio_unitario;
            }

            if($totalNumeroPrecios > 0 && $totalPrecioExterno>0){
                if($detallePedido){
                    $subTotal = $detallePedido->cantidad * $totalPrecioExterno/$totalNumeroPrecios;
                }
            }

            $totalProductosExternos += $subTotal;
        }
        if($totalProductosInternos > 0 && $totalProductosExternos>0){
            return ($totalProductosInternos - $totalProductosExternos);
        }
        return 0;
    }

    public function usarBilletera($total, $pedidoId)
    {
        $billetera = WiqliBilletera::where('usuarioId', $user->id)->first();
        $consumoBilletera = $billetera->saldo;
        if($billetera->saldo > $total)
        {
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $user->id,
                'pedidoId' => $pedidoId,
                'monto' => -($billetera->saldo - $total)
            ]);
            $billetera->update([
                'saldo' => $billetera->saldo - $total
            ]);
            $consumoBilletera = $total;
        }else{
            WiqliMovimientoBilletera::create([
                'billeteraId' => $billetera->id,
                'usuarioId' => $user->id,
                'pedidoId' => $pedidoId,
                'monto' => -($billetera->saldo)
            ]);
            $billetera->update([
                'saldo' => 0.00
            ]);
        }
        
        return $consumoBilletera;
    }

}
