<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use App\Models\PrecioProductoExterno;
use App\Models\ProductoExterno;
use Carbon\Carbon;

class ScrapPreciosTottus extends Command
{
    protected $signature = 'command:scrapPreciosTottus';

    protected $description = 'Scrap a tottus';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $data = Cache::get('dataScrapTottus');
        foreach ($data as $key => $producto) {
            try {
                $productowiqli = ProductoExterno::find($producto['id']);
                PrecioProductoExterno::create([
                    'tienda_id' => 2,
                    'producto_externo_id' => $producto['id'],
                    'precio_unitario_online' => $producto['precioOnline'],
                    'multiplicador' => $productowiqli->multiplicador,
                    'fecha_actualizacion' => Carbon::now()->toTimeString()
                ]);
            } catch (\Throwable $th) {
                //throw $th;
            }
            
        }
        return response()->json([
            'state'=> 1,
            'message' => 'Precios creados correctamente.'
        ]);
    }
}