<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use App\Models\PrecioProductoExterno;
use Carbon\Carbon;

class ScrapPreciosJuntoz extends Command
{
    protected $signature = 'command:scrapPreciosJuntoz';

    protected $description = 'Scrap a juntoz';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $data = Cache::get('dataScrapJuntoz');
        foreach ($data as $key => $producto) {
            PrecioProductoExterno::create([
                'producto_externo_id' => $producto['id'],
                'precio_unitario_online' => $producto['precioOnline'],
                'precio_unitario_regular' => $producto['precioRegular'],
                'fecha_actualizacion' => Carbon::now()->toTimeString()
            ]);
        }
        return response()->json([
            'state'=> 1,
            'message' => 'Precios creados correctamente.'
        ]);
    }
}