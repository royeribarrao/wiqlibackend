<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\PayUController;
use App\Http\Controllers\Cliente\ClienteSuscripcionController;
use App\Models\Suscripcion;
use App\Models\ClienteReferidoCupon;
use App\Models\ProductoExterno;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliDetallePedido;
use App\Mail\Wiqli\AvisoDashboardSuscripcion;
use App\Mail\Wiqli\AvisoUsuarioSuscripcion;
use App\Mail\Example;
use Mail;

class CobroSuscripcionManual extends Command
{
    protected $signature = 'command:cobroSuscripcionManual';
    protected $description = 'Sirve para cobrar a las personas suscritas de forma manual.';
    protected $payUService;

    public function __construct(
        PayUController $payU
    )
    {
        parent::__construct();
        $this->payUService = $payU;
    }

    public function handle()
    {
        $today = date('d-m-Y');
        $suscripcionesPorCobrar = Suscripcion::with(['tarjeta', 'usuario', 'productos'])
            ->where('isActive', 1)
            ->get();
        $respuestas = [];
        foreach ($suscripcionesPorCobrar as $key => $suscripcion) {
            try {
                $cobroSuscripcion = $this->payUService->cobrarSuscripcion($suscripcion, $suscripcion->tarjeta);
                
                array_push($respuestas, gettype($newPedido));
            } catch (\Throwable $th) {
                
            }
        }
        echo json_encode($respuestas);
    }
}
