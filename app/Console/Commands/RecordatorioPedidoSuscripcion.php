<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Suscripcion;
use App\Mail\Cliente\RecordatorioPedido;
use Mail;

class RecordatorioPedidoSuscripcion extends Command
{
    protected $signature = 'command:recordatorioPedidoSuscripcion';
    protected $description = 'Sirve para recordar a los usuarios que se creará un pedido a su nombre.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $today = date('d-m-Y');

        $suscripciones = Suscripcion::with(['usuario.distrito', 'productos'])
            ->where('isActive', 1)
            ->where('fechaCobro', $today)
            ->get();
        
        $respuestas = [];
        foreach ($suscripciones as $key => $suscripcion) {
            try {
                Mail::to($suscripcion->usuario->email)->send(new RecordatorioPedido(
                    $suscripcion, $suscripcion->usuario, $suscripcion->productos
                ));
                Mail::to('royer@repo.com.pe')->send(new RecordatorioPedido(
                    $suscripcion, $suscripcion->usuario, $suscripcion->productos
                ));

            } catch (\Throwable $th) {
                
            }
        }

        echo json_encode("pase todo");
    }
}
