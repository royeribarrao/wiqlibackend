<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProductoSuscripcion;
use App\Models\Wiqli\WiqliProducto;

class ActualizarPreciosSuscripcion extends Command
{
    protected $signature = 'command:actualizarPreciosSuscripcion';
    protected $description = 'Sirve para actualizar los precios de los productos de suscripcion.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $productos = ProductoSuscripcion::all();
        foreach ($productos as $key => $producto) {
            $wiqliProducto = WiqliProducto::find($producto->productoId);
            $producto->update([
                'precio_unitario' => $wiqliProducto->precio_unitario
            ]);
        }
    }
}
