<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\ScrapPreciosPlazaVea;
use App\Console\Commands\ScrapPreciosTottus;
use App\Console\Commands\ScrapPreciosWong;
use App\Console\Commands\CobrarSuscripcion;
use App\Console\Commands\Prueba;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        ScrapPreciosPlazaVea::class,
        ScrapPreciosTottus::class,
        ScrapPreciosWong::class,
        CobrarSuscripcion::class,
        Prueba::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('command:cobrarSuscripcion')
                  ->timezone('America/Lima')
                  ->weeklyOn(0, '21:00');
        $schedule->command('command:recordatorioPedidoSuscripcion')
                  ->timezone('America/Lima')
                  ->weeklyOn(6, '10:00');
        $schedule->command('command:actualizarPreciosSuscripcion')
                  ->timezone('America/Lima')
                  ->weeklyOn(0, '20:00');
    }
    
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
