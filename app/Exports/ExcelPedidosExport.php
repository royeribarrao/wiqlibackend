<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliDetallePedido;

// class PedidosExport implements FromCollection
// {
//     public function collection()
//     {
//         return WiqliPedido::all();
//     }
// }

class ExcelPedidosExport implements FromView
{
    use Exportable;

    public function pedidos($pedidos)
    {
        $this->pedidos = $pedidos;
        return $this;
    }

    public function productosAptos($productosAptos)
    {
        $this->productosAptos = $productosAptos;
        return $this;
    }

    public function productosAdicionales($productosAdicionales)
    {
        $this->productosAdicionales = $productosAdicionales;
        return $this;
    }

    public function totalPedido($totalPedido)
    {
        $this->totalPedido = $totalPedido;
        return $this;
    }

    public function total($total)
    {
        $this->total = $total;
        return $this;
    }

    public function view(): View
    {
        return view('exports.excelPedidos', [
            'productosAptos' => $this->productosAptos,
            'productosAdicionales' => $this->productosAdicionales,
            'pedidos' => $this->pedidos,
            'totalesCantidad' => $this->totalPedido,
            'totalVenta' => $this->total
        ]);
    }

    public function obtenerProductosAptos($totalesCantidad)
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])->get();

        

        $productos = WiqliProducto::with('unidad')->get()->toArray();
        
        //totales de los productos pedidos
        $totalesCantidad = [];
        $prods = WiqliDetallePedido::all()
            ->where('producto_id', '!=', 999)
            ->groupBy('producto_id');
        
        foreach ($prods as $key => $producto) {
            $subtotal = 0;
            foreach ($producto as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totalesCantidad, $subtotal);
        }
        //calculando los productos aptos con sus cantidades totales
        $detalle_pedidos = WiqliDetallePedido::select('producto_id')
            ->where('producto_id', '!=', 999)
            ->orderBy('producto_id')
            ->get()
            ->toArray();
        $detalle = [];
        
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido['producto_id']);
        }
        
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });

        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }

        foreach ($new_prods as $key => $value) {
            $new_prods[$key]['cantidad'] = $totalesCantidad[$key];
            $new_prods[$key]['total'] = $value['precio_unitario'] * $totalesCantidad[$key];
        }



        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($pedidos as $key2 => $pedido) {
                $new_prods[$key1]['pedidos'][$key2]['id'] = $pedido['id'];
            }
        }
        
        $detallePedidos = WiqliDetallePedido::where('producto_id', '!=', 999)->get();
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($detallePedidos as $key2 => $detalle) {
                foreach($producto['pedidos'] as $key3 => $pedido){
                    if($producto['id'] == $detalle['producto_id'] && $pedido['id'] == $detalle['pedido_id'])
                    {
                        $new_prods[$key1]['pedidos'][$key3]['cantidad'] = $detalle['cantidad'];
                        $new_prods[$key1]['pedidos'][$key3]['precio'] = $detalle['precio_unitario'];
                        $new_prods[$key1]['pedidos'][$key3]['total'] = $detalle['total'];
                    }
                }
            }
        }


        return $new_prods;
    }

    public function obtenerPedidos()
    {
        $pedidos = WiqliPedido::with([
            'cliente', 
            'detalle' => function ($q){
                $q->where('producto_id', '!=', 999);
        }])->get();
        return $pedidos;
    }

    public function obtenerTotalPedido()
    {
        $totales_cantidad = [];
        $prods = WiqliDetallePedido::all()
            ->where('producto_id', '!=', 999)
            ->groupBy('producto_id');
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value['cantidad'];
            }
            array_push($totales_cantidad, $subtotal);
        }
        return $totales_cantidad;
    }

    public function obtenerTotalPersonas($productos, $totalesCantidad, $pedidos)
    {
        foreach ($productos as $key => $value) {
            $productos[$key]['cantidad'] = $totalesCantidad[$key];
            $productos[$key]['total'] = $value['precio_unitario'] * $totalesCantidad[$key];
        }

        foreach ($productos as $key => $producto) {
            $id_productoApto= $key;
            foreach ($pedidos as $key => $cliente) {
                $id_pedido = $cliente['id'];
                foreach ($cliente['detalle'] as $key => $detalle) {
                    if($detalle['id'] == $producto['id'])
                    {
                        $productos[$id_productoApto]['personas'][$key]['cantidad'] = $detalle['cantidad'];
                        $productos[$id_productoApto]['personas'][$key]['precio'] = $detalle['precio_unitario'];
                    }
                }
            }
        }
    }
}