<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Wiqli\WiqliPedido;
use App\Models\Wiqli\WiqliProducto;
use App\Models\Wiqli\WiqliCliente;
use App\Models\Wiqli\WiqliDetallePedido;
use Illuminate\Support\Facades\DB;

class PedidosExport implements FromView
{
    public function view(): View
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->get();

        $productosAptos = $this->obtenerProductosAptos();
        $productosAdicionales = $this->obtenerProductosAdicionales();
        $totalPedido = $this->obtenerTotalPedido();
        $total = $this->obtenerTotalVenta();

        return view('exports.pedidos', [
            'pedidos' => $pedidos,
            'productosAptos' => $productosAptos,
            'productosAdicionales' => $productosAdicionales,
            'totalesCantidad' => $totalPedido,
            'totalVenta' => $total
        ]);
    }

    public function obtenerProductosAptos()
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->get();

        $productos = WiqliProducto::with('unidad')->orderBy('nombre', 'asc')->get()->toArray();
        
        //totales de los productos pedidos
        $totalesCantidad = [];
        $prods = DB::table('detalles_pedido')
                    ->where('detalles_pedido.producto_id', '!=', 999)
                    ->where('pedidos.status', 1)
                    ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                    ->get()
                    ->groupBy('producto_id')
                    ->toArray();
        
        foreach ($prods as $key => $producto) {
            $subtotal = 0;
            foreach ($producto as $key1 => $value) {
                $subtotal += $value->cantidad;
            }
            $dato = ['id' => null, 'total' => null];
            // $totalesCantidad[$key]['id'] = $key;
            // $totalesCantidad[$key]['total'] = $subtotal;
            $dato['id'] = $key;
            $dato['total'] = $subtotal;
            array_push($totalesCantidad, $dato);
        }
        //calculando los productos aptos con sus cantidades totales
        $detalle_pedidos = DB::table('detalles_pedido')
                ->select('producto_id', 'productos.nombre')
                ->where('detalles_pedido.producto_id', '!=', 999)
                ->where('pedidos.status', 1)
                ->orderBy('productos.nombre', 'asc')
                ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                ->leftJoin('productos', 'detalles_pedido.producto_id', 'productos.id')
                ->get()
                ->toArray();
        $detalle = [];
        
        foreach ($detalle_pedidos as $key => $pedido) {
            array_push($detalle, $pedido->producto_id);
        }
        
        $productosAptos = array_filter($productos, function ($item) use ($detalle){
            return in_array($item['id'], $detalle);
        });
        
        $new_prods = [];
        foreach ($productosAptos as $key => $producto) {
            array_push($new_prods, $producto);
        }
        foreach ($new_prods as $key => $value) {
            foreach ($totalesCantidad as $key1 => $value1) {
                if($value['id'] == $value1['id'])
                {
                    $new_prods[$key]['cantidad'] = $value1['total'];
                    $new_prods[$key]['total'] = $value['precio_unitario'] * $value1['total'];
                }
            }
        }
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($pedidos as $key2 => $pedido) {
                $new_prods[$key1]['pedidos'][$key2]['id'] = $pedido['id'];
            }
        }
        $detallePedidos = WiqliDetallePedido::where('producto_id', '!=', 999)->get();
        
        foreach ($new_prods as $key1 => $producto) {
            foreach ($detallePedidos as $key2 => $detalle) {
                foreach($producto['pedidos'] as $key3 => $pedido){
                    if($producto['id'] == $detalle['producto_id'] && $pedido['id'] == $detalle['pedido_id'])
                    {
                        $new_prods[$key1]['pedidos'][$key3]['cantidad'] = $detalle['cantidad'];
                        $new_prods[$key1]['pedidos'][$key3]['precio'] = $detalle['precio_unitario'];
                        $new_prods[$key1]['pedidos'][$key3]['total'] = $detalle['total'];
                    }
                }
            }
        }
        return $new_prods;
    }

    public function obtenerProductosAdicionales()
    {
        $productos = WiqliDetallePedido::select(
                            'detalles_pedido.id',
                            'detalles_pedido.nombre_desc',
                            'detalles_pedido.cantidad_desc',
                            'detalles_pedido.total',
                            'detalles_pedido.precio_unitario',
                            'clientes.nombres',
                            'clientes.apellidos'
                        )
                        ->where('pedidos.status', 1)
                        ->where('detalles_pedido.producto_id', '=', 999)
                        ->where('detalles_pedido.status', 1)
                        ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                        ->join('clientes', 'pedidos.cliente_id', 'clientes.id')
                        ->get();
        return $productos;
    }

    public function obtenerTotalPedido()
    {
        $totales_cantidad = [];
        $prods = DB::table('detalles_pedido')
                ->where('detalles_pedido.producto_id', '!=', 999)
                ->where('pedidos.status', 1)
                ->leftJoin('pedidos', 'pedidos.id', 'detalles_pedido.pedido_id')
                ->get()
                ->groupBy('producto_id')
                ->toArray();
        
        foreach ($prods as $key => $productos) {
            $subtotal = 0;
            foreach ($productos as $key => $value) {
                $subtotal += $value->cantidad;
            }
            array_push($totales_cantidad, $subtotal);
        }
        return $totales_cantidad;
    }

    public function obtenerTotalVenta()
    {
        $pedidos = WiqliPedido::with(['detalle' => function ($query){
            $query->where('producto_id', '!=', 999);
        }])
        ->where('status', 1)
        ->get();
        $total = 0;
        foreach ($pedidos as $key => $pedido) {
            $total += $pedido->total;
        }
        return $total;
    }
}