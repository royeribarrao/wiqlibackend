<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\Models\Wiqli\WiqliPedido;

class PedidoExport implements FromView
{
    public function view(): View
    {
        $pedido = WiqliPedido::with(['cliente', 'detalle.producto.unidad']);

        return view('exports.pedidoId', [
            'pedido' => $pedido
        ]);
    }
}
