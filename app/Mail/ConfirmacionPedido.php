<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Gestion;

class ConfirmacionPedido extends Mailable
{
    use Queueable, SerializesModels;

    public $info;
    public $productos;
    public $codigo;
    public $nuevosproductos;

    public function __construct($info, $productos, $codigo, $nuevosproductos)
    {
        $this->info = $info;
        $this->productos = $productos;
        $this->codigo = $codigo;
        $this->nuevosproductos = $nuevosproductos;
    }

    public function build()
    {
        return $this->subject('Solicitud N° '.$this->codigo.' confirmada')->view('mails.confirmacion_pedido');
    }
}