<?php

namespace App\Mail\Web;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $mensaje;
    public $nombre;

    public function __construct($email, $nombre, $mensaje)
    {
        $this->email = $email;
        $this->mensaje = $mensaje;
        $this->nombre = $nombre;
    }

    public function build()
    {
        return $this->subject('¡Contacto Web!')->view('mails.web.contacto');
    }
}
