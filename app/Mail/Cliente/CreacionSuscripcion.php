<?php

namespace App\Mail\Cliente;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreacionSuscripcion extends Mailable
{
    use Queueable, SerializesModels;

    public $cliente;
    public $suscripcion;

    public function __construct($cliente, $suscripcion)
    {
        $this->cliente = $cliente;
        $this->suscripcion = $suscripcion;
    }

    public function build()
    {
        return $this->subject('Bienvenid@ '.$this->cliente->name)->view('cliente.suscripcionCreada');
    }
}