<?php

namespace App\Mail\Cliente;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecordatorioPedido extends Mailable
{
    use Queueable, SerializesModels;

    public $suscripcion;
    public $cliente;
    public $productos;

    public function __construct(
        $suscripcion, $cliente, $productos
    )
    {
        $this->suscripcion = $suscripcion;
        $this->cliente = $cliente;
        $this->productos = $productos;
    }

    public function build()
    {
        return $this->subject('¡Tu pedido está cerca!')->view('cliente.recordatorioPedido');
    }
}