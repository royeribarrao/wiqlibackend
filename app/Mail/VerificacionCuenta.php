<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificacionCuenta extends Mailable
{
    use Queueable, SerializesModels;

    public $usuario;
    public $codigo;

    public function __construct($usuario, $codigo)
    {
        $this->usuario = $usuario;
        $this->codigo = $codigo;
    }

    public function build()
    {
        return $this->subject('¡Bienvenid@ a Wiqli!')->view('cuenta.verificacionCuenta');
    }
}
