<?php

namespace App\Mail\Wiqli;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AvisoDashboardSuscripcion extends Mailable
{
    use Queueable, SerializesModels;

    public $pedido;
    public $cliente;
    public $productos;
    public $productosAdicionales;
    public $totalAhorro;
    public $cupon;
    public $totalReferidos;
    public $referidoUsaDescuentoReferente;
    public $cuponRecurrencia;

    public function __construct(
        $pedido, $cliente, $productos, $productosAdicionales, $totalAhorro, $cupon, $totalReferidos,
        $referidoUsaDescuentoReferente, $cuponRecurrencia
    )
    {
        $this->pedido = $pedido;
        $this->cliente = $cliente;
        $this->productos = $productos;
        $this->productosAdicionales = $productosAdicionales;
        $this->totalAhorro = $totalAhorro;
        $this->cupon = $cupon;
        $this->totalReferidos = $totalReferidos;
        $this->referidoUsaDescuentoReferente = $referidoUsaDescuentoReferente;
        $this->cuponRecurrencia = $cuponRecurrencia;
    }

    public function build()
    {
        return $this->subject('¡Nuevo pedido en Wiqli!')->view('wiqli.suscripcionAdmin');
    }
}