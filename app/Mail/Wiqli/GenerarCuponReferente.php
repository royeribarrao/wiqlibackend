<?php

namespace App\Mail\Wiqli;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GenerarCuponReferente extends Mailable
{
    use Queueable, SerializesModels;

    public $cliente;
    public $codigo;

    public function __construct($cliente, $codigo)
    {
        $this->cliente = $cliente;
        $this->codigo = $codigo;
    }

    public function build()
    {
        return $this->subject('¡Comparte este cupón y obtén descuentos!')->view('wiqli.cuponesReferente');
    }
}