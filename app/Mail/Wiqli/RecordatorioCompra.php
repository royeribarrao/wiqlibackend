<?php

namespace App\Mail\Wiqli;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RecordatorioCompra extends Mailable
{
    use Queueable, SerializesModels;

    public $cliente;

    public function __construct($cliente)
    {
        $this->cliente = $cliente;
    }

    public function build()
    {
        return $this->subject($this->cliente->nombres . ', ¿Ya hiciste las compras?')->view('wiqli.recordatorioCompra');
    }
}