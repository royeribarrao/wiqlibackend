<?php namespace App\Traits;

trait Audit {
  protected static function boot() {
    parent::boot();
    static::creating( function ($model) {
      if ( isset(auth()->user()->id) ) {
        $model->created_user = auth()->user()->id;
        $model->updated_user = auth()->user()->id;
      } else {
        $model->created_user = null;
        $model->updated_user = null;
      }
    });
    
    static::updating( function ($model) {
      if ( isset(auth()->user()->id) ) {
        $model->updated_user = auth()->user()->id;
      } else {
        $model->updated_user = null;
      }
    });
  
    static::deleting( function ($model) {
      if ( isset(auth()->user()->id)) {
        $model->deleted_user = auth()->user()->id;
      } else {
        $model->deleted_user = null;
      }
    });
  }
}