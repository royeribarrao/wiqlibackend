<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ExampleController;
use App\Http\Controllers\MaintenanceSedeController;
use App\Http\Controllers\ProductController;

use App\Http\Controllers\Wiqli\WiqliPedidoController;
use App\Http\Controllers\Wiqli\WiqliProductoController;
use App\Http\Controllers\Wiqli\WiqliCategoriaController;
use App\Http\Controllers\Wiqli\WiqliUnidadController;
use App\Http\Controllers\Wiqli\WiqliConfiguracionController;
use App\Http\Controllers\Admin\AdminPedidoController;
use App\Http\Controllers\Admin\CorreoReferenteReferidoController;
use App\Http\Controllers\Admin\CuponDescuentoController;
use App\Http\Controllers\Admin\AdminScrapController;

use App\Http\Controllers\Cliente\ClientePedidoController;
use App\Http\Controllers\Cliente\ClienteSuscripcionController;

use App\Http\Controllers\ProductosScrapingController;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Rutas para repo
Route::group(['middleware' => 'auth:api'], function () use ($router) {

    Route::get('users/sedes', [UserController::class, 'getSedes']);
    Route::get('users/update-state/{id}', [UserController::class, 'cambiarEstado']);
    Route::get('users/profiles', [UserController::class, 'getProfiles']);
    Route::get('users/logout', [UserController::class, 'logout']);
    Route::get('users', [UserController::class, 'get']);
    Route::get('users/{id}', [UserController::class, 'show']);
    Route::post('users', [UserController::class , 'store']);
    Route::post('users/{id}', [UserController::class, 'update']);
    Route::get('pedido/ultimo', [UserController::class, 'getUltimaCompra']);
    Route::get('pedidos/descuento-referidos', [UserController::class, 'getTotalDescuentoReferidos']);
    Route::get('cliente/cupon-descuento', [UserController::class, 'getCuponDescuento']);

    Route::post('usuario/crear-pedido', [ClientePedidoController::class, 'crearPedido']);
    Route::get('usuario/informacion', [UserController::class, 'getInformacion']);
    //Diagnostics
    Route::get('maintenances/sedes', [MaintenanceSedeController::class, 'get']);
    Route::get('maintenances/sedes/simple', [MaintenanceSedeController::class, 'getAll']);
    Route::get('maintenances/sedes/{id}', [MaintenanceSedeController::class, 'show']);
    Route::post('maintenances/sedes', [MaintenanceSedeController::class, 'store']);
    Route::post('maintenances/sedes/state', [MaintenanceSedeController::class, 'state']);
    Route::post('maintenances/sedes/{id}', [MaintenanceSedeController::class, 'update']);


    Route::get('authinfo', [UserController::class, 'getAuthenticated']);
});

//Rutas para cliente

Route::group(['middleware' => 'auth:api'], function () use ($router) {
    Route::post('cliente/crear-suscripcion', [ClienteSuscripcionController::class, 'crearSuscripcion']);
    Route::post('cliente/editar-productos-suscripcion', [ClienteSuscripcionController::class, 'editarProductosSuscripcion']);
    Route::post('cliente/editar-periodo-suscripcion', [ClienteSuscripcionController::class, 'editarPeriodoSuscripcion']);
    Route::post('cliente/editar-datos-tarjeta-suscripcion', [ClienteSuscripcionController::class, 'editarDatosTarjetaSuscripcion']);
    Route::get('cliente/suscripcion/cancelar', [ClienteSuscripcionController::class, 'cancelarSuscripcion']);
    Route::get('cliente/suscripcion/productos/todos', [UserController::class, 'getProductosSuscripcion']);
});

//Rutas para admin

Route::group(['middleware' => 'auth:api'], function () use ($router) {
    Route::post('admin/pedido/registrar-pago-pedido/{pedidoId}', [AdminPedidoController::class, 'registrarPagoPedido']);
    Route::get('admin/pedido/pagar-total-pedido/{pedidoId}', [AdminPedidoController::class, 'pagarTotalPedido']);
    Route::get('admin/pedido/cancelar-pago-pedido/{pedidoId}', [AdminPedidoController::class, 'cancelarPagoPedido']);
    Route::get('admin/pedido/obtener-pagos/{pedidoId}', [AdminPedidoController::class, 'obtenerPagos']);

    Route::post('admin/pedidos/enviar-boletas', [AdminPedidoController::class, 'enviarBoletaCliente']);
});

//Rutas para wiqli
Route::group(['middleware' => 'auth:api'], function () use ($router) {
    Route::get('wiqli/pedidos', [AdminPedidoController::class, 'all']);
    Route::get('wiqli/pedido/{pedidoId}', [AdminPedidoController::class, 'showDetalle']);
    Route::get('wiqli/pedido/obtener-informacion/{pedidoId}', [AdminPedidoController::class, 'obtenerInformacionPedido']);
    Route::post('wiqli/pedido/detalle/agregar-producto/{pedidoId}/{productoId}', [AdminPedidoController::class, 'agregarProductoDetalleAPedido']);
    Route::get('wiqli/pedidos/actualizar-estado/{pedidoId}', [AdminPedidoController::class, 'cambiarEstado']);
    Route::post('wiqli/pedido/{pedidoId}', [AdminPedidoController::class, 'updatePedido']);

    Route::get('wiqli/pedido/detalle/actualizar-estado/{detalleId}', [AdminPedidoController::class, 'updateEstadoDetalle']);
    Route::get('wiqli/pedidos-incorrectos/todos', [AdminPedidoController::class, 'allWrongs']);

    Route::get('wiqli/detalle-pedido/eliminar/{detalleId}', [AdminPedidoController::class, 'deleteDetalle']);

    Route::get('wiqli/productos', [WiqliProductoController::class, 'allAdmin']);
    Route::get('wiqli/productos/actualizar-estado/{productoId}', [WiqliProductoController::class, 'actualizarEstadoProducto']);
    Route::get('wiqli/producto/{productoId}', [WiqliProductoController::class, 'show']);
    Route::post('wiqli/producto', [WiqliProductoController::class, 'crear']);
    Route::post('wiqli/producto/{id}', [WiqliProductoController::class, 'actualizar']);
    Route::post('wiqli/producto/eliminar/{id}', [WiqliProductoController::class, 'eliminar']);

    Route::get('wiqli/productos/relacion-scraping', [WiqliProductoController::class, 'all']);

    Route::get('wiqli/unidades/todos', [WiqliUnidadController::class, 'all']);
    Route::get('wiqli/categorias/todos', [WiqliCategoriaController::class, 'all']);

    //productos externos que son scrapeados
    Route::get('wiqli/productos-scrapeados/todos', [ProductosScrapingController::class, 'todosProductos']);
    Route::post('wiqli/producto-scrapeado', [ProductosScrapingController::class, 'crear']);
    Route::get('wiqli/producto-scrapeado/{productoId}', [ProductosScrapingController::class, 'show']);
    Route::post('wiqli/producto-scrapeado/{productoId}', [ProductosScrapingController::class, 'update']);
    Route::get('wiqli/producto-externo/estadistica/{productoId}', [ProductosScrapingController::class, 'getPrecios']);
    Route::get('wiqli/producto-externo/estadistica/precios/scraping/tiendas/{productoId}', [ProductosScrapingController::class, 'getPreciosScraping']);

    //configuracion
    Route::get('wiqli/configuracion', [WiqliConfiguracionController::class, 'getConfiguracion']);
    Route::post('wiqli/configuracion/guardar-configuracion', [WiqliConfiguracionController::class, 'guardarConfiguracion']);

    Route::get('wiqli/cupones-descuento', [CuponDescuentoController::class, 'all']);
    Route::post('wiqli/cupones-descuento/actualizar-estado/{cuponId}', [CuponDescuentoController::class, 'updateState']);
    Route::post('wiqli/cupon-descuento/{cuponId}', [CuponDescuentoController::class, 'update']);
    

    Route::get('wiqli/cupones-descuento/envio-correos/codigo/referente', [CorreoReferenteReferidoController::class, 'enviarCorreos']);
    Route::get('wiqli/precios-comparacion', [AdminScrapController::class, 'comparacionPrecios']);
});