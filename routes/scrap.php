<?php

use App\Http\Controllers\Wiqli\WiqliScrapController;

Route::get('wiqli/productos/scraping', [WiqliScrapController::class, 'obtenerProductosScraping']);
Route::post('wiqli/scraping/crear-precios/vea', [WiqliScrapController::class, 'almacenarEnCacheVea']);
Route::post('wiqli/scraping/crear-precios/juntoz', [WiqliScrapController::class, 'almacenarEnCacheJuntoz']);
Route::post('wiqli/scraping/crear-precios/tottus', [WiqliScrapController::class, 'almacenarEnCacheTottus']);
Route::post('wiqli/scraping/crear-precios/wong', [WiqliScrapController::class, 'almacenarEnCacheWong']);

Route::get('wiqli/ver-data-scraping', [WiqliScrapController::class, 'crearPrecios']);
Route::get('wiqli/scraping/ver-data/vea', [WiqliScrapController::class, 'verDataCacheVea']);
Route::get('wiqli/scraping/ver-data/juntoz', [WiqliScrapController::class, 'verDataCacheJuntoz']);
Route::get('wiqli/scraping/ver-data/tottus', [WiqliScrapController::class, 'verDataCacheTottus']);
Route::get('wiqli/scraping/ver-data/wong', [WiqliScrapController::class, 'verDataCacheWong']);

Route::get('wiqli/scraping/crear-precios/vea', [WiqliScrapController::class, 'crearPreciosVea']);
Route::get('wiqli/scraping/crear-precios/juntoz', [WiqliScrapController::class, 'crearPreciosJuntoz']);
Route::get('wiqli/scraping/crear-precios/tottus', [WiqliScrapController::class, 'crearPreciosTottus']);
Route::get('wiqli/scraping/crear-precios/wong', [WiqliScrapController::class, 'crearPreciosWong']);

