<?php

use App\Http\Controllers\Wiqli\WiqliScrapController;
use App\Http\Controllers\ExampleController;
use App\Http\Controllers\Wiqli\WiqliPedidoController;
use App\Http\Controllers\Admin\CorreoReferenteReferidoController;
use App\Http\Controllers\Admin\AdminScrapController;
use App\Http\Controllers\Admin\AdminNotificacionController;
use App\Http\Controllers\Cliente\ClienteSuscripcionController;
use App\Http\Controllers\PayUController;

Route::get('updateVea', [ExampleController::class, 'actualizarProductosExternosVea']);
Route::get('updateTottus', [ExampleController::class, 'actualizarProductosExternosTottus']);
Route::get('updateWong', [ExampleController::class, 'actualizarProductosExternosWong']);
Route::get('verPedido/{id}', [ExampleController::class, 'verPdf']);
Route::get('enviarMail', [CorreoReferenteReferidoController::class, 'enviarCuponDescuentoUnico']);

Route::get('obtenerComparativa', [AdminScrapController::class, 'comparacionPrecios']);
Route::get('verCorreos', [AdminNotificacionController::class, 'verCorreos']);
Route::get('recordarCompra', [AdminNotificacionController::class, 'recordarCompra']);
Route::get('recordarCompraNavidad', [AdminNotificacionController::class, 'recordarCompraNavidad']);
Route::get('usuarios-no-compra', [AdminNotificacionController::class, 'clientesNoCompraSemanaAtras']);

Route::get('enviar-recordatorio', [ExampleController::class, 'recordarCompra']);
Route::get('probarController', [ExampleController::class, 'recordatorioSuscripcion']);
Route::get('ping', [PayUController::class, 'ping']);
Route::get('metodos-pago', [PayUController::class, 'getMetodosDePago']);
Route::get('ver-ambiente', [PayUController::class, 'getEnvironment']);
Route::get('ver-tarjetas', [PayUController::class, 'getListaTokensPayU']);
Route::get('ver-transaccionId/{transaccionId}', [PayUController::class, 'consultaPorIdentificadorDelaTransaccion']);



Route::get('verCorreoSuscripcion', [ClienteSuscripcionController::class, 'verCorreo']);
Route::get('verCorreoSuscripcionAdmin', [ClienteSuscripcionController::class, 'verCorreoSuscripcionAdmin']);

Route::get('/greeting', function () {
    return view('wiqli.recordatorioCompra');
});