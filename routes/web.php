<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Web\ContactoController;
use App\Http\Controllers\DistritoController;
use App\Http\Controllers\CambioController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\Auth\RegisterClienteController;
use App\Http\Controllers\Wiqli\WiqliProductoController;
use App\Http\Controllers\Wiqli\WiqliCuponDescuentoController;
use App\Http\Controllers\Wiqli\WiqliConfiguracionController;
use App\Http\Controllers\Wiqli\WiqliPedidoController;

use App\Http\Controllers\Admin\CorreoReferenteReferidoController;

Route::post('oauth/token', [ AuthController::class, 'login'])->name('api/login');
Route::post('oauth-cliente/token', [ AuthController::class, 'loginCliente'])->name('api/login-cliente');
Route::post('cliente/registro', [ RegisterController::class, 'register']);
Route::get('verificar-correo/{codigo}', [ RegisterController::class, 'confirmacion']);

Route::get('wiqli/configuracion', [WiqliConfiguracionController::class, 'getConfiguracion']);

Route::get('wiqli/obtener-x-producto', [WiqliProductoController::class, 'obtenerProductoByNombre']);
Route::get('wiqli/verificar-cupon/{codigo}/{correo}', [WiqliCuponDescuentoController::class, 'verificarCupon']);

Route::get('distritos/todos', [DistritoController::class, 'getDistritos']);

Route::post('registro-usuario', [RegisterController::class, 'register']);
Route::post('contacto/email', [ContactoController::class, 'enviarEmail']);

////////////////////
Route::get('generar-contra', function () {
    return $password = app('hash')->make('secreto');
});


Route::get('string', function (){
    $dt = new DateTime();
    return $dt->format('Y-m-d');
});

require( app_path().'/../routes/example.php');
require( app_path().'/../routes/wiqli.php');
require( app_path().'/../routes/scrap.php');