<?php

use App\Http\Controllers\Wiqli\WiqliProductoController;
use App\Http\Controllers\Wiqli\WiqliPedidoController;
use App\Http\Controllers\Admin\AdminPedidoController;

Route::get('wiqli/productos/todos', [WiqliProductoController::class, 'all']);
Route::post('wiqli/crear-pedido', [WiqliPedidoController::class, 'crearPedido']);
Route::get('wiqli/exportar-pedido', [WiqliPedidoController::class, 'exportExcel']);

Route::get('wiqli/ver-data', [WiqliPedidoController::class, 'all']);
Route::get('wiqli/ver-pdf/{pedidoId}', [WiqliPedidoController::class, 'verPdf']);

Route::get('wiqli/pedidos/exportar-excel/{fechaInicial}/{fechaFinal}', [WiqliPedidoController::class, 'exportarExcel']);
Route::get('wiqli/pedidos/exportar-excel/todos', [WiqliPedidoController::class, 'exportarExcelTodos']);
Route::get('wiqli/pedidos/exportar-excel', [AdminPedidoController::class, 'exportarExcelSeleccionados']);