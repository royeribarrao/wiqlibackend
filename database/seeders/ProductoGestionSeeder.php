<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductoGestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('productos_gestion')->insert([
            'sku_producto' => '20200043230',
            'gestion_id' => 4
        ]);

        \DB::table('productos_gestion')->insert([
            'sku_producto' => '20210010010',
            'gestion_id' => 1
        ]);
    }
}
