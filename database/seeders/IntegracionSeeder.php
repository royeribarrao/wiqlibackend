<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class IntegracionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('integracions')->insert([
            'nombre' => 'repo'
        ]);
        \DB::table('integracions')->insert([
            'nombre' => 'woocommerce'
        ]);
        \DB::table('integracions')->insert([
            'nombre' => 'shopify'
        ]);
    }
}
