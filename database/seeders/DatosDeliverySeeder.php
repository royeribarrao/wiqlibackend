<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatosDeliverySeeder extends Seeder
{
    public function run()
    {
        \DB::table('datos_delivery')->insert([
            'nombres' => 'royer',
            'apellidos' => 'victor',
            'departamento' => 'Lima',
            'provincia' => 'Lima',
            'distrito' => 'san_borja',
            'direccion' => 'jr chacacuboc',
            'celular' => '939784580',
            'correo' => 'royeribarrao@gmail.com',
            'fecha_recojo' => '2021-12-14',
            'latitud' => '-12.093300',
            'longitud' => '-76.994590'
        ]);

        \DB::table('datos_delivery')->insert([
            'nombres' => 'mayra',
            'apellidos' => 'briceño',
            'departamento' => 'Lima',
            'provincia' => 'Lima',
            'distrito' => 'san_borja',
            'direccion' => 'jr abancay',
            'celular' => '999568588',
            'correo' => 'bricemay@gmail.com',
            'fecha_recojo' => '2021-12-15',
            'latitud' => '-12.093300',
            'longitud' => '-76.994590'
        ]);
    }
}
