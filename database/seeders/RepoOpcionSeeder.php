<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RepoOpcionSeeder extends Seeder
{
    public function run()
    {
        \DB::table('repo_opciones')->insert([
            'codigo' => 10,
            'abreviatura' => 'Devolución por transferencia',
            'nombre' => 'Quiero una transferencia por Plin o Yape.'
        ]);
        \DB::table('repo_opciones')->insert([
            'codigo' => 20,
            'nombre' => 'Quiero una devolución al medio de pago con el que compré.',
            'abreviatura' => 'Devolución a medio de pago'
        ]);
        \DB::table('repo_opciones')->insert([
            'codigo' => 30,
            'nombre' => 'Quiero una gif card para compra online.',
            'abreviatura' => 'Gift card online'
        ]);
        \DB::table('repo_opciones')->insert([
            'codigo' => 40,
            'nombre' => 'Quiero una gif card para compra en tienda física.',
            'abreviatura' => 'Gift card física'
        ]);
        \DB::table('repo_opciones')->insert([
            'codigo' => 50,
            'nombre' => 'Quiero vender mi gift card al 85 % del precio por Plin o Yape.',
            'abreviatura' => ''
        ]);
        \DB::table('repo_opciones')->insert([
            'codigo' => 60,
            'nombre' => 'Quiero cambiar de talla.',
            'abreviatura' => 'Cambio de talla'
        ]);
    }
}