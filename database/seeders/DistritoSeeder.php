<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DistritoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('distritos')->insert([
            'nombre' => 'Barranco',
            'valor' => 'barranco',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Jesús María',
            'valor' => 'jesus_maria',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'La Molina',
            'valor' => 'la_molina',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'La Victoria',
            'valor' => 'la_victoria',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Lince',
            'valor' => 'lince',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Magdalena',
            'valor' => 'magdalena',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Miraflores',
            'valor' => 'miraflores',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Pueblo Libre',
            'valor' => 'pueblo_libre',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'San Borja',
            'valor' => 'san_borja',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'San Isidro',
            'valor' => 'san_isidro',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'San Luis',
            'valor' => 'san_luis',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'San Miguel',
            'valor' => 'san_miguel',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Surco',
            'valor' => 'surco',
            'activo' => true
        ]);
        \DB::table('distritos')->insert([
            'nombre' => 'Surquillo',
            'valor' => 'surquillo',
            'activo' => true
        ]);
    }
}
