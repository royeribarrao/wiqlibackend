<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            [
                'name' => 'Admin Repo',
                'order' => '1',
            ], [
                'name' => 'Admin Tienda',
                'order' => '2',
            ],[
                'name' => 'Cliente',
                'order' => '3',
            ],[
                'name' => 'Admin',
                'order' => '4',
            ] 
        ]);
    }
}
