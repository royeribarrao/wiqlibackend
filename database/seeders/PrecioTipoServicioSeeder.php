<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PrecioTipoServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño A',
            'costo' => 5.50,
            'tipo_servicio_id' => 1
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño B',
            'costo' => 5.50,
            'tipo_servicio_id' => 1
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño C',
            'costo' => 7.50,
            'tipo_servicio_id' => 1
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño A',
            'costo' => 6.50,
            'tipo_servicio_id' => 2
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño B',
            'costo' => 6.50,
            'tipo_servicio_id' => 2
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño C',
            'costo' => 10.50,
            'tipo_servicio_id' => 2
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño A',
            'costo' => 4.50,
            'tipo_servicio_id' => 3
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño B',
            'costo' => 4.50,
            'tipo_servicio_id' => 3
        ]);

        \DB::table('precio_tipo_servicio')->insert([
            'nombre' => 'Tamaño C',
            'costo' => 7.00,
            'tipo_servicio_id' => 3
        ]);
    }
}
