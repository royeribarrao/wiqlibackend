<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DiagnosticSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('diagnostics')->insert([
            [
                'name' => 'DISCOPATÍA DEGENERATIVA CERVICAL',
            ], 
            [
                'name' => 'HERNIA C5-C6-C7',
            ],
            [
                'name' => 'SACROILITIS BILATERAL',
            ],
            [
                'name' => 'ARTROSIS CADERA DERECHA',
            ],
            [
                'name' => 'ARTROSIS CADERA IZQUIERDA',
            ],
            [
                'name' => 'ARTROSIS COLUMNA LUMBAR',
            ],
            [
                'name' => 'DISCOPATÍA DEGENERATIVA LUMBAR',
            ],
            [
                'name' => 'ARTROSIS CADERA IZQUIERDA MUY SEVERA QUE AMERITA PRÓTESIS (NO APLICA A LÁSER)',
            ],
            [
                'name' => 'ESCOLIOSIS CERVICO-DORSAL',
            ],
            [
                'name' => 'ESCOLIOSIS LUMBAR',
            ],
            [
                'name' => 'PROTRUSIÓN L3-L4',
            ],
            [
                'name' => 'HERNIA L4-L5-S1',
            ],
            [
                'name' => 'DISCOPATÍA DORSAL',
            ],
            [
                'name' => 'DISCOPATÍA CERVICAL',
            ],
            [
                'name' => 'HERNIA L4-L5',
            ],
            [
                'name' => 'SECUELA ÓSEA POR CONTUSIÓN EN CADERA DERECHA (DONACIÓN)',
            ],
            [
                'name' => 'NECROSIS AVASCULAR CADERA IZQUIERDA',
            ],
            [
                'name' => 'ARTROSIS SEVERA CADERA IZQUIERDA',
            ],
            [
                'name' => 'ESPONDILOSIS LUMBAR',
            ],
            [
                'name' => 'HERNIA L3-L4',
            ],
            [
                'name' => 'ESPONDILOARTROSIS LUMBAR DEGENERATIVA',
            ],
            [
                'name' => 'DISCOPATÍA LUMBAR DEGNERATIVA',
            ],
            [
                'name' => 'ARTROSIS DE CADERA DERECHA MODERADA',
            ],
            [
                'name' => 'ARTROSIS DE CADERA IZQUIERDA MODERADA',
            ],
            [
                'name' => 'ARTROSIS COLUMNA DORSAL',
            ],
            [
                'name' => 'ARTROSIS Y CONDROMALACIA SEVERA RODILLA DERECHA',
            ],
            [
                'name' => 'ARTROSIS Y CONDROMALACIA SEVERA RODILLA IZQUIERDA'
            ],
            [
                'name' => 'DISCOPATÍA DEGENERATIVA LUMBAR'
            ],
            [
                'name' => 'DISCOPATÍA DEGENERATIVA DORSAL'
            ],
            [
                'name' => 'DISCOPATÍA DEGENERATIVA CERVICAL'
            ],
            [
                'name' => 'ANTEROLISTESIS L5-S1'
            ],
            [
                'name' => 'ARTROSIS MUY SEVERA RODILLA DERECHA'
            ],
            [
                'name' => 'ARTROSIS SEVERA CADERA DERECHA'
            ],
            [
                'name' => 'OSTEOARTRITIS DE AMBAS RODILLAS'
            ]

        ]);
    }
}
