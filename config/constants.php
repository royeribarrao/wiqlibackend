<?php

return [ 
    'correosAdmin' => [
        'ricardo@repo.com.pe',
        'renzo@repo.com.pe',
        'royer@repo.com.pe',
        'mariapia@repo.com.pe'
    ],
    'correo_prueba' => 'contacto@repo.com.pe',
    'correo_produccion' => 'contacto@repo.com.pe',
    'correo_desarrollo' =>'royer@repo.com.pe',
    'correo_ricardo' =>'ricardo@repo.com.pe',
    'correo_renzo' =>'renzo@repo.com.pe',
    'correo_royer' =>'royer@repo.com.pe',
    'correo_mapi' => 'mariapia@repo.com.pe'
];