<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo pedido</title>
    <link href="/wiqli/css/style.css" rel="stylesheet" />
    <style>
        .tituloResaltante{
            color: #A7C94E;
            font-weight: 700;
            margin: 2rem;
        }


        .textoDisclaimer{
            margin: 1rem;
            font-size: calc(3px + 0.8vw + 0.6rem);
            color: #A7C94E;
            font-weight: 600;
        }

        .mensajeFinalDestacado{
        color: #F23440;
        font-weight: 700;
        }

        .contenedorPrimario{
            display: flex;
            justify-content: center;
        }

        .contenedorSecundario{
            max-width: 500px;
            text-align: center;
            margin: 0.5rem;
            border: #F23440 solid 2px;
            padding-top: 2rem;
            padding-bottom: 2rem;
            padding-right: 8px;
            padding-left: 8px;
            border-radius: 30px;
        }

        .textoContacto{
            margin: 0.3rem;
        }

        a{
            color: #F19600;
        }

        .tituloDescripcion{
            font-weight: 800;
        }
        table, th, td {
            border: 1px solid black;
        }
        .textoRojo{
            color: red
        }
    </style>
</head>
<body>
    <div class="contenedorPrimario">
        <div class="contenedorSecundario">
            <div class="primeraParteCorreo">
                <img src="{{ $message->embed("miniLogo.png") }}" alt="logo wiqli haz las compras desde tu casa">
                <h1 class="tituloResaltante">Nuevo pedido de Wiqli</h1>
            </div>
            <div class="detalleCorreo">
    
                <p class="mensajeFinalDestacado">Detalle de pedido</p>
                <div>
                    <p class="tituloDescripcion">Nombre:</p>
                    <p>{{ $cliente->nombres }} {{ $cliente->apellidos}}</p>
                    <p class="tituloDescripcion">Direccion:</p>
                    <p>{{ $cliente->direccion }}</p>
                    <p>{{ $cliente->referencia }}</p>
                    <p class="tituloDescripcion">Teléfono:</p>
                    <p>{{ $cliente->telefono }}</p>
                    <p class="tituloDescripcion">Fecha de entrega:</p>
                    <p>{{ $pedido->fecha_entrega }}</p>
                    <p class="tituloDescripcion">Observación:</p>
                    <p>{{ $pedido->observacion }}</p>
                    <p class="tituloDescripcion">Productos:</p>
                    <table style="width:100%">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Unidad</th>
                                <th>Cantidad</th>
                                <th>Precio unitario</th>
                                <th>Total</th>
                              </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                                <tr>
                                    <td>{{ $producto['producto'] ? $producto['producto']['nombre'] : $producto['nombre_desc'] }}</td>
                                    <td>{{ $producto['unidad_medida'] }}</td>
                                    <td>{{ $producto['producto'] ? $producto['cantidad'] : $producto['cantidad_desc'] }}</td>
                                    <td>{{ $producto['precio_unitario'] }}</td>
                                    <td>{{ 'S/ ' . $producto['total'] }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4">Total productos</td>
                                <td>{{'S/ ' . number_format($pedido['totalProductos'], 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">Delivery</td>
                                <td>{{'S/ ' . number_format($pedido['costo_delivery'], 2, '.', ',')}}</td>
                            </tr>

                            @if ($pedido['descuento'])
                            <tr>
                                <td colspan="4">Descuento</td>
                                <td class="textoRojo">{{'S/ ' . number_format($pedido['descuento'], 2, '.', ',')}}</td>
                            </tr>
                            @endif

                            <tr>
                                <td colspan="4">Total</td>
                                <td>{{'S/ ' . number_format($pedido['total'], 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <p class="tituloDescripcion">Total a Pagar (aprox.):</p>
                    <p> {{'S/ ' . number_format($pedido['total'], 2, '.', ',')}}</p>
                    
                    @if (count($productosAdicionales) > 0)
                        <p class="tituloDescripcion">Productos adicionales</p>
                        <table style="width:100%">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($productosAdicionales as $producto)
                                    <tr>
                                        <td>{{ $producto['producto'] ? $producto['producto']['nombre'] : $producto['nombre_desc'] }}</td>
                                        <td>{{ $producto['producto'] ? $producto['cantidad'] : $producto['cantidad_desc'] }}</td>
                                        <td>{{ $producto['producto'] ? $producto['producto']['unidad']['nombre'] : 'No tiene unidad' }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif

                    @if (count($totalReferidos) > 0)
                        <p class="tituloDescripcion">Las siguientes personas usaron tu código de referido y te damos un descuento de:</p>
                        @foreach ($totalReferidos as $referido)
                            <p>Persona: {{ $referido->correo_referido }}</p>
                            <p>Monto: {{ $referido->monto }}</p>
                        @endforeach
                    @endif

                    @if ($cuponRecurrencia)
                    <p>
                        Aplicaste a un cupón de descuento por ser un comprador recurrente: 
                        {{'S/ ' . number_format($pedido['descuento'], 2, '.', ',')}}
                    </p> 
                    @endif
                    
                    <p class="tituloDescripcion">Total ahorro (aprox.):</p>
                    <p> {{ number_format($totalAhorro, 2, '.', ',') }}</p>

                    @if ($referidoUsaDescuentoReferente)
                    <p>
                        Además, ahorraste {{'S/ ' . number_format($pedido['descuento'], 2, '.', ',')}} con tu cupón de descuento aplicado.
                    </p>
                    @endif

                </div>
            </div>
            <hr>
            <div class="contenedorCorreo">
                <h5 class="textoDisclaimer">¿Tienes alguna duda?</h5>
                <p class="textoContacto">Escríbenos al</p>
                <p class="textoContacto"><a href="https://api.whatsapp.com/send?phone=947298060&text=Hola,%20necesito%20ayuda%20para%20hacer%20mi%20pedido">947298060</a></p>
            </div>
        </div>
    </div>
</body>