<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="correosWiqli.css">
    <title>Compra ahora!</title>
    <style>
        .tituloResaltante{
            color: #A7C94E;
            font-weight: 700;
            margin: 2rem;
        }


        .textoDisclaimer{
            margin: 1rem;
            font-size: calc(3px + 0.8vw + 0.6rem);
            color: #A7C94E;
            font-weight: 600;
        }

        .mensajeFinalDestacado{
        color: #F23440;
        font-weight: 700;
        }

        .contenedorPrimario{
            display: flex;
            justify-content: center;
        }

        .contenedorSecundario{
            max-width: 500px;
            text-align: center;
            margin: 0.5rem;
            border: #F23440 solid 2px;
            padding-top: 2rem;
            padding-bottom: 2rem;
            padding-right: 8px;
            padding-left: 8px;
            border-radius: 30px;
        }

        .textoContacto{
            margin: 0.3rem;
        }

        a{
            color: #F19600;
        }

        .tituloDescripcion{
            font-weight: 800;
        }
        table, th, td {
            border: 1px solid black;
        }
        .textoRojo{
            color: red
        }
    </style>
</head>
<body>
    <div class="contenedorPrimario">
        <div class="contenedorSecundario">
            <img src="{{ $message->embed("miniLogo.png") }}" alt="logo wiqli haz las compras desde tu casa">
        <h2 class="tituloResaltante">¡{{$cliente->nombres}}, empieza una nueva semana!</h2>
        <h2 class="tituloResaltante">Olvídate de ir al mercado y haz tus compras en Wiqli</h2>
        <div>
            <div>
                <h3 class="tituloDescripcion">Entra desde aquí a 
                    <a href="https://www.wiqli.com/">www.wiqli.com</a> y pide los mejores productos a la puerta de tu casa.
                </h3>
            </div>
        </div>
        </div>
    </div>
</body>
</html>