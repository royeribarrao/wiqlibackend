<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Correo de confirmación Wiqli</title>
    <link href="/wiqli/css/style.css" rel="stylesheet" />
    <style>
        .tituloResaltante{
            color: #A7C94E;
            font-weight: 700;
            margin: 2rem;
        }


        .textoDisclaimer{
            margin: 1rem;
            font-size: calc(3px + 0.8vw + 0.6rem);
            color: #A7C94E;
            font-weight: 600;
        }

        .mensajeFinalDestacado{
        color: #F23440;
        font-weight: 700;
        }

        .contenedorPrimario{
            display: flex;
            justify-content: center;
        }

        .contenedorSecundario{
            max-width: 500px;
            text-align: center;
            margin: 0.5rem;
            border: #F23440 solid 2px;
            padding-top: 2rem;
            padding-bottom: 2rem;
            padding-right: 8px;
            padding-left: 8px;
            border-radius: 30px;
        }

        .textoContacto{
            margin: 0.3rem;
        }

        a{
            color: #F19600;
        }

        .tituloDescripcion{
            font-weight: 800;
        }
        table, th, td {
            border: 1px solid black;
        }
        .textoRojo{
            color: red
        }
    </style>
</head>
<body>
    <div class="contenedorPrimario">
        <div class="contenedorSecundario">
            <div class="primeraParteCorreo">
                
                <img src="{{ $message->embed("miniLogo.png") }}" alt="logo wiqli haz las compras desde tu casa">
            </div>
            <div class="detalleCorreo">
                <h1 class="tituloResaltante">Nuevo cliente suscrito</h1>
                <h1>Nombre: {{ $cliente->name}}</h1>
                <h1>Email: {{ $cliente->email}}</h1>
                <h1>Código: {{ $suscripcion->codigoSuscripcion}}</h1>
            </div>
            <hr>
            <div class="contenedorCorreo">
                <h5 class="textoDisclaimer">¿Tienes alguna duda?</h5>
                <p class="textoContacto">Escríbenos al</p>
                <p class="textoContacto"><a href="https://api.whatsapp.com/send?phone=947298060&text=Hola,%20necesito%20ayuda%20para%20hacer%20mi%20pedido">947298060</a></p>
            </div>
        </div>
    </div>
</body>

</html>