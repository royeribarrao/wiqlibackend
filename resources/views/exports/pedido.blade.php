<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <head>
        <style>
            .tituloResaltante{
                color: #A7C94E;
                font-weight: 700;
                margin: 2rem;
            }
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                
            }
            .texto-centrado{
                text-align: center !important
            }
            .texto-derecho{
                text-align: right
            }
            .texto-izquierda{
                text-align: left
            }
            .textoRojo{
                color: red
            }
        </style>
    </head>
    <body>
        <h4>Reporte del usuario</h4>
        <table style="width:100%">
            <thead>
                <tr>
                    <th class="texto-centrado">Cliente</th>
                    <th class="texto-centrado">Dirección</th>
                    <th class="texto-centrado">Teléfono</th>
                    <th class="texto-centrado">Fecha de entrega</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $pedido['cliente']['nombres'] }} {{ $pedido['cliente']['apellidos'] }}</td>
                    <td>{{ $pedido['cliente']['direccion'] .'-'. $pedido['cliente']['referencia'] }}</td>
                    <td>{{ $pedido['cliente']['telefono'] }}</td>
                    <td>{{ $pedido['fecha_entrega'] }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <table style="width:100%">
            <thead>
                <tr>
                    <th>Producto</th>
                    <th>Unidad</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th>Total</th>
                  </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <td>{{ $producto['producto'] ? $producto['producto']['nombre'] : $producto['nombre_desc'] }}</td>
                        <td>{{ $producto['unidad_medida'] }}</td>
                        <td class="texto-centrado">{{ $producto['producto'] ? $producto['cantidad'] : $producto['cantidad_desc'] }}</td>
                        <td class="texto-centrado">{{ $producto['precio_unitario'] }}</td>
                        <td class="texto-derecho">{{ 'S/ ' . $producto['total'] }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4">Sub total</td>
                    <td class="texto-derecho">{{ 'S/ ' . $pedido['totalProductos']}}</td>
                </tr>
                <tr>
                    <td colspan="4">Costo Delivery</td>
                    <td class="texto-derecho">{{ 'S/ ' . $pedido['costo_delivery']}}</td>
                </tr>

                @if ($pedido['descuento'])
                <tr>
                    <td colspan="4">Descuento</td>
                    <td class="textoRojo texto-derecho">{{'S/ ' . number_format($pedido['descuento'], 2, '.', ',')}}</td>
                </tr>
                @endif

                <tr>
                    <td colspan="4">Total</td>
                    <td class="texto-derecho">{{'S/ ' . number_format($pedido['total'], 2, '.', ',')}}</td>
                </tr>
            </tbody>
        </table>
        
        @if ($ahorroExterno)
            <p>
                Total ahorro aprox. {{'S/ ' . number_format($ahorroExterno, 2, '.', ',')}}
            </p>
        @endif

        @if ($cupon)
            <p class="tituloDescripcion">
                Gracias al código {{ $cupon->codigo }}  de {{ $cupon->correo_creador }}
                accediste a un descuento de S/ {{ $cupon->monto }} .
            </p>
        @endif
        @if (count($referidos) > 0)
            <p class="tituloDescripcion">Las siguientes personas usaron tu código y te descontamos lo siguiente:</p>
            @foreach ($referidos as $referido)
                <p>Persona: {{ $referido->correo_referido }}</p>
                <p>Monto: {{ 'S/ ' . $referido->monto }}</p>
            @endforeach
        @endif
    </body>
</html>
