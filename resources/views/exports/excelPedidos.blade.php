<style>
    th, td {
      border: 1px solid black;
    }
    </style>
<table> 
    <tbody>
        {{-- <tr>
            <td> </td>
            <td>PedidoID: </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $cliente)
                <td>{{ $cliente['id'] }}</td>
                <td> </td>
                <td> </td>
            @endforeach
        </tr> --}}
        <tr>
            <td> </td>
            <td>Nombre</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $cliente)
                <td>{{ $cliente['cliente']['nombres'] }}</td>
                <td> </td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Apellido</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $cliente)
                <td>{{ $cliente['cliente']['apellidos'] }}</td>
                <td> </td>
            @endforeach
            
        </tr>
        <tr>
            <td> </td>
            <td>Celular</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $cliente)
                <td>{{ $cliente['cliente']['telefono'] }}</td>
                <td> </td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Fecha Entrega:</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $cliente)
                <td>{{ $cliente['fecha_entrega'] }}</td>
                <td> </td>
            @endforeach
        </tr>
        <tr></tr>
        <tr>
            {{-- <th>ID</th> --}}
            <td> </td>
            <th style="border: 1px solid black;">Producto</th>
            <th style="border: 1px solid black;">Unidad de medida</th>
            <th style="border: 1px solid black;">Precio unitario</th>
            <th style="border: 1px solid black;">Cantidad total</th>
            <th style="border: 1px solid black;">Unidad mínima</th>
            <th style="border: 1px solid black;">Cantidad real</th>
            <th> </th>
            @foreach($pedidos as $cliente)
                <td style="border: 1px solid black;">CANTIDAD</td>
                <td style="border: 1px solid black;">TOTAL</td>
                
            @endforeach
        </tr>
    @foreach($productosAptos as $producto)
        @if ($producto['categoria_id'] == 3)
            <tr>
                {{-- <td>{{ $producto['id'] }}</td> --}}
                <td> </td>
                <td style="border: 1px solid black;">{{ $producto['nombre'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['unidad']['abreviatura'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['precio_unitario'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad_minima'] }}</td>
                <td style="border: 1px solid black;">{{ number_format($producto['cantidad']/$producto['cantidad_minima'], 2, '.', ',') }}</td>
                <td> </td>
                @foreach ($producto['pedidos'] as $key => $pedido)
                    <td style="border: 1px solid black;">{{ isset($pedido['cantidad']) ? $pedido['cantidad'] : '' }}</td>
                    <td style="border: 1px solid black;">{{ isset($pedido['total']) ? number_format($pedido['total'], 3, '.', ',') : '' }}</td>
                    
                @endforeach
            </tr>
        @endif
    @endforeach
    <tr></tr>
    @foreach($productosAptos as $producto)
        @if ($producto['categoria_id'] == 1)
            <tr>
                {{-- <td>{{ $producto['id'] }}</td> --}}
                <td> </td>
                <td style="border: 1px solid black;">{{ $producto['nombre'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['unidad']['abreviatura'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['precio_unitario'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad_minima'] }}</td>
                <td style="border: 1px solid black;">{{ number_format($producto['cantidad']/$producto['cantidad_minima'], 2, '.', ',') }}</td>
                <td> </td>
                @foreach ($producto['pedidos'] as $key => $pedido)
                    <td style="border: 1px solid black;">{{ isset($pedido['cantidad']) ? $pedido['cantidad'] : '' }}</td>
                    <td style="border: 1px solid black;">{{ isset($pedido['total']) ? number_format($pedido['total'], 3, '.', ',') : '' }}</td>
                    
                @endforeach
            </tr>
        @endif
    @endforeach
    <tr></tr>
    @foreach($productosAptos as $producto)
        @if ($producto['categoria_id'] == 2)
            <tr>
                {{-- <td>{{ $producto['id'] }}</td> --}}
                <td> </td>
                <td style="border: 1px solid black;">{{ $producto['nombre'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['unidad']['abreviatura'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['precio_unitario'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad_minima'] }}</td>
                <td style="border: 1px solid black;">{{ number_format($producto['cantidad']/$producto['cantidad_minima'], 2, '.', ',') }}</td>
                <td> </td>
                @foreach ($producto['pedidos'] as $key => $pedido)
                    <td style="border: 1px solid black;">{{ isset($pedido['cantidad']) ? $pedido['cantidad'] : '' }}</td>
                    <td style="border: 1px solid black;">{{ isset($pedido['total']) ? number_format($pedido['total'], 3, '.', ',') : '' }}</td>
                    
                @endforeach
            </tr>
        @endif
    @endforeach
    <tr></tr>
    
    @foreach($productosAptos as $producto)
        @if ($producto['categoria_id'] == 4)
            <tr>
                {{-- <td>{{ $producto['id'] }}</td> --}}
                <td> </td>
                <td style="border: 1px solid black;">{{ $producto['nombre'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['unidad']['abreviatura'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['precio_unitario'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad_minima'] }}</td>
                <td style="border: 1px solid black;">{{ number_format($producto['cantidad']/$producto['cantidad_minima'], 2, '.', ',') }}</td>
                <td> </td>
                @foreach ($producto['pedidos'] as $key => $pedido)
                    <td style="border: 1px solid black;">{{ isset($pedido['cantidad']) ? $pedido['cantidad'] : '' }}</td>
                    <td style="border: 1px solid black;">{{ isset($pedido['total']) ? number_format($pedido['total'], 3, '.', ',') : '' }}</td>
                    
                @endforeach
            </tr>
        @endif
    @endforeach
    <tr></tr>
    @foreach($productosAptos as $producto)
        @if ($producto['categoria_id'] == 5)
            <tr>
                {{-- <td>{{ $producto['id'] }}</td> --}}
                <td> </td>
                <td style="border: 1px solid black;">{{ $producto['nombre'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['unidad']['abreviatura'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['precio_unitario'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad'] }}</td>
                <td style="border: 1px solid black;">{{ $producto['cantidad_minima'] }}</td>
                <td style="border: 1px solid black;">{{ number_format($producto['cantidad']/$producto['cantidad_minima'], 2, '.', ',') }}</td>
                <td> </td>
                @foreach ($producto['pedidos'] as $key => $pedido)
                    <td style="border: 1px solid black;">{{ isset($pedido['cantidad']) ? $pedido['cantidad'] : '' }}</td>
                    <td style="border: 1px solid black;">{{ isset($pedido['total']) ? number_format($pedido['total'], 3, '.', ',') : '' }}</td>
                    
                @endforeach
            </tr>
        @endif
    @endforeach
    <tr></tr>
        <tr>
            <td> </td>
            <td>Total</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>{{ 'S/ ' . number_format($totalVenta, 2, '.', ',') }}</td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $key => $pedido)
                
                <td> </td>
                <td>{{ $pedido['totalProductos'] }}</td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Delivery</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>S/ 10.00</td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $key => $pedido)
                
                <td> </td>
                <td>{{ $pedido['costo_delivery'] }}</td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Descuento</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>S/ 0.00</td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $key => $pedido)
                
                <td> </td>
                <td>{{ $pedido['descuento'] ? $pedido['descuento']: 0.00 }}</td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Total a pagar</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td>{{ 'S/ ' . number_format($totalVenta + 10, 2, '.', ',') }}</td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $key => $pedido)
                
                <td> </td>
                <td>{{ $pedido['total'] }}</td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Pagado</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $key => $pedido)
                
                <td> </td>
                <td>{{ $pedido['pagado'] ? 'SI' : 'NO' }}</td>
            @endforeach
        </tr>
        <tr>
            <td> </td>
            <td>Tipo de pago</td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td></td>
            <td> </td>
            <td> </td>
            @foreach($pedidos as $key => $pedido)
                
                <td> </td>
                <td>{{ $pedido['tipoPago'] == 1 ? 'Contra entrega' : 'Web' }}</td>
            @endforeach
        </tr>
        <tr></tr>
        <tr></tr>
        <tr>
            <td>PRODUCTOS ADICIONALES</td>
        </tr>
        <tr>
            <td> </td>
            <td style="border: 1px solid black;">Producto</td>
            <td style="border: 1px solid black;">Cantidad</td>
            <td style="border: 1px solid black;">Precio Unitario </td>
            <td style="border: 1px solid black;">Venta Total</td>
            <td colspan="2" style="border: 1px solid black;">Cliente</td>
        </tr>
        @foreach ($productosAdicionales as $producto)
            <tr>
                <td> </td>
                <td style="border: 1px solid black;">{{$producto['nombre_desc']}}</td>
                <td style="border: 1px solid black;">{{$producto['cantidad_desc']}}</td>
                <td style="border: 1px solid black;">{{$producto['precio_unitario']}}</td>
                <td style="border: 1px solid black;">{{$producto['total']}}</td>
                <td colspan="2" style="border: 1px solid black;">{{$producto['nombres'] . ' ' . $producto['apellidos']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>