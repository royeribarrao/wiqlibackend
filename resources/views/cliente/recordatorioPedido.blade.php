<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Correo de confirmación Wiqli</title>
    <link href="/wiqli/css/style.css" rel="stylesheet" />
    <style>
        .tituloResaltante{
            color: #A7C94E;
            font-weight: 700;
            margin: 2rem;
        }


        .textoDisclaimer{
            margin: 1rem;
            font-size: calc(3px + 0.8vw + 0.6rem);
            color: #A7C94E;
            font-weight: 600;
        }

        .mensajeFinalDestacado{
        color: #F23440;
        font-weight: 700;
        }

        .contenedorPrimario{
            display: flex;
            justify-content: center;
        }

        .contenedorSecundario{
            max-width: 500px;
            text-align: center;
            margin: 0.5rem;
            border: #F23440 solid 2px;
            padding-top: 2rem;
            padding-bottom: 2rem;
            padding-right: 8px;
            padding-left: 8px;
            border-radius: 30px;
        }

        .textoContacto{
            margin: 0.3rem;
        }

        a{
            color: #F19600;
        }

        .tituloDescripcion{
            font-weight: 800;
        }
        table, th, td {
            border: 1px solid black;
        }
        .textoRojo{
            color: red
        }
    </style>
</head>
<body>
    <div class="contenedorPrimario">
        <div class="contenedorSecundario">
            <div class="primeraParteCorreo">
                
                <h1 class="tituloResaltante">{{ $cliente->name}}, te enviamos el detalle del pedido, próximo a realizar.</h1>
            </div>
            <div class="detalleCorreo">
                <p>
                    Muchas gracias por confiar en nosotros.
                    Se realizará el cobro de tu pedido el día de hoy  {{ $suscripcion->fechaCobro }}.
                    Te enviaremos tu pedido el día seleccionado.
                </p>

                <p class="tituloDescripcion">Productos:</p>
                    <table style="width:100%">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Unidad</th>
                                <th>Cantidad</th>
                                <th>Precio unitario</th>
                                <th>Total</th>
                              </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                                @if($producto['productoId'] != 999)
                                <tr>
                                    <td>{{ $producto['producto'] ? $producto['producto']['nombre'] : $producto['nombre_desc'] }}</td>
                                    <td>{{ $producto['unidad_medida'] }}</td>
                                    <td>{{ $producto['producto'] ? $producto['cantidad'] : $producto['cantidad_desc'] }}</td>
                                    <td>{{ $producto['precio_unitario'] }}</td>
                                    <td>{{ 'S/ ' . $producto['total'] }}</td>
                                </tr>
                                @endif
                            @endforeach
                            
                            
                            <tr>
                                <td colspan="4">Total Productos</td>
                                <td>{{'S/ ' . number_format($suscripcion['montoCobro'], 2, '.', ',')}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">Delivery</td>
                                <td>{{'S/ ' . $cliente->distrito['tarifa'] }}</td>
                            </tr>
                            
                            <tr>
                                <td colspan="4">Total</td>
                                <td>{{'S/ ' . number_format($suscripcion['total'], 2, '.', ',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <p class="tituloDescripcion">Total a Pagar (aprox.):</p>
                    <p> {{'S/ ' . number_format($suscripcion['montoCobro'], 2, '.', ',')}}</p>

                    
                    <p class="tituloDescripcion">Productos adicionales</p>
                    <table style="width:100%">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th>Unidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                                @if($producto['productoId'] == 999)
                                <tr>
                                    <td>{{ $producto['producto'] ? $producto['producto']['nombre'] : $producto['nombre_desc'] }}</td>
                                    <td>{{ $producto['producto'] ? $producto['cantidad'] : $producto['cantidad_desc'] }}</td>
                                    <td>{{ $producto['producto'] ? $producto['producto']['unidad']['nombre'] : 'No tiene unidad' }}</td>
                                </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    

                <p>En caso tengas alguna duda sobre el estado de tu pedido o quieras agregar algún artículo no dudes en contactarnos al 
                    <a href="https://api.whatsapp.com/send?phone=947298060&text=Hola,%20necesito%20ayuda%20para%20hacer%20mi%20pedido">
                        947298060
                    </a>
                </p>
                <p>Te deseamos una excelente semana</p>
                <p class="mensajeFinalDestacado">¡Empieza bien tu semana con Wiqli!</p>
            </div>
            <hr>
            <div class="contenedorCorreo">
                <h5 class="textoDisclaimer">¿Tienes alguna duda?</h5>
                <p class="textoContacto">Escríbenos al</p>
                <p class="textoContacto"><a href="https://api.whatsapp.com/send?phone=947298060&text=Hola,%20necesito%20ayuda%20para%20hacer%20mi%20pedido">947298060</a></p>
            </div>
        </div>
    </div>
</body>

</html>