<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verificacion Cuenta</title>
    <style>
        .tituloResaltante{
            color: #A7C94E;
            font-weight: 700;
            margin: 2rem;
        }


        .textoDisclaimer{
            margin: 1rem;
            font-size: calc(3px + 0.8vw + 0.6rem);
            color: #A7C94E;
            font-weight: 600;
        }

        .mensajeFinalDestacado{
        color: #F23440;
        font-weight: 700;
        }

        .contenedorPrimario{
            display: flex;
            justify-content: center;
        }

        .contenedorSecundario{
            max-width: 500px;
            text-align: center;
            margin: 0.5rem;
            border: #F23440 solid 2px;
            padding-top: 2rem;
            padding-bottom: 2rem;
            padding-right: 8px;
            padding-left: 8px;
            border-radius: 30px;
        }

        .textoContacto{
            margin: 0.3rem;
        }

        a{
            text-decoration: none
        }

        .tituloDescripcion{
            font-weight: 800;
        }
        table, th, td {
            border: 1px solid black;
        }
        .textoRojo{
            color: red
        }

        .botonCopiado{
            background-color: #5FAD57 !important;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.4);
            border: none;
            border-radius: 28px;
            padding: 5px;
            margin: 0.3rem;
            font-weight: 700;
            color: white;
            padding-left: 2rem;
            padding-right: 2rem;
            padding-top: 0.1rem;
            padding-bottom: 0.1rem;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="primeraParteCorreo">
                <img src="{{ $message->embed("miniLogo.png") }}" alt="logo wiqli haz las compras desde tu casa" />
            </div>
            <p>¡Qué emoción, ya eres parte de Wiqli!</p>
            <div class="col-md-12">
                <p>Has creado tu cuenta exitosamente.</p>
                <p>
                    Solo hace falta verificar tu cuenta aquí:
                </p>
            </div>
            <button class="botonCopiado">
                <a href="https://wiqli.com/verificar-correo/{{ $codigo }}">Verificar cuenta</a>
            </button>
        </div>
        <p>En caso tengas alguna duda sobre el estado de tu pedido o quieras agregar algún artículo no dudes en contactarnos al 
            <a href="https://api.whatsapp.com/send?phone=947298060&text=Hola,%20necesito%20ayuda%20para%20hacer%20mi%20pedido">
                947298060
            </a>
        </p>
        <p>Te deseamos una excelente semana</p>
        <p class="mensajeFinalDestacado">¡Empieza bien tu semana con Wiqli!</p>

        <div class="contenedorCorreo">
            <h5 class="textoDisclaimer">¿Tienes alguna duda?</h5>
            <p class="textoContacto">Escríbenos al</p>
            <p class="textoContacto"><a href="https://api.whatsapp.com/send?phone=947298060&text=Hola,%20necesito%20ayuda%20para%20hacer%20mi%20pedido">947298060</a></p>
        </div>
    </div>
</body>
</html>